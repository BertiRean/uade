package MerkleTree;

import java.util.HashMap;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.LinkedList;

public class MerkleTree 
{
	MerkleNode hashRoot;
	HashMap<Integer, MerkleNode> transLeavesMap;
	
	public MerkleTree()
	{
		hashRoot = null;
		transLeavesMap = new HashMap<Integer, MerkleNode>();
	}
	
	void destroyTree(MerkleNode root)
	{
		if (root == null)
			return;
		
		if (root.isLeaf())
		{
			root = null;	
			return;
		}
		
		destroyTree(root.leftTree);
		root.leftTree = null;
		destroyTree(root.rightTree);
		root.rightTree = null;
	}
	
	void preOrder(MerkleNode root)
	{
		if (root == null)
			return;
		
		preOrder(root.leftTree);
		System.out.println(root.toString());
		preOrder(root.rightTree);
	}
	
		
	ArrayList<MerkleNode> getHashesFromFullNode(Integer transaction, int steps)
	{
		ArrayList<MerkleNode> hashes = new ArrayList<MerkleNode>();
		
		MerkleNode currentNode = transLeavesMap.get(transaction);
		
		if (currentNode == null)
			return hashes;
		
		MerkleNode brother = null;
		
		while (currentNode.parent != null)
		{
			steps++;
			MerkleNode parent = currentNode.parent;
			brother = parent.getBrotherOf(currentNode);
			hashes.add(brother);
			currentNode = parent;
		}
		
		System.out.println("Number of Steps Needed: " + steps);
		
		return hashes;
	}
	
	void BuildTree(ArrayList<Integer> transactions)
	{
		LinkedList<MerkleNode> nodes = new LinkedList<MerkleNode>();
		
		for (Integer trans : transactions)
		{
			MerkleNode node = new MerkleNode(trans);
			
			transLeavesMap.put(trans, node);
			nodes.add(node);
		}
		
		this.hashRoot = BuildTreeLevels(nodes);
		
		return;
	}
	
	
	
	MerkleNode BuildTreeLevels(LinkedList<MerkleNode> nodes)
	{
		if (nodes.isEmpty())
			return null;
		
		if (nodes.size() < 2)
			return nodes.get(0);		
		
		LinkedList<MerkleNode> parents = new LinkedList<MerkleNode>();
		
		while (!nodes.isEmpty())
		{
			MerkleNode leftTree = nodes.pollFirst();
			MerkleNode rightTree = null;
			
			if (nodes.isEmpty())
				rightTree = leftTree;
			else
				rightTree = nodes.pollFirst();
			
			MerkleNode parent = new MerkleNode();
			
			BigInteger parentHash = leftTree.hashKey.add(rightTree.hashKey); 
			parent.hashKey = MerkleNode.getMD5Hash(parentHash);
			parent.leftTree = leftTree;
			parent.rightTree = rightTree;
			parent.transDescription = leftTree.transDescription + rightTree.transDescription;
			
			leftTree.parent = parent;
			rightTree.parent = parent;
			
			parents.add(parent);
		}
		
		return BuildTreeLevels(parents);
	}
}
