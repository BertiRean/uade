package MerkleTree;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Random;

public class MainClass 
{
	private static int getRand(int min, int max) 
	{
        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }
	
	public static void main(String[] args)
	{
		MerkleTree fullNode = new MerkleTree();
		MerkleTree lightNode = new MerkleTree();
		int n = 4096;
		
		ArrayList<Integer> transactions = new ArrayList<Integer>();
		
		for (int i = 1; i <= n; ++i)
			transactions.add(i);
		
		fullNode.BuildTree(transactions);
		
		lightNode.hashRoot = fullNode.hashRoot;
		
		int steps = 0;
		Integer randomTrans = transactions.get(getRand(0, transactions.size() - 1));
		ArrayList<MerkleNode> path = fullNode.getHashesFromFullNode(randomTrans, steps);
		ArrayList<MerkleNode> noTransaction = fullNode.getHashesFromFullNode(n + 3, steps);
		
		if (noTransaction.isEmpty())
			System.out.println("Transaccion no encontrada");
		
		
		/*System.out.println("Merkle Tree Pre Order Traversal: ");
		fullNode.preOrder(fullNode.hashRoot);
		System.out.println("End of Traversal");*/
		
		
		if (path.isEmpty())
			System.out.println("Transaction not Found");
		else
		{
			BigInteger totalHash = MerkleNode.getMD5Hash(randomTrans);
			System.out.println("Hashes Needed for Transaction Number: " + randomTrans);
			
			for (MerkleNode node : path)
			{
				totalHash = MerkleNode.getMD5Hash(totalHash.add(node.hashKey));
				System.out.println(node.toString());
			}
			
			System.out.println("\n\nHashRoot of LightNode Equals to Hashes Sended: " + totalHash.equals(lightNode.hashRoot.hashKey));
			System.out.println("\tHashRoot: " + lightNode.hashRoot.hashKey);
			System.out.println("\tTotal Hashes Returned: " + totalHash);
		}
		
		fullNode.destroyTree(fullNode.hashRoot);
		fullNode = null;
		
		System.out.println("Program Finished");
	}
}
