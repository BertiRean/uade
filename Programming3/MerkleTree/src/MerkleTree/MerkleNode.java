package MerkleTree;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.math.BigInteger;

public class MerkleNode 
{
	public MerkleNode leftTree;
	public MerkleNode rightTree;
	public MerkleNode parent;
	public String transDescription;
	
	public BigInteger hashKey;
	
	public MerkleNode()
	{
		hashKey = BigInteger.valueOf(-1);
		parent = null;
		leftTree = null;
		rightTree = null;
		transDescription = new String();
	}
	
	static BigInteger getMD5Hash(Integer transaction)
	{
	    try 
	    {
            // Static getInstance method is called with hashing MD5 
            MessageDigest md = MessageDigest.getInstance("MD5"); 
  
            // digest() method is called to calculate message digest 
            //  of an input digest() return array of byte
            byte[] messageDigest = md.digest(BigInteger.valueOf(transaction).toByteArray()); 
  
            // Convert byte array into signum representation 
            BigInteger hashCode = new BigInteger(1, messageDigest); 
  
            return hashCode;
        }// For specifying wrong message digest algorithms 
	    catch (NoSuchAlgorithmException e) 
	    { 
            throw new RuntimeException(e); 
        } 
	}
	
	static BigInteger getMD5Hash(BigInteger transaction)
	{
	    try 
	    {
            // Static getInstance method is called with hashing MD5 
            MessageDigest md = MessageDigest.getInstance("MD5"); 
  
            // digest() method is called to calculate message digest 
            //  of an input digest() return array of byte
            byte[] messageDigest = md.digest(transaction.toByteArray()); 
  
            // Convert byte array into signum representation 
            BigInteger hashCode = new BigInteger(1, messageDigest); 
  
            return hashCode;
        }// For specifying wrong message digest algorithms 
	    catch (NoSuchAlgorithmException e) 
	    { 
            throw new RuntimeException(e); 
        } 
	}
	
	public MerkleNode(int transaction)
	{
		hashKey = getMD5Hash(transaction);
		parent = null;
		leftTree = null;
		rightTree = null;
		transDescription = new String();
		transDescription += String.valueOf(transaction);
	}
	
	public boolean isLeaf()
	{
		return leftTree == null && rightTree == null;
	}
	
	public MerkleNode getBrotherOf(MerkleNode child)
	{
		if (child == leftTree)
			return rightTree;
		else if (child == rightTree)
			return leftTree;
		
		return null;
	}
	
	public String toString()
	{
		return "Hash of Node: " + hashKey + " --- H" + transDescription;
	}
};


