import math


chances = [0.17, 0.13, 0.07, 0.13, 0.12, 0.05, 0.13, 0.2]
entropies = []

totalEntro = 0
totalMidLong = 0

for prob in chances:
  entro = prob * math.log(1/prob, 2)
  entropies.append(entro)

for entro in entropies:
  totalEntro += entro
  print(entro, '\n')

print("Total Entropie: ", totalEntro)

long = [3 ,3 ,4, 3, 3, 4, 3 , 3]
longMid = []

for x in range(len(long)):
  
  midLong = chances[x] * long[x]
  longMid.append(midLong)


for midLong in longMid:
  print(midLong, '\n')
  totalMidLong += midLong


sourceEntropy = totalEntro / totalMidLong

print("Total Long Mid and Total Entropy:", totalMidLong, totalEntro)

print("Source Entropy: ", sourceEntropy)
print("Total Redundancy: ", (1 - sourceEntropy) * 100)