#include <iostream>
#include <string>
#include <vector>
#include <bitset>
#include <cassert>
#include <tuple>
#include <algorithm>

using namespace std;

using big_int = std::int_fast64_t;

// note: exp must be non-negative
big_int pow(int base, int exp)
{
    big_int result{ 1 };
    while (exp)
    {
        if (exp & 1)
            result *= base;
        exp >>= 1;
        base *= base;
    }
 
    return result;
}

big_int get_parity_count(size_t n)
{
  size_t i = 0;
  big_int k = 0;
  for (i = 0; k < n; ++i)
    k = pow(2, i);

  return i;
}

std::vector<int> get_parity_bits_positions(big_int k)
{
	std::vector<int> parity_bits_position;

	for (size_t i = 0; i < k; ++i)
		parity_bits_position.push_back(pow(2, i) - 1);

	return parity_bits_position;
}

void fill_hamming_word(std::string & word, std::string const & info_word, std::vector<int> const & parity_bits_position)
{
	size_t k = 0;
	size_t j = 0;
	for (size_t i = 0; i < word.size(); ++i)
	{
		if (parity_bits_position[j] == i)
		{
			j++;
			word[i] = '?';
		}
		else
			word[i] = info_word[k++];
	}
}

void set_parity_values(std::vector<int> const & parity_bits_position, std::string & hamming_word, bool isEvenParity)
{
	size_t i = 0;
	
  std::cout << "Before Transf: " << hamming_word;

	for (auto parity_bit : parity_bits_position)
	{
		i = parity_bit + 1;
		big_int checked_vals = 0;
		big_int sum = 0;
    	bool skipCheck = false;

    std::cout << "\n\nParity " << parity_bit + 1 << " = ";

		for (size_t j = parity_bit; j < hamming_word.size(); ++j)
		{
	      if (checked_vals >= i)
	      {
	        skipCheck = !skipCheck;
	        checked_vals = 0;
	      }

	      checked_vals++;

	      if (!skipCheck)
	      {
	        char bit = hamming_word[j];
          cout << "i" << j + 1 << "= " << bit <<  ", ";
	        if (bit == '1')
			  	  sum += 1;
	      }
		}

		if (sum % 2 == 0)
			hamming_word[parity_bit] = isEvenParity ? '0' : '1';
		else
			hamming_word[parity_bit] = isEvenParity ? '1' : '0';

	}
  std::cout << "\n\nAfter Transf: " << hamming_word << "\n\n";
}

std::string get_hamming_code(std::string bit_word, bool isEvenParity)
{
	size_t n = bit_word.size();
	big_int k = get_parity_count(n);
	std::vector<int> parity_bits_position = get_parity_bits_positions(k);

	std::string hamming_word(n + k, '?');

	fill_hamming_word(hamming_word, bit_word, parity_bits_position);
	set_parity_values(parity_bits_position, hamming_word, isEvenParity);
	return hamming_word;
}

unsigned long long get_error_position(std::vector<int> & parity_bits_position, std::string & hamming_word, bool isEvenParity)
{
	size_t i = 0;
	
  std::reverse(parity_bits_position.begin(), parity_bits_position.end());

  std::string error_bit_code;

	for (auto parity_bit : parity_bits_position)
	{
		i = parity_bit + 1;
		big_int checked_vals = 0;
		big_int sum = 0;
    bool skipCheck = false;

    std::cout << "\n\nParity " << parity_bit + 1 << " = ";

		for (size_t j = parity_bit; j < hamming_word.size(); ++j)
		{
	      if (checked_vals >= i)
	      {
	        skipCheck = !skipCheck;
	        checked_vals = 0;
	      }

	      checked_vals++;

	      if (!skipCheck)
	      {
	        char bit = hamming_word[j];
          cout << "i" << j + 1 << "= " << bit <<  ", ";
	        if (bit == '1')
			  	  sum += 1;
	      }
		}

		if (sum % 2 == 0)
			error_bit_code.push_back(isEvenParity ? '0' : '1');
		else
			error_bit_code.push_back(isEvenParity ? '1' : '0');

	}

  cout << "\n\nError Binary Index: " << error_bit_code;

  return std::stoull(error_bit_code, 0, 2);
}


std::string decode_hamming_code(std::string & bit_word, bool isEvenParity)
{
  cout << "Fixing Error in Hamming Code: " << bit_word;

	size_t n = bit_word.size() + 1;

	big_int k = 0;
  big_int i = 0;

  for (i = 0; k < n; ++i)
  {
		k = pow(2, i);

    if (k >= n)
      break;

  }

	std::vector<int> parity_bits_position = get_parity_bits_positions(i);

	auto error_pos = get_error_position(parity_bits_position, bit_word, isEvenParity);
	
  cout << "\n\nBefore Fix Error: " << bit_word << "\n\n";
  
  auto error_bit = bit_word.at(error_pos - 1);

	bit_word.at(error_pos - 1) = error_bit == '0' ? '1' : '0';

  cout << "After Fix Error: " << bit_word << "\n\n";

	return bit_word;
}

void check_tests()
{
	using test_pair = std::tuple<std::string, std::string, bool>;

	std::vector<test_pair> tests = 
	{
		test_pair("010111", "0101101111", false),
		test_pair("011010", "1001110010", false),
		test_pair("110111", "1011101111", false),
		test_pair("110001", "1110100001", false),
    test_pair("10011010", "011100101010", true),
    test_pair("1000", "1110000", true)
	};

	for (auto test : tests)
  {
    string input_word = get<0>(test);
    string coded_word = get<1>(test);
    bool isEvenParity = get<2>(test);
    assert(coded_word == get_hamming_code(input_word, isEvenParity));
  }

  std::vector<test_pair> decode_tests = 
  {
    test_pair("0001001", "0011001", true),
    test_pair("0111010", "0101010", true),
  };

  for (auto test : decode_tests)
  {
    string input_word = get<0>(test);
    string coded_word = get<1>(test);
    bool isEvenParity = get<2>(test);
    assert(coded_word == decode_hamming_code(input_word, isEvenParity));
  }
}

int main() 
{
	check_tests();
  return 0; 
}