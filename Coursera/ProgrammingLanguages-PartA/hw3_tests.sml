val p1 = TupleP([Variable("Ant"), ConstructorP("P12", Variable("Ana")), TupleP ( [Variable("Mar"), Variable("Sio")] ) ]);
val p2 = TupleP([ConstructorP("P12", Wildcard), TupleP ( [Wildcard, UnitP, ConstP(10)] ), ConstP(12) ]);
val p3 = TupleP([Variable("Ant"), ConstructorP("P12", Variable("Ant")), TupleP ( [Variable("Mar"), Variable("Sio")] ) ]);
