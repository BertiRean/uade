(* Dan Grossman, Coursera PL, HW2 Provided Code *)

(* if you use this function to compare two strings (returns true if the same
   string), then you avoid several of the functions in problem 1 having
   polymorphic types that may be confusing *)
fun same_string(s1 : string, s2 : string) =
    s1 = s2

(* put your solutions for problem 1 here *)
fun all_except_option(s : string, strings : string list) =
	let fun build_option_list(items, s, lst, founded) = 
		case items of
			[] => if not founded then NONE else SOME lst
			| curr_str::items' => if same_string(s, curr_str) then build_option_list(items', s, lst, not founded) else build_option_list(items', s, lst @ [curr_str], founded)
	in
		build_option_list(strings, s, [], false)
	end


fun get_substitutions1(str_lists : string list list, s : string) =
	case str_lists of
		[] => []
		| curr_str_list::str_lists' => let
			val option_list = all_except_option(s, curr_str_list)
		in
			case option_list of
				NONE => get_substitutions1(str_lists', s)
				| SOME lst => lst @ get_substitutions1(str_lists', s)
		end 

fun get_substitutions2(str_lists : string list list, s : string) =
	let fun get_string_lists(str_lists, s, ret_list : string list) =
		case str_lists of
			[] => ret_list
			| curr_str_list::str_lists' => 
			let
			 	val option_list = all_except_option(s, curr_str_list)
			 in
			 	case option_list of
					NONE => get_string_lists(str_lists', s, ret_list)
					| SOME lst => get_string_lists(str_lists', s, ret_list @ lst)
			 end 

	in
		get_string_lists(str_lists, s, [])
	end

fun similar_names(subs : string list list, full_name : {first:string, middle:string, last:string}) = 
	let
		val names = get_substitutions2(subs, #first full_name)
	in
		let fun sub_names(names : string list, lst, name_record : {first:string, middle:string, last:string}) =
			case names of
				[] => [name_record] @ lst
				| name::names' => let
					val comb = {first = name, middle = #middle name_record, last = #last name_record}
				in
					sub_names(names', lst @ [comb], name_record)
				end
		in
			sub_names(names, [], full_name)
		end
	end

(* you may assume that Num is always used with values 2, 3, ..., 10
   though it will not really come up *)
datatype suit = Clubs | Diamonds | Hearts | Spades
datatype rank = Jack | Queen | King | Ace | Num of int 
type card = suit * rank

datatype color = Red | Black
datatype move = Discard of card | Draw 

exception IllegalMove

(* put your solutions for problem 2 here *)
fun card_color(c : card) =
	case c of
		(Clubs, i) => Black
		| (Spades, i) => Black
		| (Diamonds, i) => Red
		| (Hearts, i) => Red

fun card_value(c : card) = 
	case c of
		(ci, Ace) => 11
		|(ci, Jack) => 10
		|(ci, Queen) => 10
		|(ci, King) => 10
		|(ci, Num(i)) => i

fun remove_card(cs : card list, c : card, ex) =
	let fun build_card_list(cs, c, e, acc, exist) =
		case cs of
			[] => if not exist then raise e else acc
			| curr_c::cs' => 
				if c = curr_c 
				then 
					if exist then build_card_list(cs', c, e, curr_c::acc, exist) else build_card_list(cs', c, e,  acc, not exist) 
				else
					build_card_list(cs', c, e, curr_c::acc, exist)

	in
		build_card_list(cs, c, ex, [], false)
	end


fun all_same_color(cs : card list) =
	case cs of
		[] => true
		| _::[] => true
		| c::(next_c::cs') => card_color(c) = card_color(next_c) andalso all_same_color(next_c::cs')

fun sum_cards(cs : card list) =
	let fun sum(cs, acc) =
		case cs of
			[] => acc
			| c::cs' => sum(cs', acc + card_value(c))
	in
		sum(cs, 0)
	end

fun score(held : card list, goal : int) =
	let fun preliminar_score(held, goal) =
		let
			val sum = sum_cards(held)
		in
			if sum > goal then 3 * (sum - goal) else (goal - sum)
		end
	in
		let
			val total = preliminar_score(held, goal)
		in
			if all_same_color(held) then total div 2 else total
		end
	end

fun officiate(cards : card list, moves : move list, goal : int) =
	let fun process_game(cards, moves, goal, held) =
		case moves of
			[] => score(held, goal)
			| mv::moves' => case mv of
								Discard(c) => process_game(cards, moves', goal, remove_card(held, c, IllegalMove))
								| Draw => case cards of
											[] => score(held, goal)
											| curr_c::cards' => if sum_cards(curr_c::held) > goal then score(curr_c::held, goal) else process_game(cards', moves', goal, curr_c::held)
	in
		process_game(cards, moves, goal, [])
	end


fun score_challenge(held : card list, goal : int) =
