fun is_older (dateOne : int*int*int, dateTwo : int*int*int) =
	if #1 dateOne < #1 dateTwo
	then true
	else
		if #1 dateOne > #1 dateTwo
		then false
		else
			if #2 dateOne < #2 dateTwo
			then true
			else
				if #2 dateOne > #2 dateTwo
				then false
				else
					if #3 dateOne < #3 dateTwo
					then true
					else false


fun number_in_month(dateList : (int*int*int) list, month : int) = 
	if null dateList
	then 0
	else
		if #2 (hd dateList) = month
		then
			1 + number_in_month(tl dateList, month)
		else
			0 + number_in_month(tl dateList, month)


fun number_in_months (dateList : (int*int*int) list, months : int list) = 
	if null months
	then 0
	else
		let
			val curr_months = number_in_month(dateList, hd months)
		in
			curr_months + number_in_months(dateList, tl months)
		end


fun dates_in_month (dateList : (int*int*int) list, month : int) = 
	if null dateList
	then []
	else
		let
			val currDate = hd dateList
		in
			if #2 currDate = month
			then
				currDate :: dates_in_month(tl dateList, month)
			else
				dates_in_month(tl dateList, month)
		end


fun dates_in_months(dateList : (int*int*int) list, months : int list) =
	if null months
	then
		[]
	else
		dates_in_month(dateList, hd months) @ dates_in_months(dateList, tl months)


(* Returns the nth element from list, only works if n <= list size*)
fun get_nth(words : string list, n : int) = 
	if n = 1
	then
		hd words
	else
		get_nth(tl words, n - 1)


fun date_to_string(date : int*int*int) = 
	let
		val months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
	in
		get_nth(months, #2 date) ^ " " ^ Int.toString(#3 date) ^ ", " ^ Int.toString(#1 date)
	end

fun number_before_reaching_sum(sum : int, numbers : int list) =
	if sum <= 0
	then
		0
	else
		let
			val added = hd numbers
		in
			if added < sum
			then
				1 + number_before_reaching_sum(sum - added, tl numbers)
			else
				0
		end


fun what_month(day : int) = 
	let
		val month_bounds = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
	in
		number_before_reaching_sum(day, month_bounds) + 1
	end


fun month_range(day1 : int, day2 : int) = 
	if day1 > day2
	then
		[]
	else
		let
			fun create_month_list(from : int, to : int) = 
				if from = to
				then
					[what_month(to)]
				else
					what_month(from) :: create_month_list(from + 1, to)
		in
			create_month_list(day1, day2)
		end


fun oldest (dates : (int*int*int) list) =
	if null dates
	then NONE
	else
		let
			fun oldest_date(olderDate : (int*int*int), dateList : (int*int*int) list) = 
				if null dateList
				then
					olderDate
				else
					let
						val nextDate = hd dateList
					in
						if is_older(olderDate, nextDate)
						then
							oldest_date(olderDate, tl dateList)
						else
							oldest_date(nextDate, tl dateList)
					end
		in
			SOME (oldest_date(hd dates, tl dates))
		end