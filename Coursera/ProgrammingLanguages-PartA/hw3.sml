(* Coursera Programming Languages, Homework 3, Provided Code *)

exception NoAnswer

datatype pattern = Wildcard
		 | Variable of string
		 | UnitP
		 | ConstP of int
		 | TupleP of pattern list
		 | ConstructorP of string * pattern

datatype valu = Const of int
	      | Unit
	      | Tuple of valu list
	      | Constructor of string * valu

fun g f1 f2 p =
    let 
	val r = g f1 f2 
    in
	case p of
	    Wildcard          => f1 ()
	  | Variable x        => f2 x
	  | TupleP ps         => List.foldl (fn (p,i) => (r p) + i) 0 ps
	  | ConstructorP(_,p) => r p
	  | _                 => 0
    end

(**** for the challenge problem only ****)

datatype typ = Anything
	     | UnitT
	     | IntT
	     | TupleT of typ list
	     | Datatype of string

(**** you can put all your code here ****)

fun only_capitals(ls : string list) =
	List.filter (fn (s) => Char.isUpper(String.sub (s, 0))) ls

fun longest_string1(ls : string list) =
	List.foldl (fn (x, y) => 
		if String.size x > String.size y then x else y) "" ls

fun longest_string2(ls : string list) =
		List.foldl (fn (x, y) => 
		if String.size x >= String.size y then x else y) "" ls

fun longest_string_helper f ls =
	List.foldl (fn (x, y) => 
		if f (String.size x, String.size y) then x else y) "" ls

val longest_string3 = longest_string_helper (fn (x, y) => x > y);
val longest_string4 = longest_string_helper (fn (x, y) => x >= y);


fun longest_capitalized (ls : string list) =
	(longest_string1 o only_capitals) ls

fun rev_string(s : string) =
	(String.implode o List.rev o String.explode) s
	

fun first_answer f ls =
	case ls of
		[] => raise NoAnswer
		| x::xs' => case f x of
			SOME x' => x'
			| _ => first_answer f xs'


fun all_answers f ls =
	let
		fun loop f ls acc =
			case ls of
				[] => acc
				| x::xs' => 
				case f x of
					SOME x' => loop f xs' (SOME (valOf(acc) @ x'))
					| NONE => NONE
	in
		loop f ls (SOME [])
	end

fun count_wildcards p =
	g (fn () => 1) (fn s => 0) p

fun count_wild_and_variable_lengths  p =
	g (fn () => 1) (fn s => String.size s) p

fun count_some_var (str, p) =
	g (fn () => 0) (fn s => if str = s then 1 else 0) p

fun check_pat p = 
	let
		fun duplicates ls =
			case ls of
				[] => false
				| x'::[] => false
				| x::xs' => (List.exists (fn (s1 : string) => s1 = x) xs') orelse (duplicates xs')

		fun get_pattern_str p =
			case p of
				Variable s => [s]
				| TupleP ps => List.foldl (fn (p', ls) => (get_pattern_str p') @ ls) [] ps
				| ConstructorP (_, p') => get_pattern_str(p')
				| _ => []

	in
		not (duplicates (get_pattern_str p))
	end

fun match (v : valu, p : pattern) =
	case p of
		Wildcard => SOME []
		| Variable s => SOME [(s, v)]
		| UnitP => (case v of Unit => SOME [] | _ => NONE)
		| ConstP i => (case v of 
						Const i' => if i = i' then SOME [] else NONE | _ => NONE)
		| ConstructorP(s1, p1) => (case v of Constructor(s2, v1) => if s1 = s2 then match(v1, p1) else NONE | _=> NONE)
		| TupleP ps => (
			case v of 
				Tuple vs => all_answers (fn (p1, v1) => case match(v1, p1) of SOME ls => SOME (ls) | NONE => NONE) (ListPair.zip(ps, vs))
				| _=> NONE
			)

fun first_match v ps =
	first_answer (fn (p) => case match(v, p) of 
		SOME ls => SOME (SOME (ls))
		| _ => NONE) ps