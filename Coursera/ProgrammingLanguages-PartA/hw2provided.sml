(* Dan Grossman, Coursera PL, HW2 Provided Code *)

(* if you use this function to compare two strings (returns true if the same
   string), then you avoid several of the functions in problem 1 having
   polymorphic types that may be confusing *)
fun same_string(s1 : string, s2 : string) =
    s1 = s2

(* put your solutions for problem 1 here *)

fun all_except_option(str : string, strings : string list) =
	let fun build_option_list(items, str, lst, founded) = 
		case items of
			[] => if not founded then NONE else SOME lst
			| curr_str::items' => if same_string(str, curr_str) then build_option_list(items', str, lst, not founded) else build_option_list(items', str, curr_str::lst, founded)
	in
		build_option_list(strings, str, [], false)
	end


fun get_substitutions1(str_lists : string list list, s : string) =
	let fun get_string_lists(str_lists, s, ret_list : string list) =
		case str_lists of
			[] => ret_list
			| curr_str_list::str_lists' =>
			let
			 	val option_list = all_except_option(s, curr_str_list)
			 in
			 	case option_list of
					NONE => get_string_lists(str_lists', s, ret_list)
					| SOME lst => get_string_lists(str_lists', s, lst::ret_list)
			 end 

	in
		get_string_lists(str_lists, s, [])
	end

(* you may assume that Num is always used with values 2, 3, ..., 10
   though it will not really come up *)
datatype suit = Clubs | Diamonds | Hearts | Spades
datatype rank = Jack | Queen | King | Ace | Num of int 
type card = suit * rank

datatype color = Red | Black
datatype move = Discard of card | Draw 

exception IllegalMove

(* put your solutions for problem 2 here *)
