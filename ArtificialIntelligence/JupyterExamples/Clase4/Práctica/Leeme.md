# Instalar Python
* Web Oficial: https://www.python.org/
* Conda: https://anaconda.org/anaconda/conda
* Comprobar: `python --version`

# Entornos virtuales (recomendado)
* Crear: `python -m venv venv-ml`
* Activar: `.\venv-ml\Scripts\activate`

# Jupyter
* Notebook: 
    * Instalar: `python pip install notebook`
    * `jupyter notebook`
* Jupyter-lab: 
    * Instalar: `python pip install jupyterlab`
    * `jupyter-lab`

# Integrar Enviroment & Jupyter
* Dependencias: `python -m pip install ipykernel`
* Instalar: `python -m ipykernel install --name=venv-ml`
* Desinstalar: `jupyter kernelspec uninstall venv-ml`
* Listar: `jupyter kernelspec list`