'''
Condiciones
~~~~~~~~~~~
Quien usa vim?


Resumen:
Colores = Rojo, Azul, Verde, Blanco, Amarillo
Profesiones = Matematico, Hacker, Ingeniero, Analista, Desarrollador
Lenguaje = Python, C#, JAVA, C++, JavaScript
BD = Cassandra, MongoDB, Neo4j, Redis, HBase
editor = Brackets, Sublime Text, Atom, Notepad++, Vim
'''

import random
import time


colors =      {'001' : 'red',          '010' : 'blue',          '011' : 'green',    '100' : 'white',    '101' : 'yellow'}
prefession =  {'001' : 'Mathematician','010' : 'Hacker',        '011' : 'Engineer', '100' : 'Analyst',  '101' : 'Developer'}
languaje =    {'001' : 'Python',       '010' : 'C#',            '011' : 'Java',     '100' : 'C++',      '101' : 'JavaScript'}
database =    {'001' : 'Cassandra',    '010' : 'MongoDB',       '011' : 'HBase',    '100' : 'Neo4j',    '101' : 'Redis'}
editor =      {'001' : 'Brackets',     '010' : 'Sublime Text',  '011' : 'Vim',      '100' : 'Atom',     '101' : 'Notepad++'}

target_score = 15
N = 10000
MAX_ITERATIONS = 200
POPULATION_SIZE = 2000

class Phenotype:

    def __init__(self):
        # crear un individuo
        self.chromosome = ['' for gen in range(25)]
        self.score   = 0
        pass

    def randomize(self):
      gen_codes = ['001', '010', '011', '100', '101']

      for x in range(len(self.chromosome)):
        new_code = random.choice(gen_codes)
        self.chromosome[x] = new_code

    def decode(self):
        ''' traduce 0's y 1's (conjunto de genes: 3) en valores segun un diccionario '''
        return [[colors[self.chromosome[i*5+0]], 
                 prefession[self.chromosome[i*5+1]],
                 languaje[self.chromosome[i*5+2]],
                 database[self.chromosome[i*5+3]],
                 editor[self.chromosome[i*5+4]]] for i in range(5)]
        

    def encode(self):
        pass


    def mutate(self):
        ''' muta un fenotipo, optimizado'''
        first = random.randint(0, len(self.chromosome) - 1)
        second = random.randint(0, len(self.chromosome) - 1)

        # Intentamos 10 veces como maximo cambiar el second si son iguales
        count = 0
        while count < 10 and first == second:
          second = random.randint(0, len(self.chromosome) - 1)
          count += 1

        temp = self.chromosome[first]
        self.chromosome[first] = self.chromosome[second]
        self.chromosome[second] = temp

        pass

    def fitness_function(self):
        ''' calcula el valor de fitness del cromosoma segun el problema en particular '''
        self.score = 1

        ok_score = 1
        fail_score = -1
        punish_score = -1

        
        #                         0         1         2         3        4
        # Pheno Structure => [ Color, Profesion, Languaje, Database, Editor]
        # Testeo de Pistas
        chromo_decoded = self.decode()

        # Pena a los duplicados
        chromo_len = len(chromo_decoded)

        for i in range(chromo_len):
          itr = chromo_decoded[i]

          for j in range(i + 1, chromo_len):
            next_ = chromo_decoded[j]

            for k in range(len(itr)):
              if itr[k] == next_[k]:
                if k == 0:
                  hasFiveHouse = False
                self.score += 3 * punish_score

        for i in range(len(chromo_decoded)):
          pheno = chromo_decoded[i]
          # Pistas Individuales
          self.score += self.fitness_one_hint(pheno, i)

          # Pistas Vecinales
          # 6. La casa verde esta a la derecha de la casa blanca.
          if pheno[0] == 'green':
            if i - 1 >= 0:
              if chromo_decoded[i - 1][0] == 'white':
                self.score += ok_score
              else:
                self.score += fail_score
            else:
              self.score += punish_score

          # 11. La persona que usa HBase vive al lado de la que programa en JavaScript.
          if pheno[3] == 'HBase':
            hasJavaScript = False
            if i - 1 >= 0: # JavaScript esta en la casa izquierda
              if chromo_decoded[i-1][2] == 'JavaScript':
                hasJavaScript = True
              else:
                hasJavaScript = False
            if i + 1 < len(chromo_decoded) and not hasJavaScript: # JavaScript es usado en la casa derecha
              if chromo_decoded[i+1][2] == 'JavaScript':
                hasJavaScript = True
              else:
                hasJavaScript = False

            if hasJavaScript:
              self.score += ok_score
            else:
              self.score += fail_score

          # 12. La persona que usa Cassandra es vecina de la que programa en C#.
          if pheno[3] == 'Cassandra':
            hasCSharp = False
            if i - 1 >= 0: # Javascript esta en la casa izquierda
              if chromo_decoded[i-1][2] == 'C#':
                hasCSharp = True
              else:
                hasCSharp = False
            if i + 1 < len(chromo_decoded) and not hasCSharp: # Javascript es usado en la casa derecha
              if chromo_decoded[i+1][2] == 'C#':
                hasCSharp = True
              else:
                hasCSharp = False

            if hasCSharp:
              self.score += ok_score
            else:
              self.score += fail_score

        pass

    def fitness_one_hint(self, pheno, index):

      #                         0         1         2         3        4
      # Pheno Estructure => [ Color, Profesion, Languaje, Database, Editor]

      ok_score = 1
      fail_score = -1
      punish_score = -1

      pheno_color = pheno[0]
      pheno_prof  = pheno[1]
      pheno_lang  = pheno[2]
      pheno_datab = pheno[3]
      pheno_editor= pheno[4]

      fit_score   = 0

      # 2. El Matematico vive en la casa roja.
      if pheno_color == 'red':
        if pheno_prof == 'Mathematician':
          fit_score += ok_score
        else:
          fit_score += 2 * fail_score

      # 3. El hacker programa en Python.
      if pheno_prof == 'Hacker':
        if pheno_lang == 'Python':
          fit_score += ok_score
        else:
          fit_score += 2 * fail_score

      # 4. El Brackets es utilizado en la casa verde.
      if pheno_editor == 'Brackets':
        if pheno_color == 'green':
          fit_score += ok_score
        else:
          fit_score += 2 * fail_score

      # 5. El analista usa Atom.
      if pheno_prof == 'Analyst':
        if pheno_editor == 'Atom':
          fit_score += ok_score
        else:
          fit_score += 2 * fail_score

      # 7. La persona que usa Redis programa en Java
      if pheno_datab == 'Redis':
        if pheno_lang == 'Java':
          fit_score += ok_score
        else:
          fit_score += 2 * fail_score

      # 8. Cassandra es utilizado en la casa amarilla
      if pheno_datab == 'Cassandra':
        if pheno_color == 'yellow':
          fit_score += ok_score
        else:
          fit_score += 2 * fail_score

      # 9. Notepad++ es usado en la casa del medio.
      if pheno_editor == 'Notepad++':
        if index == 2:
          fit_score += ok_score
        else:
          fit_score += 2 * fail_score

      # 10. El Desarrollador vive en la primer casa.
      if pheno_prof == 'Developer':
        if index == 0:
          fit_score += ok_score
        else:
          fit_score += 2 * fail_score

      # 13. La persona que usa Neo4J usa Sublime Text.
      if pheno_editor == 'Sublime Text':
        if pheno_datab == 'Neo4j':
          fit_score += ok_score
        else:
          fit_score += 2 * fail_score

      # 14. El Ingeniero usa MongoDB.
      if pheno_prof == 'Engineer':
        if pheno_datab == 'MongoDB':
          fit_score += ok_score
        else:
          fit_score += 2 * fail_score

      # 15. EL desarrollador vive en la casa azul.
      if pheno_color == 'blue':
        if pheno_prof == 'Developer':
          fit_score += ok_score
        else:
          fit_score += 2 * fail_score

      return fit_score

class Riddle:

    def __init__(self):
        self.start_time = time.time()
        self.population = []
        self.restart = True

    '''
    proceso general
    '''
    def selection(self, chromosome_list):
        parents = []

        while (len(chromosome_list) > 0):
            chromosome1 = random.choice(chromosome_list)
            chromosome_list.remove(chromosome1)

            chromosome2 = random.choice(chromosome_list)
            chromosome_list.remove(chromosome2)

            parents.append((chromosome1,chromosome2))
        return parents

    def solve(self, n_population):
        
        self.restart = False
        self.population.clear()
        self.generate(n_population)
        print(f"Población creada con {len(self.population)} individuos")

        ''' descomentame '''
        print(self.population[0].chromosome)
        print(self.population[0].decode())

        print("Inicio del proceso iterativo")
        fit, indi = self.iterar()

        indi_decoded = indi.decode()

        if fit >= target_score:
          for i in range(5):
            editor = indi_decoded[i][4]

            if editor == 'Vim':
              print("El {0} vive en la casa de color {1}, programa en {2}, usa como base de datos {3} y su programa editor es {4}\n\n".format(
                indi_decoded[i][1], indi_decoded[i][0], indi_decoded[i][2], indi_decoded[i][3], indi_decoded[i][4]))

        
        for x in indi.decode():
            print(x)

        print(f"Fin del proceso, mejor resultado \n - Fitness: {fit} \n - Individuo {indi.chromosome} \n - Individuo {indi.decode()}")

    def iterar(self):

        counter = 0
        break_condition = False
        
        while not(break_condition):
            
            if counter % 100 == 0:
                print("--------------------------------")
                print(counter)
                print(self.population[0].decode())
                print("--------------------------------")

            for x in self.population: 
              x.fitness_function()

            self.population.sort(key =lambda x : x.score, reverse = True)

            scores = []
            for x in range(5):
              scores.append(self.population[x].score)


            if self.population[0].score >= target_score:
              print("Top Fives Scores {0}".format(scores))
              break_condition = False
              break

            # seleccion
            parents = self.selection(self.population)

            # Crossover
            for pair in parents:
              child_pair = self.crossOver(pair[0], pair[1], 1)
              self.population.append(child_pair[0])
              self.population.append(child_pair[1])
        
            # mutate
            for x in range(200):
              y = random.randint(0, len(self.population) - 1)
              self.mutate(self.population[y])

            # condicion de corte
            if counter > MAX_ITERATIONS :
                print("Top Fives Scores {0}".format(scores))
                print("Maximas Iteraciones Alcanzadas, se reiniciara la poblacion")
                self.restart = True
                break_condition = True


            counter += 1

        return self.population[0].score, self.population[0]

    '''
    operacion: generar individuos y agregarlos a la poblacion
    '''
    def generate(self, i):
        for x in range(0,i):
            newbie = Phenotype()
            newbie.randomize()
            self.population.append(newbie)

    '''
    operacion: mutación. Cambiar la configuración fenotipica de un individuo
    '''
    def mutate(self, crossed, prob=0.5):
        if random.randint(0, 99) / 100 <= prob:
          crossed.mutate()
        pass


    '''
    operacion: cruazamiento. Intercambio de razos fenotipicos entre individuos
    '''
    def crossOver(self, parent1, parent2, limit):

        child = Phenotype()

        for x in range(len(child.chromosome)):
          prob = random.randint(0, 99) / 100

          if prob < 0.5:
            child.chromosome[x] = parent1.chromosome[x]
          else:
            child.chromosome[x] = parent2.chromosome[x]

        if parent1.score > parent2.score:
          return (parent1, child)
        else:
          return (parent2, child)

        pass

start = time.time()

rid = Riddle()

while rid.restart:
  rid.solve(POPULATION_SIZE * 5)

end = time.time()
hours, rem = divmod(end-start, 3600)
minutes, seconds = divmod(rem, 60)
print("Tiempo transcurrido {:0>2}:{:0>2}:{:05.2f}".format(int(hours),int(minutes),seconds))