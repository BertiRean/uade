# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util
import queue

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s]

def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print("Start:", problem.getStartState())
    print("Is the start a goal?", problem.isGoalState(problem.getStartState()))
    print("Start's successors:", problem.getSuccessors(problem.getStartState()))
    """
    "*** YOUR CODE HERE ***"
    action_path = []
    start_state = problem.getStartState()
    visited = {}
    stopSearch = [False]
    dfs_recursive(problem, start_state, visited, action_path, stopSearch)
    return action_path

def dfs_recursive(problem, current_state, visited, action_path, stopSearch):

	if problem.isGoalState(current_state):
		stopSearch[0] = True
		return

	visited[hash(current_state)] = (current_state)

	for sucessor in problem.getSuccessors(current_state):

		if stopSearch[0]:
			return

		next_state  = sucessor[0]
		next_action = sucessor[1]
		action_path.append(next_action)

		if visited.get(hash(next_state)) != None:
			action_path.pop()
			continue
		else:
			if problem.isGoalState(next_state):
				stopSearch[0] = True
				return

			dfs_recursive(problem, next_state, visited, action_path, stopSearch)

			if not stopSearch[0]:
				action_path.pop()


def breadthFirstSearch(problem):
    """Search the shallowest nodes in the search tree first."""
    "*** YOUR CODE HERE ***"
    visited = {}
    start_state = problem.getStartState()
    action_path = []

    if problem.isGoalState(start_state):
    	return []

    queueBfs = queue.Queue()

    """ Lo que ira en la cola seran Tuplas formadas por (Estado, Camino a ese estado desde
    el estado inicial). Asi obtenemos el conjunto solucion del problema de manera instantanea al
    hallar el estado objetivo. 
	
	Esta manera puede consumir mas memoria pero, tambien ahorra a el momento de encontrar el estado
	objetivo, ya que no hay que calcular otra vez el camino desde el estado inicial al objetivo.

    """
    queueBfs.put((start_state, action_path))

    while not queueBfs.empty():
    	current_tuple = queueBfs.get()
    	current_state = current_tuple[0]
    	action_path = current_tuple[1]

    	for successor in problem.getSuccessors(current_state):
    		next_state = successor[0]
    		next_action = successor[1]

    		if visited.get(hash(next_state)) != None:
    			continue
    		else:
    			visited[hash(next_state)] = next_state
    			next_path = action_path.copy()
    			next_path.append(next_action)

    			if problem.isGoalState(next_state):
    				return next_path

    			next_tuple = (next_state, next_path)
    			queueBfs.put(next_tuple)


    return action_path

def uniformCostSearch(problem):
    """Search the node of least total cost first."""
    "*** YOUR CODE HERE ***"
    start_state = problem.getStartState()
    heap = queue.PriorityQueue()
    visited = {}
    action_path = []

    heap.put((0, [start_state, action_path]))

    while not heap.empty():
    	current_tuple = heap.get()
    	current_state = current_tuple[1][0]
    	action_path = current_tuple[1][1]

    	if problem.isGoalState(current_state):
    		return action_path

    	visited[hash(current_state)] = current_state


    	for child in problem.getSuccessors(current_state):
    		next_state = child[0]

    		if visited.get(hash(next_state)) != None:
    			continue

    		next_action = child[1]
    		cost_to_next_state = child[2]

    		if problem.isGoalState(next_state):
    			action_path.append(next_action)
    			return action_path

    		next_path = action_path.copy()
    		next_path.append(next_action)
    		heap.put((cost_to_next_state, [next_state, next_path]))


    return action_path

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    "*** YOUR CODE HERE ***"

    start_state = problem.getStartState()
    heap = queue.PriorityQueue()
    visited = {}
    action_path = []

    heap.put((0, [start_state, action_path]))

    while not heap.empty():
    	current_tuple = heap.get()
    	current_state = current_tuple[1][0]
    	action_path = current_tuple[1][1]

    	if problem.isGoalState(current_state):
    		return action_path

    	visited[hash(current_state)] = current_state

    	for child in problem.getSuccessors(current_state):
    		next_state = child[0]

    		if visited.get(hash(next_state)) != None:
    			continue

    		next_action = child[1]
    		cost_to_next_state = child[2]
    		next_action_cost = cost_to_next_state + heuristic(next_state, problem)

    		if problem.isGoalState(next_state):
    			action_path.append(next_action)
    			return action_path

    		next_path = action_path.copy()
    		next_path.append(next_action)
    		heap.put((next_action_cost, [next_state, next_path]))


    return action_path

# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
