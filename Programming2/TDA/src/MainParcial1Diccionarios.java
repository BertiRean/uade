import java.util.Random;

public class MainParcial1Diccionarios 
{
	public static void PasarDicMulADicSimple(DiccionarioSimpleEst dictSimple, DiccionarioMultipleEst dictMult)
	{	
		
		// Estrategia:
		/*
		 * 1. Se obtienen las claves del diccionario multiple.
		 * 2. Se obtiene los valores asociados a cada clave del conjunto anterior y se itera sobre el conjunto de valores
		 * 3. Se suma cada valor a una variable sum, para obtener la suma en total de valores asociados a esa clave
		 * 4. Se verifica si la suma es >= 0 o < 0, y se coloca el valor -1 o 1 respectivamente
		 * 5. Se agrega el par clave valor a el dict Simple, aqui el valor es sum
		 * 6. Se elimina la clave vista del conj de claves del diccionario multiple.
		 * 7. Se repiten los pasos desde el 2 hasta el 6 hasta el conj de claves del dict multople quede vacio
		 * 
		 * 
		 * 
		 * 
		 * */
		
		ConjuntoEst clavesDictMult = CopiarConjunto(dictMult.Claves()); // O(n)
		
		// Total en Complejidad: O(n^3) o O(n^2) Dependiendo de Elegir y sacar en los TDA Conjunto
		// La complejidad variara
		
		while (!clavesDictMult.ConjuntoVacio()) // O(n)
		{
			int claveActual = clavesDictMult.Elegir(); // O(1)
			ConjuntoEst valores = CopiarConjunto(dictMult.Recuperar(claveActual)); // O(n) o O(1) si no se desea copiar
			
			int suma = 0;
			while (!valores.ConjuntoVacio()) // O(n)
			{
				int valorActual = valores.Elegir(); // O(1)
				suma += valorActual;
				valores.Sacar(valorActual); // O(n) o O(1) si el elemento a elegir siempre es el ultimo entonces 
				// Sacar sera O(1), eso depende de la implementacion del ConjuntoTDA
			}
			
			System.out.print("Suma: " + suma + "\n");
			if (suma < 0)
				suma = -1;
			else
				suma = 1;
			
			dictSimple.Agregar(claveActual, suma); // O(n)
			clavesDictMult.Sacar(claveActual); // O(n) o O(1) Depende de la implementacion del conjunto en Elegir
		}
	}
	
	public static void MostrarDiccionario(DiccionarioSimpleEst origen)
	{
		ConjuntoEst claves = origen.Claves();
		
		System.out.print("\nMostrar Diccionario\n\n");
		while (!claves.ConjuntoVacio())
		{
			int clave = claves.Elegir();
			int valor = origen.Recuperar(clave);
			System.out.print("Clave: " + clave + ", " + "Valor: " + valor + '\n');
			claves.Sacar(clave);
		}
		System.out.print("\nFin Mostrar Diccionario\n");
	}
	
	public static ConjuntoEst CopiarConjunto(ConjuntoEst origen)
	{
		ConjuntoEst copia = new ConjuntoEst();
		ConjuntoEst aux = new ConjuntoEst();
		
		copia.Inicializar();
		aux.Inicializar();
		
		while (!origen.ConjuntoVacio())
		{
			int valor = origen.Elegir();
			copia.Agregar(valor);
			aux.Agregar(valor);
			origen.Sacar(valor);
		}
		
		while (!aux.ConjuntoVacio())
		{
			int valor = aux.Elegir();
			origen.Agregar(valor);
			aux.Sacar(valor);
		}
		
		return copia;
	}
	
	public static void MostrarDiccionario(DiccionarioMultipleEst origen)
	{
		ConjuntoEst claves = CopiarConjunto(origen.Claves());
		
		System.out.print("\nMostrar Diccionario Multiple\n\n");
		while (!claves.ConjuntoVacio())
		{
			int clave = claves.Elegir();
			ConjuntoEst valores = CopiarConjunto(origen.Recuperar(clave));
			
			System.out.print("Clave: " + clave + "\n");
			while (!valores.ConjuntoVacio())
			{
				int valorActual = valores.Elegir();
				System.out.print("\tValor: " + valorActual + '\n');
				valores.Sacar(valorActual);
			}
			
			claves.Sacar(clave);
		}
		System.out.print("\nFin Mostrar Diccionario Multiple\n");
	}

	public static void main(String[] args)
	{
		DiccionarioSimpleEst dictSimple = new DiccionarioSimpleEst();
		DiccionarioMultipleEst dictMult = new DiccionarioMultipleEst();
		
		dictMult.Inicializar();
		dictSimple.Inicializar();
		
		// Generamos valores aleatorios para las claves
		Random rand = new Random();
		
		for (int i = 1; i < 5; ++i)
		{
			int max = rand.nextInt(5);
			
			for (int j = 0; j < max; ++j)
			{
				int valorAleatorio = rand.nextInt(10);
				
				if (rand.nextBoolean())
					valorAleatorio *= -1;
				
				dictMult.Agregar(i, valorAleatorio);
			}
		}
		
		MostrarDiccionario(dictMult);
		PasarDicMulADicSimple(dictSimple, dictMult);
		MostrarDiccionario(dictSimple);
	}
}
