class IntWrapper
{
	public int value;
}
	

public class MainArbolBinarioBusqueda 
{	
	public static void main(String[] args)
	{
		ArbolBinario raiz = new ArbolBinario();
		raiz.InicializarArbol();
		
		int valores[] = new int[] {10, 8, 11, 5, 9, 13, 12, 14};
		
		ArbolBinario t2 = new ArbolBinario();
		t2.InicializarArbol();
		
		int valores2[] = new int[] {10, 8, 12, 5, 9, 11, 14, 13, 15 };
		
		for(int k : valores)
			raiz.AgregarElemento(k);
		
		for (int k : valores2)
			t2.AgregarElemento(k);
		
		PreOrden(raiz);
		System.out.println("End Pre Order");
		InOrder(raiz);
		System.out.println("End In Order");
		PostOrden(raiz);
		System.out.println("End Post Orderx");
		
		int claveABuscar = 10;
		int nivel = 4;
		
		System.out.print("Clave "  + claveABuscar + " Encontrada: " + Busqueda(claveABuscar, raiz));
		System.out.println("");
		System.out.print("EsHoja La "  + claveABuscar + " A Buscar: " + BusquedaDeHoja(claveABuscar, raiz));
		System.out.println("");
		System.out.print("EsHoja La "  + claveABuscar + " A Buscar: " + BusquedaDeHoja(claveABuscar, raiz));
		System.out.println("");
		System.out.println("Profundidad de: " + claveABuscar + " -> " + ProfundidadElemento(claveABuscar, raiz));
		System.out.println("Altura del Arbol: " + AlturaArbol(raiz));
		System.out.println("Cantidad Hojas Arbol: " + CantidadHojas(raiz));
		System.out.println("Suma Elementos Arbol: " + SumaElementos(raiz));
		System.out.println("Arboles Iguales Forma: " + ArbolesIgualesForma(raiz, t2));
		System.out.println("Arboles Iguales Forma y Claves: " + ArbolesIguales(raiz, t2));
		System.out.println("Cantidad de Elementos en Nivel " + nivel + ": " + CantElementosEnNivelN(nivel, t2));

		MainConjuntoEstatico.MostrarConjunto(MayoresAK(15, raiz));
		
	}
	
	public static int ProfundidadElemento(int elemento, ArbolBinarioTDA raiz)
	{
		return ProfElementoRec(elemento, raiz, 1);
	}
	
	public static int CantElementosEnNivelN(int nivel, ArbolBinarioTDA raiz)
	{
		IntWrapper cant  = new IntWrapper();
		cant.value = 0;
		
		CantElementosEnNivelRec(nivel, cant, 1, raiz);
		
		return cant.value;
	}
	
	public static void CantElementosEnNivelRec(int nivel, IntWrapper cant, int nivelActual, ArbolBinarioTDA raiz)
	{
		if (nivelActual > nivel)
			return;
		
		if (raiz.ArbolVacio())
			return;
		
		if (nivelActual == nivel)
			cant.value++;
		
		CantElementosEnNivelRec(nivel, cant, nivelActual + 1, raiz.HijoDer());
		CantElementosEnNivelRec(nivel, cant, nivelActual + 1, raiz.HijoIzq());
		
	}
	
	// Arboles iguales en forma y elementos
	public static Boolean ArbolesIguales(ArbolBinarioTDA t1, ArbolBinarioTDA t2)
	{
		if (t1.ArbolVacio() && t2.ArbolVacio())
			return true;
		
		if (!t1.ArbolVacio() && !t2.ArbolVacio())
		{
			if (t1.Raiz() != t2.Raiz())
				return false;
			
			if (!ArbolesIguales(t1.HijoIzq(), t2.HijoIzq()))
				return false;
			
			if (!ArbolesIguales(t1.HijoDer(), t2.HijoDer()))
				return false;	
			
			return true;
		}
		
		return false;
	}
	
	public static ConjuntoEst MayoresAK(int k, ArbolBinarioTDA raiz)
	{
		ArbolBinarioTDA raizAux = raiz;
		ConjuntoEst valores = new ConjuntoEst();
		
		valores.Inicializar();
		
		while (!raizAux.ArbolVacio())
		{
			if (raizAux.Raiz() > k)
				valores.Agregar(raizAux.Raiz());
			
			raizAux = raizAux.HijoDer();
		}
		
		return valores; 
	}
	
	public static boolean ArbolesIgualesForma(ArbolBinarioTDA t1, ArbolBinarioTDA t2)
	{
		if (t1.ArbolVacio() && t2.ArbolVacio())
			return true;
		
		if (!t1.ArbolVacio() && !t2.ArbolVacio())
		{
			if (!ArbolesIgualesForma(t1.HijoIzq(), t2.HijoIzq()))
				return false;
			
			if (!ArbolesIgualesForma(t1.HijoDer(), t2.HijoDer()))
				return false;	
			
			return true;
		}
		
		return false;
	}
	
	public static int CantidadHojas(ArbolBinarioTDA raiz)
	{
		IntWrapper hojas = new IntWrapper();
		hojas.value = 0;
		CantidadHojasRec(raiz, hojas);
		return hojas.value;
	}
	
	public static void CantidadHojasRec(ArbolBinarioTDA raiz, IntWrapper hojasActuales)
	{
		if (raiz.ArbolVacio())
			return;
		
		if (raiz.HijoDer().ArbolVacio() && raiz.HijoIzq().ArbolVacio())
			hojasActuales.value++;
		
		CantidadHojasRec(raiz.HijoIzq(), hojasActuales);
		CantidadHojasRec(raiz.HijoDer(), hojasActuales);
	}
	
	public static int SumaElementos(ArbolBinarioTDA raiz)
	{
		IntWrapper suma = new IntWrapper();
		suma.value = 0;
		SumaElementosRec(raiz, suma);
		return suma.value;
	}
	
	public static void SumaElementosRec(ArbolBinarioTDA raiz, IntWrapper suma)
	{
		if (raiz.ArbolVacio())
			return;
		
		suma.value += raiz.Raiz();
		SumaElementosRec(raiz.HijoIzq(), suma);
		SumaElementosRec(raiz.HijoDer(), suma);
	}
	
	public static int AlturaArbol(ArbolBinarioTDA raiz)
	{
		if (raiz.ArbolVacio())
			return 0;
		
		int alturaDerecha = AlturaArbol(raiz.HijoDer());
		int alturaIzquierda = AlturaArbol(raiz.HijoIzq());
		
		int max;
		
		if (alturaDerecha > alturaIzquierda)
			max = alturaDerecha;
		else
			max = alturaIzquierda;
		
		return 1 + max;
	}
	
	public static int ProfElementoRec(int elemento, ArbolBinarioTDA raiz, int profActual)
	{
		if (raiz.ArbolVacio())
			return 0;
		
		int claveRaiz = raiz.Raiz();
		
		if (claveRaiz == elemento)
			return profActual;
		
		if (elemento < claveRaiz)
			return ProfElementoRec(elemento, raiz.HijoIzq(), profActual + 1);
		else if (elemento > claveRaiz)
			return ProfElementoRec(elemento, raiz.HijoDer(), profActual + 1);
		
		return 0;
		
	}
	
	public static Boolean Busqueda(int elemento, ArbolBinarioTDA raiz)
	{
		if (raiz.ArbolVacio())
			return false;
		
		int claveRaiz = raiz.Raiz();
		
		if (claveRaiz == elemento)
			return true;
		
		if (elemento < claveRaiz)
			return Busqueda(elemento, raiz.HijoIzq());
		else if (elemento > claveRaiz)
			return Busqueda(elemento, raiz.HijoDer());
		
		return false;
		
	}
	
	public static Boolean BusquedaDeHoja(int elemento, ArbolBinarioTDA raiz)
	{
		if (raiz.ArbolVacio())
			return false;
		
		int claveRaiz = raiz.Raiz();
		
		if (claveRaiz == elemento)
		{
			return raiz.HijoDer().ArbolVacio() && raiz.HijoIzq().ArbolVacio();
		}
		
		if (elemento < claveRaiz)
			return BusquedaDeHoja(elemento, raiz.HijoIzq());
		else if (elemento > claveRaiz)
			return BusquedaDeHoja(elemento, raiz.HijoDer());
		
		return false;
	}
	
	public static void PreOrden(ArbolBinarioTDA raiz)
	{
		if (raiz.ArbolVacio())
			return;
		
		System.out.print(raiz.Raiz() + ", ");
		PreOrden(raiz.HijoIzq());
		PreOrden(raiz.HijoDer());
	}
	
	public static void InOrder(ArbolBinarioTDA raiz)
	{
		if (raiz.ArbolVacio())
			return;
		
		InOrder(raiz.HijoIzq());
		System.out.print(raiz.Raiz() + ", ");
		InOrder(raiz.HijoDer());
	}
	
	public static void PostOrden(ArbolBinarioTDA raiz)
	{
		if (raiz.ArbolVacio())
			return;
		
		PostOrden(raiz.HijoIzq());
		PostOrden(raiz.HijoDer());
		System.out.print(raiz.Raiz() + ", ");
	}
}
