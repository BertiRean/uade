interface ArbolBinarioTDA 
{
	int Raiz();
	ArbolBinarioTDA HijoIzq();
	ArbolBinarioTDA HijoDer();
	boolean ArbolVacio();
	
	void InicializarArbol();
	void AgregarElemento(int elemento);
	void EliminarElemento(int elemento);
}

class NodoBinario
{
	ArbolBinarioTDA hijoIzq;
	ArbolBinarioTDA hijoDer;
	int elemento;
}

public class ArbolBinarioBusqueda implements ArbolBinarioTDA
{	
	public NodoBinario raiz;
	
	public void InicializarArbol()
	{
		raiz = null;
	}
	
	public int Raiz()
	{
		return raiz.elemento;
	}
	
	public ArbolBinarioTDA HijoIzquierdo()
	{
		return raiz.hijoIzq;
	}
	
	public ArbolBinarioTDA HijoDerecho()
	{
		return raiz.hijoDer;
	}
	
	public int Elemento()
	{
		return raiz.elemento;
	}
	
	public boolean ArbolVacio()
	{
		return raiz == null;
	}
	
	public void AgregarElemento(int elemento)
	{
		if (ArbolVacio())
		{
			raiz = new NodoBinario();
			raiz.elemento = elemento;
			raiz.hijoDer = new ArbolBinarioBusqueda();
			raiz.hijoDer.InicializarArbol();
			raiz.hijoIzq = new ArbolBinarioBusqueda();
			raiz.hijoIzq.InicializarArbol();
		}
		else
		{
			if (elemento < raiz.elemento)
				raiz.hijoIzq.AgregarElemento(elemento);
			else if (elemento > raiz.elemento)
				raiz.hijoDer.AgregarElemento(elemento);
		}
	}
	
	public void EliminarElemento(int elemento)
	{
		if (ArbolVacio())
			return;
		
		if (elemento < raiz.elemento)
			raiz.hijoIzq.EliminarElemento(elemento);
		else if (elemento > raiz.elemento)
			raiz.hijoDer.EliminarElemento(elemento);
		else if (elemento == raiz.elemento)
		{
			// Nodo Hoja
			if (raiz.hijoDer.ArbolVacio() && raiz.hijoIzq.ArbolVacio())
				raiz = null;
			
			// Se sustituye el nodo eliminado por el elemento mayor del ABB
			// es decir se obtiene el mas a la derecha, se copia su valor
			// se agrega a el nodo actual y se elimina el nodo mas a la derecha
			if (!raiz.hijoDer.ArbolVacio())
			{
				raiz.elemento = ObtenerMayor(this.raiz.hijoDer);
				raiz.hijoDer.EliminarElemento(raiz.elemento);
			}
			// Lo mismo que el de arriba pero con el menor elemento del ABB
			else if (!raiz.hijoIzq.ArbolVacio())
			{
				raiz.elemento = ObtenerMenor(this.raiz.hijoIzq);
				raiz.hijoIzq.EliminarElemento(raiz.elemento);
			}
		}
	}
	
	public static int ObtenerMayor(ArbolBinarioTDA raiz)
	{
		if (raiz.HijoDer().ArbolVacio())
			return raiz.Raiz();
		
		return ObtenerMayor(raiz.HijoDer());
	}
	
	public static int ObtenerMenor(ArbolBinarioTDA raiz)
	{
		if (raiz.HijoIzq().ArbolVacio())
			return raiz.Raiz();
		
		return ObtenerMenor(raiz.HijoIzq());
	}

	@Override
	public ArbolBinarioTDA HijoIzq() {
		// TODO Auto-generated method stub
		return raiz.hijoIzq;
	}

	@Override
	public ArbolBinarioTDA HijoDer() {
		// TODO Auto-generated method stub
		return raiz.hijoDer;
	}
	
	public void CopiarArbol(ArbolBinarioTDA raizCopia)
	{
		if (this.ArbolVacio())
			return;
		
		raizCopia.AgregarElemento(raiz.elemento);
		((ArbolBinarioBusqueda) this.HijoIzq()).CopiarArbol(raizCopia.HijoIzq());
		((ArbolBinarioBusqueda) this.HijoDer()).CopiarArbol(raizCopia.HijoDer());
	}
	
	public boolean EsHoja()
	{
		return raiz.hijoDer.ArbolVacio() && raiz.hijoIzq.ArbolVacio();
	}
}