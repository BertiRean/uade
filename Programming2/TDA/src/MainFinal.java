
public class MainFinal 
{
	public static ConjuntoTDA AdyacentesA(int v, GrafoTDA g)
	{
		ConjuntoTDA salientes = new ConjuntoDinamico();
		salientes.Inicializar();
		
		ConjuntoTDA vertices = g.Vertices();
		
		while (!vertices.ConjuntoVacio()) // O(n)
		{
			int vertice = vertices.Elegir();
			
			if (g.ExisteArista(v, vertice)) // O(n)
				salientes.Agregar(vertice);
			
			vertices.Sacar(vertice);
		}
		
		return salientes;
	}
	
	
	/**
	 * Estrategia:
	 * 
	 * Se agrega cada vertice del grafo a el diccionario como clave y como valor 
	 * sera los nodos adyacentes. Si no hay nodos adyacentes se anadira el valor
	 * 0 a el valor de esa clave.
	 * 
	 * Se deben obtener los adyacentes primero a traves de un metodo auxiliar
	 * y en base a el conjunto devuelto se agrega un valor u otro a el valor
	 * de la clave.
	 * 
	 * @param g Grafo de Entrada
	 */
	public static void Grafo2Diccionario(GrafoTDA g)
	{
		ConjuntoTDA vertices = g.Vertices();
		DiccionarioMultipleTDA dict = new DiccionarioMultipleEst();
		dict.Inicializar();
		
		// Total de Tiempo: O(n^3) -> N es el numero de vertices del grafo
		
		while (!vertices.ConjuntoVacio()) // O(n)
		{
			int vertice = vertices.Elegir(); // O(n) o O(1) depende del criterio de eleccion
			
			ConjuntoTDA adyacentes = AdyacentesA(vertice, g); // O(n ^ 2)
			
			if (adyacentes.ConjuntoVacio())
				dict.Agregar(vertice, 0);
			else
			{
				while (!adyacentes.ConjuntoVacio()) // O(n ^ 2) -> Sacar es O(n)
				{
					int nodoAdy = adyacentes.Elegir();
					
					dict.Agregar(vertice, nodoAdy);
					
					adyacentes.Sacar(nodoAdy); // O(n)
				}
			}
			
			vertices.Sacar(vertice); // O(n)
		}
		
		ConjuntoTDA dictClaves = dict.Claves();
		
		while (!dictClaves.ConjuntoVacio())
		{
			int clave = dictClaves.Elegir();
			
			System.out.println("Vertice: " + clave + "\nAdyacentes a el Vertice: \n");
			
			MostrarConjunto(dict.Recuperar(clave));
			
			System.out.println();
			dictClaves.Sacar(clave);
		}
	}
	
	static ConjuntoTDA CopiarConjunto(ConjuntoTDA origen)
	{
		ConjuntoTDA copia = new ConjuntoEst();
		ConjuntoTDA aux = new ConjuntoEst();
		
		copia.Inicializar();
		aux.Inicializar();
		
		while (!origen.ConjuntoVacio())
		{
			int valor = origen.Elegir();
			copia.Agregar(valor);
			aux.Agregar(valor);
			origen.Sacar(valor);
		}
		
		while (!aux.ConjuntoVacio())
		{
			int valor = aux.Elegir();
			origen.Agregar(valor);
			aux.Sacar(valor);
		}
		
		return copia;
	}
	
	public static void MostrarConjunto(ConjuntoTDA conjuntoTDA)
	{
		
		ConjuntoTDA aux = CopiarConjunto(conjuntoTDA);
		
		while (!aux.ConjuntoVacio())
		{
			int valor = aux.Elegir();
			System.out.print(valor + ", ");
			aux.Sacar(valor);
		}
		
		System.out.print('\n');
		
	}
	
	
	public static void InfijoPila(PilaTDA pila, ArbolBinarioTDA raiz)
	{
		if (raiz.ArbolVacio())
			return;
		
		InfijoPila(pila, raiz.HijoIzq());
		pila.Apilar(raiz.Raiz());
		InfijoPila(pila, raiz.HijoDer());
	}
	
	public static void ArbolMayorMenor(ArbolBinarioTDA raiz)
	{
		// Coste O(n)
		PilaTDA pila = new PilaEstatica();
		pila.Inicializar();
		
		InfijoPila(pila, raiz); // O(n)
		
		while (!pila.PilaVacia()) // O(n)
		{
			System.out.print(pila.Tope() + ", ");
			pila.Desapilar();
		}
	}
	
	public static void main(String[] args)
	{
		ArbolBinario raiz = new ArbolBinario();
		raiz.InicializarArbol();
		
		int valores[] = new int[] {10, 8, 11, 5, 9, 13, 12, 14};
		
		ArbolBinario t2 = new ArbolBinario();
		t2.InicializarArbol();
		
		for(int k : valores)
			raiz.AgregarElemento(k);
		
		System.out.println("Arbol Ordenado Mayor Menor");
		ArbolMayorMenor(raiz);
		System.out.println();
		
		GrafoTDA g = new GrafoDinamico();
		g.InicializarGrafo();
		
		g.AgregarVertice(1);
		g.AgregarVertice(2);
		g.AgregarVertice(3);
		
		g.AgregarArista(1, 2, 7);
		g.AgregarArista(2, 3, 9);
		g.AgregarArista(1, 3, 5);
		
		Grafo2Diccionario(g);
	}
	
}
