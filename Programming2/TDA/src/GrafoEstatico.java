interface GrafoTDA
{
	void InicializarGrafo();
	void AgregarVertice(int v);
	void EliminarVertice(int v);
	void AgregarArista(int vInicio, int vFinal, int peso);
	void EliminarArista(int vInicio, int vFinal);
	int PesoArista(int vInicio, int vFinal);
	ConjuntoTDA Vertices();
	Boolean ExisteArista(int vInicio, int vFinal);
}


public class GrafoEstatico implements GrafoTDA
{
	static int n = 100;
	
	int aristas[][]; // Para ver las aristas entre nodos
	int vertices[]; // Mapeo de Indices a Vertices. Aqui cada entrada del arreglo es el valor del nodo.
	int cantNodos;
	
	public void InicializarGrafo()
	{
		cantNodos = 0;
		aristas = new int[n][n];
		vertices = new int[n];
		
		// Una entrada con 0 indica que no hay arista asociada a los nodos en la posicio
		// i,j del grafo.
		for (int i = 0; i < n; ++i)
			for (int j = 0; j < n; ++j)
				aristas[i][j] = 0;
	}
	
	private int Vert2Indice(int v)
	{
		int i = cantNodos - 1;
		
		
		while (i >= 0 && vertices[i] != v)
			i--;
		
		return i;
	}
	
	public void AgregarArista(int vInicio, int vFinal, int peso)
	{
		int idxInicio = Vert2Indice(vInicio);
		int idxFinal  = Vert2Indice(vFinal);
		
		// Los 2 nodos existen
		if (idxInicio >= 0 && idxFinal >= 0)
			aristas[idxInicio][idxFinal] = peso;
	}
	
	public void AgregarVertice(int v)
	{
		int idxVertice = Vert2Indice(v);
		
		// El nodo existe
		if (idxVertice >= 0)
			return;
		
		vertices[cantNodos] = v;
		
		for (int i = 0; i < cantNodos; i++)
		{
			aristas[cantNodos][i] = 0;
			aristas[i][cantNodos] = 0;
		}
		
		cantNodos++;
	}
	
	public void EliminarVertice(int v)
	{
		int idxVertice = Vert2Indice(v);
		
		// Vertice no existe
		if (idxVertice < 0)
			return;
		
		// Elimino las aristas salientes y entrantes del vertice
		// Esto se hace intercambiando el ultimo elemento de la matrix con el elemento actual
		for (int i = 0; i < cantNodos; ++i)
			aristas[i][idxVertice] = aristas[i][cantNodos-1];
		
		for (int i = 0; i < cantNodos; ++i)
			aristas[idxVertice][i] = aristas[cantNodos - 1][i];
		
		aristas[idxVertice][idxVertice] = aristas[cantNodos-1][cantNodos -1];
		vertices[idxVertice] = vertices[cantNodos - 1];
		cantNodos--;
	}
	
	
	public void EliminarArista(int vInicio, int vFinal)
	{
		int idxInicio = Vert2Indice(vInicio);
		int idxFinal = Vert2Indice(vFinal);
		
		// Los nodos existen
		if (idxInicio >= 0 && idxFinal >= 0)
			aristas[idxInicio][idxFinal] = 0;
	}
	
	public int PesoArista(int vInicio, int vFinal)
	{
		int idxInicio = Vert2Indice(vInicio);
		int idxFinal = Vert2Indice(vFinal);
		
		// Los vertices existen
		if (idxInicio >= 0 && idxFinal >= 0 && 
			aristas[idxInicio][idxFinal] != 0)
			return aristas[idxInicio][idxFinal];
		
		return 0;
	}
	
	public Boolean ExisteArista(int vInicio, int vFinal)
	{
		int idxInicio = Vert2Indice(vInicio);
		int idxFinal = Vert2Indice(vFinal);
		
		// Los vertices existen
		if (idxInicio >= 0 && idxFinal >= 0)
			return aristas[idxInicio][idxFinal] != 0;
		
		return false;
	}
	
	public ConjuntoTDA Vertices()
	{
		ConjuntoEst conj = new ConjuntoEst();
		conj.Inicializar();
		
		for (int i = 0; i < cantNodos; ++i)
			conj.Agregar(vertices[i]);
		
		return conj;
	}
}
