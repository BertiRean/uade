
public class MainGrafo 
{
	public static void main(String[] args)
	{
		GrafoTDA g = new GrafoDinamico();
		g.InicializarGrafo();
		
		
		GrafoTDA g2 = new GrafoDinamico();
		g2.InicializarGrafo();
		
		int vertex [] = new int[] {1, 11, 18, 17, 7, 13, 4};
		
		for (int x : vertex)
			g2.AgregarVertice(x);
		
		g2.AgregarArista(1, 13, 2);
		g2.AgregarArista(1, 11, 4);
		g2.AgregarArista(1, 18, 6);
		g2.AgregarArista(1, 17, 8);
		
		g2.AgregarArista(7, 11, 5);
		g2.AgregarArista(7, 18, 3);
		g2.AgregarArista(7, 17, 1);
		g2.AgregarArista(7, 4, 7);
		
		g2.AgregarArista(4, 11, 9);
		g2.AgregarArista(13, 11, 12);
		g2.AgregarArista(1, 4, 56);
		MainConjuntoEstatico.MostrarConjunto(VerticesAdyacentesDe(1, 7, g2));
		
		/*for (int i = 4; i > 0; i--)
			g.AgregarVertice(i);
		
		g.AgregarArista(1, 2, 4);
		g.AgregarArista(1, 3, 3);
		g.AgregarArista(1, 4, 6);
		g.AgregarArista(4, 2, 10);
		g.AgregarArista(2, 3, 8);
		g.AgregarArista(4, 3, 20);
		
		
		System.out.println("Existe Arista entre 1 y 2: " + g.ExisteArista(1, 2));
		System.out.println("Existe Arista entre 1 y 3: " + g.ExisteArista(1, 3));
		System.out.println("Existe Arista entre 1 y 4: " + g.ExisteArista(1, 4));
		System.out.println("Existe Arista entre 4 y 4: " + g.ExisteArista(4, 4));
		System.out.println("Existe Arista entre 4 y 2: " + g.ExisteArista(4, 2));
		System.out.println("Existe Arista entre 2 y 3: " + g.ExisteArista(2, 3));
		System.out.println("Existe Arista entre 2 y 2: " + g.ExisteArista(2, 2));
		System.out.println("Existe Arista entre 4 y 3: " + g.ExisteArista(4, 3));
		
		
		int vertice = 3;
		System.out.println("\nArista de Mayor Peso en Vertice " + vertice + ": " + AristaDeMayorCosto(vertice, g));
		
		System.out.println("\nVertices Adyacentes Dobles: ");
		MainConjuntoEstatico.MostrarConjunto(AdyacentesDobles(g));
		System.out.println("\nFin de Vertices Adyacentes Dobles");
		
		
		System.out.println("\nVertices Puentes: ");
		MainConjuntoEstatico.MostrarConjunto(VerticesPuente(g, 1, 3));
		System.out.println("\nFin de Vertices Puentes");
		
		
		System.out.println("Predecesores de vertice: " + vertice);
		MainConjuntoEstatico.MostrarConjunto(PredecesoresDeVertice(vertice, g));
		
		
		System.out.println("\n\nVertices Aislados: ");
		MainConjuntoEstatico.MostrarConjunto(VerticesAislados(g));
		System.out.println("\n\nFin Vertices Aislados");
		
		System.out.println("\n\nEliminando Vertice con Valor: " + vertice );
		g.EliminarVertice(vertice);
		
		System.out.println("Existe Arista entre 4 y 2: " + g.ExisteArista(4, 2));
		System.out.println("Existe Arista entre 1 y 2: " + g.ExisteArista(1, 2));
		System.out.println("Existe Arista entre 1 y 4: " + g.ExisteArista(1, 3));
		System.out.println("Existe Arista entre 1 y 3: " + g.ExisteArista(1, 4));
		
		MainConjuntoEstatico.MostrarConjunto(g.Vertices());*/
		
	}
	
	public static ConjuntoTDA VerticesAdyacentesDe(int v1, int v2, GrafoTDA g)
	{
		ConjuntoTDA adyacentes = new ConjuntoDinamico();
		adyacentes.Inicializar();
		
		// Estrategia
		/*
		 * Se obtienen todos los vertices, se itera sobre cada uno y se verifica si hay una arista entre v1 y el vertice
		 * y entre v2 y el vertice, si existen en ambos casos, se agrega al conjunto a devolver.
		 * 
		 * 
		 * */
		
		ConjuntoTDA vertices = g.Vertices();
		
		while (!vertices.ConjuntoVacio()) // Coste O(n)
		{
			int vertice = vertices.Elegir(); // Coste O(1) Seleccionando el Primero o Ultimo elemento siempre,
			// Si es dinamica la implementacion entonces Elegir puede ser O(1) o O(n) dependiendo del criterio de seleccion
			
			
			/*
			 * Si es un grafo Estatico ExisteArista es O(1), si es dinamico es O(k), donde k
			 * es el numero de aristas salientes de v1 o v2
			 * 
			 * */
			if (g.ExisteArista(v1, vertice) && g.ExisteArista(v2, vertice))
				adyacentes.Agregar(vertice);
			
			vertices.Sacar(vertice); // O(n), en el peor de los casos
		}
		
		// Coste Total: O(n) * O(1) * O(1) * O(n) = O(n^2) // Grafo Impl Estatica
		// 				O(n) * O(1) * O(k) * O(n) = O(n^2*k) // Grafo Impl Dinamica
		// Recuerde que como se implementa Elegir y Sacar puede hacer algunas optimizaciones en los tiempos.
		
		return adyacentes;
	}
	
	public static ConjuntoTDA VerticesSalientesDe(int v, GrafoTDA g)
	{
		ConjuntoTDA salientes = new ConjuntoDinamico();
		salientes.Inicializar();
		
		ConjuntoTDA vertices = g.Vertices();
		
		while (!vertices.ConjuntoVacio())
		{
			int vertice = vertices.Elegir();
			
			if (g.ExisteArista(v, vertice))
				salientes.Agregar(vertice);
			
			vertices.Sacar(vertice);
		}
		
		return salientes;
	}
	
	public static ConjuntoTDA VerticesEntrantesEn(int v, GrafoTDA g)
	{
		ConjuntoTDA entrantes = new ConjuntoDinamico();
		entrantes.Inicializar();
		
		ConjuntoTDA vertices = g.Vertices();
		
		while (!vertices.ConjuntoVacio())
		{
			int vertice = vertices.Elegir();
			
			if (g.ExisteArista(vertice, v))
				entrantes.Agregar(vertice);
			
			vertices.Sacar(vertice);
		}
		
		return entrantes;
	}
	
	public static ConjuntoTDA PredecesoresDeVertice(int v, GrafoTDA g)
	{
		ConjuntoTDA predecesores = new ConjuntoDinamico();
		predecesores.Inicializar();
		ConjuntoTDA vertices = g.Vertices();
		
		while (!vertices.ConjuntoVacio())
		{
			int vert = vertices.Elegir();
			
			if (g.ExisteArista(vert, v))
				predecesores.Agregar(vert);
			
			vertices.Sacar(vert);
		}
		
		return predecesores;
	}
	
	public static int AristaDeMayorCosto(int v, GrafoTDA g)
	{
		ConjuntoTDA vertices = g.Vertices();
		
		int mayor = 0;
		
		while (!vertices.ConjuntoVacio())
		{
			int vert = vertices.Elegir();
			
			int peso = g.PesoArista(v, vert);
			
			if (peso > mayor)
				mayor = peso;
			
			vertices.Sacar(vert);
		}
		
		return mayor;
	}
	
	public static ConjuntoTDA AdyacentesDobles(GrafoTDA g)
	{
		ConjuntoTDA vertices = g.Vertices();
		ConjuntoTDA dobles = new ConjuntoDinamico();
		dobles.Inicializar();
		
		while (!vertices.ConjuntoVacio())
		{
			int v = vertices.Elegir();
			ConjuntoTDA copia = g.Vertices();
			
			while (!copia.ConjuntoVacio())
			{
				int x = copia.Elegir();
				ConjuntoTDA copia2 = g.Vertices();
				
				while (!copia2.ConjuntoVacio())
				{
					int w = copia2.Elegir();
					
					if (g.ExisteArista(v, x) && g.ExisteArista(x, w))
						dobles.Agregar(w);
					
					copia2.Sacar(w);
				}
				
				copia.Sacar(x);
			}
			
			vertices.Sacar(v);
		}
		
		return dobles;
	}
	
	public static ConjuntoTDA VerticesPuente(GrafoTDA g, int v1, int v2)
	{
		ConjuntoTDA vertices = g.Vertices();
		ConjuntoTDA puentes = new ConjuntoDinamico();
		puentes.Inicializar();
		
		while (!vertices.ConjuntoVacio())
		{
			int puente = vertices.Elegir();
			
			if (g.ExisteArista(v1, puente) && 
				g.ExisteArista(puente, v2))
				puentes.Agregar(puente);
			
			vertices.Sacar(puente);
		}
		
		return puentes;
	}
	
	public static ConjuntoTDA VerticesAislados(GrafoTDA g)
	{
		ConjuntoTDA aislados = g.Vertices();
		ConjuntoTDA vertices = g.Vertices();
		
		// Partimos de la idea que todos los vertices estan aislados,
		// estos estan en un conjunto. Si encontramos una arista que entre o salga de
		// algunos de los vertices lo eliminamos del conjunto de aislados.
		
		while (!vertices.ConjuntoVacio())
		{
			ConjuntoTDA conjCopia = g.Vertices();
			int vertice = vertices.Elegir();
			
			conjCopia.Sacar(vertice);
			
			while (!conjCopia.ConjuntoVacio())
			{
				int auxVertex = conjCopia.Elegir();
				
				if (g.ExisteArista(vertice, auxVertex) || 
					g.ExisteArista(auxVertex, vertice))
				{
					aislados.Sacar(vertice);
					aislados.Sacar(auxVertex);
				}
				
				conjCopia.Sacar(auxVertex);
			}	
				
			vertices.Sacar(vertice);
		}
		
		return aislados;
	}
}
