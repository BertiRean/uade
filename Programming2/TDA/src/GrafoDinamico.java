class NodoGrafo
{
	int vertice;
	NodoGrafo sgtNodo;
	NodoArista aristas;
}

class NodoArista
{
	int etiqueta;
	NodoGrafo nodoDestino;
	NodoArista sgt;
}


public class GrafoDinamico implements GrafoTDA
{
	NodoGrafo nodoOrigen;

	@Override
	public void InicializarGrafo() 
	{
		// TODO Auto-generated method stub
		nodoOrigen = null;
	}

	@Override
	public void AgregarVertice(int v) 
	{
		// TODO Auto-generated method stub
		if (ExisteVertice(v))
			return;
		
		NodoGrafo nodoNuevo = new NodoGrafo();
		nodoNuevo.vertice = v;
		nodoNuevo.aristas = null;
		nodoNuevo.sgtNodo = nodoOrigen;
		nodoOrigen = nodoNuevo;
	}
	
	private NodoGrafo Vert2Nodo(int v)
	{
		NodoGrafo cursor = nodoOrigen;
		
		while (cursor != null && cursor.vertice != v)
			cursor = cursor.sgtNodo;
		
		return cursor;
	}

	@Override
	public void EliminarVertice(int v) 
	{
		// TODO Auto-generated method stub
		
		// Si el vertice es el primer elemento
		if (nodoOrigen != null && nodoOrigen.vertice == v)
		{
			nodoOrigen = nodoOrigen.sgtNodo;
			return;
		}
		
		NodoGrafo cursor = nodoOrigen;
		
		while (cursor != null)
		{
			this.EliminarAristaNodo(cursor, v);
			
			// Nodo Encontrado
			if (cursor.sgtNodo != null && cursor.sgtNodo.vertice == v)
				cursor.sgtNodo = cursor.sgtNodo.sgtNodo;
			
			cursor = cursor.sgtNodo;
		}
	}
	
	private void EliminarAristaNodo(NodoGrafo nodoOrigen, int v)
	{
		NodoArista cursorArista = nodoOrigen.aristas;
		
		// No hay aristas
		if (cursorArista == null)
			return;
		
		NodoArista anterior = null;
		NodoArista actual = cursorArista;
		
		// Buscamos la arista
		while (actual != null && actual.nodoDestino.vertice != v)
		{
			anterior = actual;
			actual = actual.sgt;
		}
		
		// Si la arista esta en el principio de la lista enlazada
		if (actual != null && anterior == null)
			nodoOrigen.aristas = actual.sgt;
		
		// Se encontro la arista
		if (actual != null && anterior != null)
			anterior.sgt = actual.sgt;
	}

	@Override
	public void AgregarArista(int vInicio, int vDestino, int peso) 
	{
		// TODO Auto-generated method stub
		NodoGrafo nodoInicio 	= Vert2Nodo(vInicio);
		NodoGrafo nodoFinal 	= Vert2Nodo(vDestino);
		
		// Si existen los nodos
		if (nodoInicio != null && nodoFinal != null)
		{
			NodoArista arista = new NodoArista();
			arista.etiqueta = peso;
			arista.nodoDestino = nodoFinal;
			arista.sgt = nodoInicio.aristas;
			nodoInicio.aristas = arista;
		}
	}

	@Override
	public void EliminarArista(int vInicio, int vDestino) 
	{
		// TODO Auto-generated method stub
		NodoGrafo nodoInicio = Vert2Nodo(vInicio);
		this.EliminarAristaNodo(nodoInicio, vDestino);
	}

	@Override
	public int PesoArista(int vInicio, int vDestino) 
	{
		// TODO Auto-generated method stub
		NodoGrafo nodoInicio = Vert2Nodo(vInicio);
		
		NodoArista cursor = nodoInicio.aristas;
		
		while (cursor != null && cursor.nodoDestino.vertice != vDestino)
			cursor = cursor.sgt;
		
		if (cursor != null)
			return cursor.etiqueta;
		
		return 0;
	}

	@Override
	public ConjuntoTDA Vertices() 
	{
		ConjuntoTDA conj = new ConjuntoDinamico();
		conj.Inicializar();
		
		NodoGrafo cursor = nodoOrigen;
		
		while (cursor != null)
		{
			conj.Agregar(cursor.vertice);
			cursor = cursor.sgtNodo;
		}
		
		return conj;
	}

	@Override
	public Boolean ExisteArista(int vInicio, int vFinal) 
	{
		// TODO Auto-generated method stub
		NodoGrafo nInicio = Vert2Nodo(vInicio);
		
		if (nInicio == null)
			return false;
		
		NodoArista cursor = nInicio.aristas;
		
		while (cursor != null && cursor.nodoDestino.vertice != vFinal)
			cursor = cursor.sgt;
		
		return cursor != null;
	}
	
	private Boolean ExisteVertice(int v)
	{
		NodoGrafo cursor = nodoOrigen;
		
		while (cursor != null && cursor.vertice != v)
			cursor = cursor.sgtNodo;
		
		return cursor != null && cursor.vertice == v;
	}
	
}
