/**
 * 
 * @author Antonio Martin Berti Plaza
 * DNI: 95.930.018
 * LU: 1127084
 */
public class MainParcialGrafos 
{
	static ConjuntoTDA CopiarConjunto2(ConjuntoTDA origen)
	{
		ConjuntoTDA copia = new ConjuntoEst();
		ConjuntoTDA aux = new ConjuntoEst();
		
		copia.Inicializar();
		aux.Inicializar();
		
		while (!origen.ConjuntoVacio())
		{
			int valor = origen.Elegir();
			copia.Agregar(valor);
			aux.Agregar(valor);
			origen.Sacar(valor);
		}
		
		while (!aux.ConjuntoVacio())
		{
			int valor = aux.Elegir();
			origen.Agregar(valor);
			aux.Sacar(valor);
		}
		
		return copia;
	}
	
	public static void MostrarConjunto(ConjuntoTDA conjuntoTDA)
	{
		System.out.print("\nMostrar\n");
		ConjuntoTDA aux = CopiarConjunto2(conjuntoTDA);
		
		while (!aux.ConjuntoVacio())
		{
			int valor = aux.Elegir();
			System.out.print(valor + ", ");
			aux.Sacar(valor);
		}
		
		System.out.print('\n');
		System.out.print("Fin de Mostrar");
	}
	
	public static void main(String[] args)
	{
		GrafoTDA g2 = new GrafoDinamico();
		g2.InicializarGrafo();
		
		int vertex [] = new int[] {1, 11, 18, 17, 7, 13, 4};
		
		for (int x : vertex)
			g2.AgregarVertice(x);
		
		g2.AgregarArista(1, 13, 2);
		g2.AgregarArista(1, 11, 4);
		g2.AgregarArista(1, 18, 6);
		g2.AgregarArista(1, 17, 8);
		
		g2.AgregarArista(7, 11, 5);
		g2.AgregarArista(7, 18, 3);
		g2.AgregarArista(7, 17, 1);
		g2.AgregarArista(7, 4, 7);
		
		g2.AgregarArista(4, 11, 9);
		g2.AgregarArista(13, 11, 12);
		g2.AgregarArista(1, 4, 56);
		
		MostrarConjunto(VerticesAdyacentesDe(1, 7, g2));
		System.out.println("\n");
		
		ArbolBinario t1 = new ArbolBinario();
		t1.InicializarArbol();
		
		int valores[] = new int[] {10, 8, 11, 5, 9, 13, 12, 14};
		
		ArbolBinario t2 = new ArbolBinario();
		t2.InicializarArbol();
		
		int valores2[] = new int[] {10, 8, 5, 9, 11, 14, 13, 15 };
		
		for(int k : valores)
			t1.AgregarElemento(k);
		
		for (int k : valores2)
			t2.AgregarElemento(k);
		
		System.out.println("Arboles Iguales Forma: " + ArbolesSimilares(t1, t2) + "\n");
		
		ArbolBinario t3 = new ArbolBinario();
		t3.InicializarArbol();
		
		int valores3[] = new int[] {10, 8, 12, 5, 9, 11, 14, 13, 15 };
		
		for(int k : valores3)
			t3.AgregarElemento(k);
		
		System.out.println("Arboles Iguales Forma: " + ArbolesSimilares(t1, t3) + "\n");
	}
	
	public static ConjuntoTDA VerticesAdyacentesDe(int v1, int v2, GrafoTDA g)
	{
		ConjuntoTDA adyacentes = new ConjuntoDinamico();
		adyacentes.Inicializar();
		
		// Estrategia
		/*
		 * Se obtienen todos los vertices, se itera sobre cada uno y se verifica si hay una arista entre v1 y el vertice
		 * y entre v2 y el vertice, si existen en ambos casos, se agrega al conjunto a devolver.
		 * 
		 * 
		 * */
		
		ConjuntoTDA vertices = g.Vertices();
		
		while (!vertices.ConjuntoVacio()) // Coste O(n)
		{
			int vertice = vertices.Elegir(); // Coste O(1) Seleccionando el Primero o Ultimo elemento siempre,
			// Si es dinamica la implementacion entonces Elegir puede ser O(1) o O(n) dependiendo del criterio de seleccion
			
			
			/*
			 * Si es un grafo Estatico ExisteArista es O(1), si es dinamico es O(k), donde k
			 * es el numero de aristas salientes de v1 o v2
			 * 
			 * */
			if (g.ExisteArista(v1, vertice) && g.ExisteArista(v2, vertice))
				adyacentes.Agregar(vertice);
			
			vertices.Sacar(vertice); // O(n), en el peor de los casos
		}
		
		// Coste Total: O(n) * O(1) * O(1) * O(n) = O(n^2) // Grafo Impl Estatica
		// 				O(n) * O(1) * O(k) * O(n) = O(n^2*k) // Grafo Impl Dinamica
		// Recuerde que como se implementa Elegir y Sacar puede hacer algunas optimizaciones en los tiempos.
		
		return adyacentes;
	}
	
	public static boolean ArbolesSimilares(ArbolBinarioTDA t1, ArbolBinarioTDA t2)
	{
		// Estrategia:
		/*
		 *  Dos Arboles son sumilares, si sus ramas derecha e izquierda son similares tambien
		 * Si dos arboles son vacios ambos son similares,
		 * Si sus raices no son nulas, entonces seran similares si sus ramas izquierda y derecha son similares en forma.
		 * Si 1 arbol esta vacio y el otro no, entonces no son similares.
		 * Este codigo es basado en el recorrido prefijo o pre orden. Es como tener dos punteros que van moviendose
		 * en paralelo a lo largo de los dos arboles de entrada.
		 * */
		if (t1.ArbolVacio() && t2.ArbolVacio())
			return true;
		
		if (!t1.ArbolVacio() && !t2.ArbolVacio())
		{
			if (!ArbolesSimilares(t1.HijoIzq(), t2.HijoIzq()))
				return false;
			
			if (!ArbolesSimilares(t1.HijoDer(), t2.HijoDer()))
				return false;	
			
			return true;
		}
		
		return false;
	}

}
