interface DiccionarioMultipleTDA
{
	void Inicializar();
	void Agregar(int clave, int valor);
	void Eliminar(int clave);
	ConjuntoEst Recuperar(int clave);
	ConjuntoEst Claves();
}


public class DiccionarioMultipleEst implements DiccionarioMultipleTDA
{
	class Elemento
	{
		ConjuntoEst valores;
		int clave;
	}
	
	Elemento[] elementos;
	int cant;
	
	private int Clave2Ind (int clave) 
	{
		int i = cant-1;
		
		while (i >= 0 && elementos[i].clave != clave)
			i--;
		
		return i;
	}
	
	public void Inicializar()
	{
		elementos = new Elemento[100];
		cant = 0;
	}
	
	public void Agregar(int clave, int valor)
	{
		int pos = Clave2Ind(clave);
		 
		 if (pos != -1) // Clave encontrada
		 {
			 elementos[pos].valores.Agregar(valor);
		 }
		 else
		 {
			 pos = cant;
			 Elemento e = new Elemento();
			 e.clave = clave;
			 e.valores = new ConjuntoEst();
			 e.valores.Inicializar();
			 e.valores.Agregar(valor);
			 elementos[pos] = e; 
			 cant++;
		 }
	}
	
	public void Eliminar(int clave)
	{
		int pos = Clave2Ind(clave);
		
		if (pos != -1)
		{
			elementos[pos] = elementos[cant - 1];
			cant--;
		}
	}

	public ConjuntoEst Recuperar(int clave) 
	{
		int pos = Clave2Ind(clave);
		
		if (pos != -1)
			return elementos[pos].valores;
		
		return null;
	}
	
	public ConjuntoEst Claves()
	{
		ConjuntoEst claves = new ConjuntoEst();
		claves.Inicializar();
		
		for (int i = 0; i < cant; ++i)
			claves.Agregar(elementos[i].clave);
		
		return claves;
	}
}
