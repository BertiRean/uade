import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

import javax.swing.WindowConstants;
import javax.swing.SwingUtilities;


/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class MenuPrincipal extends javax.swing.JFrame 
{
	private JButton crearAutoDeportivo;
	private JButton crearAutoSedan;
	private JButton alquilarAuto;
	private JButton salir;

	public static void main(String[] args) 
	{
		MenuPrincipal inst = new MenuPrincipal();
		inst.setLocationRelativeTo(null);
		inst.setVisible(true);
	}
	
	public MenuPrincipal() 
	{
		super();
		initGUI();
	}
	
	private void initGUI()
	{
		try
		{
			getContentPane().setLayout(null);
			crearAutoDeportivo = new JButton();
			getContentPane().add(crearAutoDeportivo);
			crearAutoDeportivo.setText("Alta Auto Deportivo");
			crearAutoDeportivo.setBounds(88, 46, 237, 23);
			crearAutoDeportivo.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent evt) 
				{
				
				}
			});
			
			crearAutoSedan = new JButton();
			getContentPane().add(crearAutoSedan);
			crearAutoSedan.setText("Alta Auto Sedan ");
			crearAutoSedan.setBounds(88, 109, 237, 23);
			crearAutoSedan.addActionListener(new ActionListener() 
			{
				public void actionPerformed(ActionEvent evt) 
				{
				
				}
			});
			
			salir = new JButton();
			getContentPane().add(salir);
			salir.setText("Salir del Sistema");
			salir.setBounds(88, 200, 237, 23);
			{
				alquilarAuto = new JButton();
				getContentPane().add(alquilarAuto);
				alquilarAuto.setText("Alquilar Auto");
				alquilarAuto.setBounds(88, 152, 232, 21);
				alquilarAuto.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent evt) {
						System.out.println("alquilarAuto.actionPerformed, event="+evt);
						//TODO add your code for alquilarAuto.actionPerformed
					}
				});
			}
			salir.addActionListener(new ActionListener() 
			{
				public void actionPerformed(ActionEvent evt) 
				{
				
				}
			});
			
			setSize(400, 300);
		} 
		catch (Exception e) 
		{
		    //add your error handling code here
			e.printStackTrace();
		}
	}

}
