package Negocio;

import java.util.Date;

public class PuntajeJugador implements Comparable<PuntajeJugador>
{
	private String nombreJugador;
	private Date fechaLogro;
	private int puntaje;
	
	public PuntajeJugador(String nombre, Date fecha, int puntos)
	{
		this.nombreJugador = nombre;
		this.fechaLogro = fecha;
		this.puntaje = puntos;
	}
	
	public PuntajeJugador getCopy()
	{
		return new PuntajeJugador(this.nombreJugador, this.fechaLogro, this.puntaje);
	}
	
	public int verPuntaje() { return this.puntaje; }
	public String verNombre() { return this.nombreJugador; }

	@Override
	public int compareTo(PuntajeJugador rhs) {
		// TODO Auto-generated method stub
		return rhs.puntaje - this.puntaje;
	}
}
