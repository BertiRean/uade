package Negocio;

public class Barra 
{
	private static final int BARRA_VELOCIDAD_X = 1;
	private int velocidad;
	
	private RectColision areaCol;
	private Posicion2D pos;
	
	private int anchoTablero;
	
	public Barra()
	{
		areaCol = new RectColision(0, 0, 0, 0);
		pos = new Posicion2D(0, 0);
		velocidad = 0;
		this.anchoTablero = 0;
	}
	
	public Barra(int posX, int posY, int anchoCol, int largoCol, int anchoTablero)
	{
		areaCol = new RectColision(posX, posY, anchoCol, largoCol);
		pos = new Posicion2D(posX, posY);
		velocidad = 0;
		this.anchoTablero = anchoTablero;
	}
	
	public void actualizarPosicion(long deltaT)
	{
		int newX = this.pos.getX();
		newX += velocidad * deltaT;
		
		this.pos.setX(newX);
		this.areaCol.getCentro().setX(newX);
		
		int ancho = this.areaCol.getAncho();
		int bordeDerecho = this.areaCol.bordeDerecho();
		int bordeIzquierdo = this.areaCol.bordeIzquierdo();
		
		if (bordeIzquierdo <= 10 && velocidad < 0)
		{
			velocidad = 0;
			newX = 0 + ancho/2 + 10;
			this.pos.setX(newX);
			this.areaCol.getCentro().setX(newX);
		}
		
		if (bordeDerecho >= anchoTablero - 30 && velocidad > 0)
		{
			velocidad = 0;
			newX = anchoTablero - ancho/2 - 10;
			this.pos.setX(newX);
			this.areaCol.getCentro().setX(newX);
		}
	}
	
	public void detenerBarra()
	{
		this.velocidad = 0;
	}
	
	public boolean estaFueraDeBorde()
	{
		if (this.areaCol.bordeDerecho() > anchoTablero && velocidad > 0)
			return true;
		
		
		if (this.areaCol.bordeIzquierdo() < 0 && velocidad < 0)
			return true;
		
		return false;
	}
	
	public void moverIzquierda()
	{
		if (this.estaFueraDeBorde())
			velocidad = 0;
		else
			velocidad = -BARRA_VELOCIDAD_X;
	}
	
	public void moverDerecha()
	{
		if (this.estaFueraDeBorde())
			velocidad = 0;
		else
			velocidad = BARRA_VELOCIDAD_X;
	}
	
	public RectColision getAreaColision()
	{
		return this.areaCol;
	}

	public ObjetoJuegoView getBarraView() 
	{
		// TODO Auto-generated method stub
		Posicion2D topLeft = this.areaCol.superiorIzquierda();
		return new ObjetoJuegoView(topLeft.getX(), topLeft.getY(), this.areaCol.getAncho(), this.areaCol.getLargo());
	}

}
