package Negocio;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Random;

public class TableroDePuntajes 
{
	private ArrayList<PuntajeJugador> puntajes;
	
	public TableroDePuntajes()
	{
		this.puntajes = new ArrayList<PuntajeJugador>();
		genRandomScores();
	}
	
	public void genRandomScores()
	{
		Random r = new Random();
		int count = 0;
		
		this.puntajes.clear();
		while (count < 20)
		{
			PuntajeJugador p = new PuntajeJugador("ABC", Date.from(Instant.now()), r.nextInt(100));
			this.puntajes.add(p);
			count += 1;
		}
		
		Collections.sort(this.puntajes);
	}
	
	public int getMinPuntaje()
	{
		int min = -1000000;
		
		for (PuntajeJugador itr : this.puntajes)
		{
			if (itr.verPuntaje() < min)
				min = itr.verPuntaje();
		}
		
		return min;
	}
	
	public String[] getTop20()
	{
		ArrayList<String> puntosStr = new ArrayList<String>();
		
		Collections.sort(this.puntajes);
		
		for (int i = 0; i < this.puntajes.size() && i < 20; ++i)
		{
			String str = "";
			str = this.puntajes.get(i).verNombre() + " ------ " + this.puntajes.get(i).verPuntaje();
			puntosStr.add(new String(str));
		}
		
		return puntosStr.toArray(new String[20]);
	}
	
	public void agregarPuntaje(String nombreJugador, int puntos)
	{
		Date fecha = Date.from(Instant.now());
		
		PuntajeJugador puntaje = new PuntajeJugador(nombreJugador, fecha, puntos);
		this.puntajes.add(puntaje);
		Collections.sort(this.puntajes);
	}
}
