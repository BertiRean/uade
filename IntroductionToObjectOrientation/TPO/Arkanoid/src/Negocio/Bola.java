package Negocio;

import java.util.Random;

public class Bola 
{
	private static final float ANGULO_BLOQUE_MIN = 1.4835f;
	private static final float ANGULO_BLOQUE_MAX = 1.6580f;
	private static final float ANGULO_BARRA_DERECHA 	= 1.0471f;
	private static final float ANGULO_BARRA_IZQUIERDA 	= 1.5707f;
	
	private RectColision areaCol;
	private Posicion2D pos;
	
	private float velocidadX;
	private float velocidadY;
	private float velocidadMultiplo;
	
	private int largoTablero;
	private int anchoTablero;
	
	public Bola(int posX, int posY, int anchoColision, int largoColision)
	{
		areaCol = new RectColision(posX, posY, anchoColision, largoColision);
		pos = new Posicion2D(posX, posY);
		
		velocidadX = 1;
		velocidadY = -1;
		velocidadMultiplo = 0.85f;
	}
	
	public void rebotar(Ladrillo bloque)
	{
		Random r = new Random();
		double theta = r.doubles(1, ANGULO_BLOQUE_MIN, ANGULO_BLOQUE_MAX).toArray()[0];
		
		double direccion = Math.atan2(velocidadY, velocidadX);
		
		direccion += theta;
		
		velocidadX = (float) (velocidadMultiplo * Math.cos(direccion));
		velocidadY = (float) (velocidadMultiplo * Math.sin(direccion));
	}
	
	public void rebotar(Barra barra)
	{
		float theta = (float) Math.atan2(velocidadY, velocidadX);
		boolean estaIzquierda = false;
		int cuadrante = this.obtenerCuadranteDireccion();
		
		estaIzquierda = this.pos.getX() < barra.getAreaColision().getCentro().getX();
		
		if (cuadrante == 1)
			theta += estaIzquierda ? -ANGULO_BARRA_IZQUIERDA : -ANGULO_BARRA_DERECHA;
		else
			theta += estaIzquierda ? ANGULO_BARRA_IZQUIERDA : ANGULO_BARRA_DERECHA;
		
		
		velocidadX = (float) (velocidadMultiplo * Math.cos(theta));
		velocidadY = (float) (velocidadMultiplo * Math.sin(theta));
	}
	
	public void actualizarPosicion(long deltaT)
	{
		int x = pos.getX();
		int y = pos.getY();
		
		x += (velocidadX * velocidadMultiplo * deltaT);
		y += (velocidadY * velocidadMultiplo * deltaT);
		
		pos.setX(x);
		pos.setY(y);
		
		this.areaCol.getCentro().setX(x);
		this.areaCol.getCentro().setY(y);
		
		int cuadrante = obtenerCuadranteDireccion();
		if (areaCol.bordeIzquierdo() < 0)
		{
			
			if (cuadrante == 3)
			{
				velocidadX = (float) (velocidadMultiplo * Math.cos(7 * Math.PI/4));
				velocidadY = (float) (velocidadMultiplo * Math.sin(7 * Math.PI/4));
			}
			else if (cuadrante == 2)
			{
				velocidadX = (float) (velocidadMultiplo * Math.cos(Math.PI/4));
				velocidadY = (float) (velocidadMultiplo * Math.sin(Math.PI/4));
			}
		}
		
		if (areaCol.bordeDerecho() > anchoTablero)
		{
			if (cuadrante == 4)
			{
				velocidadX = (float) (velocidadMultiplo * Math.cos(Math.PI/4));
				velocidadY = (float) (velocidadMultiplo * Math.sin(Math.PI/4));
			}
			else if (cuadrante == 1)
			{
				velocidadX = (float) (velocidadMultiplo * Math.cos(3 * Math.PI/4));
				velocidadY = (float) (velocidadMultiplo * Math.sin(3 * Math.PI/4));
			}
		}
		
		if (areaCol.bordeSuperior() < 0)
		{
			if (cuadrante == 4)
			{
				velocidadX = (float) (velocidadMultiplo * Math.cos(Math.PI/4));
				velocidadY = (float) (velocidadMultiplo * Math.sin(Math.PI/4));
			}
			else if (cuadrante == 3)
			{
				velocidadX = (float) (velocidadMultiplo * Math.cos(3 * Math.PI/4));
				velocidadY = (float) (velocidadMultiplo * Math.sin(3 * Math.PI/4));
			}
		}
	}
	
	private int obtenerCuadranteDireccion()
	{
		if (velocidadX > 0 && velocidadY > 0)
			return 1;
		
		if (velocidadX < 0 && velocidadY > 0)
			return 2;
		
		if (velocidadX < 0 && velocidadY < 0)
			return 3;
		
		if (velocidadX > 0 && velocidadY < 0)
			return 4;
		
		return 0;
	}
	
	public void setVelocidadX(int pVelX)
	{
		this.velocidadX = pVelX;
	}
	
	public void setVelocidadY(int pVelY)
	{
		this.velocidadY = pVelY;
	}
	
	public void setBordesTablero(int pAnchoTablero, int pLargoTablero)
	{
		this.largoTablero = pLargoTablero;
		this.anchoTablero = pAnchoTablero;
	}
	
	public Boolean estaFueraDelTablero()
	{
		return pos.getY() >= largoTablero;
	}
	
	public Boolean estaColisionandoCon(RectColision rect)
	{
		return rect.estaColisionandoCon(this.areaCol);
	}

	public void incrementarVelocidad() {
		// TODO Auto-generated method stub
		velocidadMultiplo *= 1.0025;
	}

	public ObjetoJuegoView getBolaView() {
		// TODO Auto-generated method stub
		Posicion2D topLeft = this.areaCol.superiorIzquierda();
		
		return new ObjetoJuegoView(topLeft.getX(), topLeft.getY(), this.areaCol.getAncho(), this.areaCol.getLargo());
	}
}
