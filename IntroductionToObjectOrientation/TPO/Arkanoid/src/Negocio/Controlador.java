package Negocio;

import GUI.ArkanoidMainWindow;

public class Controlador 
{
	private TableroJuego tablero;
	private TableroDePuntajes puntajeTablero;
	
	public boolean estaEnTableroPuntaje;
	
	public Controlador()
	{
		this.estaEnTableroPuntaje = false;
		this.puntajeTablero = new TableroDePuntajes();
	}
	
	public void setEstaTableroPuntaje(boolean p)
	{
		this.estaEnTableroPuntaje = p;
	}
	
	public boolean estaEnTableroPuntaje()
	{
		return this.estaEnTableroPuntaje;
	}
	
	public boolean esFinDeJuego()
	{
		return this.tablero.esFinDeJuego();
	}
	
	public void actualizarObjetos(long deltaT)
	{
		tablero.actualizar(deltaT);
	}
	
	public void teclaEspacioPresionada()
	{
		if (this.estaEnTableroPuntaje())
			this.setEstaTableroPuntaje(false);
		
		if (!this.tablero.estaIniciadaLaPartida() || this.tablero.esFinDeJuego())
			this.tablero.inicializarTablero();
		else
		{
			if (this.tablero.estaPausado())
				this.tablero.pausar(false);
			else if (this.tablero.estaNivelEnProgreso())
				this.tablero.pausar(true);
			else if (this.tablero.estaEsperandoInicio())
				this.tablero.iniciarPartida();
			else if (this.tablero.estaNivelCompletado())
				this.tablero.avanzarDeNivel();
		}
	}
	
	public void flechaDerechaPresionada()
	{
		tablero.moverBarra(true);
	}
	
	public void flechaIzquierdaPresionada()
	{
		tablero.moverBarra(false);
	}

	public void inicializarTablero() 
	{
		// TODO Auto-generated method stub
		int ancho = ArkanoidMainWindow.SCREEN_WIDTH;
		int alto = ArkanoidMainWindow.SCREEN_HEIGHT;
		
		tablero = new TableroJuego(ancho, alto);
		tablero.inicializarTablero();
	}

	public void detenerBarra() 
	{
		// TODO Auto-generated method stub
		tablero.detenerBarra();
	}
	
	public boolean entraEnRanking()
	{
		int puntajeActual = this.tablero.getPuntaje();
		int minPuntaje = this.puntajeTablero.getMinPuntaje();
		
		return puntajeActual > minPuntaje;
	}

	public void agregarPuntaje(String nombreJugador) {
		this.puntajeTablero.agregarPuntaje(nombreJugador, this.tablero.getPuntaje());
		// TODO Auto-generated method stub
		
	}

	public ObjetoJuegoView getBolaView() {
		// TODO Auto-generated method stub
		return this.tablero.getBolaView();
	}

	public ObjetoJuegoView getBarraView() {
		// TODO Auto-generated method stub
		return this.tablero.getBarraView();
	}

	public ObjetoJuegoView[] getBloquesViews() {
		// TODO Auto-generated method stub
		return this.tablero.getBloquesViews();
	}

	public int getPuntaje() {
		// TODO Auto-generated method stub
		return this.tablero.getPuntaje();
	}

	public int getVidas() {
		// TODO Auto-generated method stub
		return this.tablero.getVidas();
	}

	public int getNivelActual() {
		// TODO Auto-generated method stub
		return this.tablero.getNivelActual();
	}

	public boolean estaPausado() {
		// TODO Auto-generated method stub
		return this.tablero.estaPausado();
	}

	public boolean estaNivelCompletado() {
		// TODO Auto-generated method stub
		return this.tablero.estaNivelCompletado();
	}

	public boolean estaEsperandoInicio() {
		// TODO Auto-generated method stub
		return this.tablero.estaEsperandoInicio();
	}

	public String[] getTop20Puntajes() {
		// TODO Auto-generated method stub
		return this.puntajeTablero.getTop20();
	}
}
