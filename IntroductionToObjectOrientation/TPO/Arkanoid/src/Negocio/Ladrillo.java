package Negocio;

public class Ladrillo 
{
	private RectColision areaCol;
	private Posicion2D pos;
	private int numeroFila;
	private boolean destruido;
	private boolean entregoPuntaje;
	
	
	public Ladrillo(int numeroFila, int x, int y, int anchoCol, int largoCol)
	{
		pos = new Posicion2D(x, y);
		areaCol = new RectColision(x, y, anchoCol, largoCol);
		this.numeroFila = numeroFila;
		this.destruido = false;
		this.entregoPuntaje = false;
	}
	
	public RectColision getAreaColision()
	{
		return this.areaCol;
	}
	
	public boolean estaDestruido()
	{
		return this.destruido;
	}
	
	public int calcularPuntaje()
	{
		if (entregoPuntaje)
			return 0;
		
		entregoPuntaje = true;
		
		if (numeroFila >= 0 && numeroFila < 5)
			return 40;
		
		if (numeroFila >= 5 && numeroFila < 10)
			return 30;
		
		if (numeroFila >= 10 && numeroFila < 15)
			return 20;
		
		if (numeroFila >= 15 && numeroFila < 20)
			return 10;
		
		return 50;
	}
	
	public boolean entregoPuntos() { return this.entregoPuntaje; }
	
	public void destruir() { this.destruido = true; }

	public ObjetoJuegoView getBloqueView() {
		Posicion2D topLeft = this.areaCol.superiorIzquierda();

		return new ObjetoJuegoView(topLeft.getX(), topLeft.getY(), this.areaCol.getAncho(), this.areaCol.getLargo());
	} 
}
