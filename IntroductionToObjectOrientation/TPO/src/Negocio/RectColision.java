package Negocio;

public class RectColision 
{
	private Posicion2D centro;
	
	private int ancho;
	private int largo;
	
	public RectColision()
	{
		this.ancho = this.largo = 0;
		centro = new Posicion2D();
	}
	
	public RectColision(int centroX, int centroY, int ancho, int largo)
	{
		this.ancho = ancho;
		this.largo = largo;
		
		centro = new Posicion2D(centroX, centroY);	
	}
	
	public Posicion2D getCentro() { return this.centro; }
	public int getAncho() { return this.ancho; }
	public int getLargo() { return this.largo; }
	public int bordeIzquierdo() { return centro.getX() - ancho/2; }
	public int bordeDerecho() { return centro.getX() + ancho/2; }
	public int bordeSuperior() { return centro.getY() - largo/2; };
	public int bordeInferior() { return centro.getY() + largo/2; };
	
	public Posicion2D superiorDerecha()
	{	
		return new Posicion2D(centro.getX() + ancho/2, centro.getY() - largo/2);
	}
	
	public Posicion2D superiorIzquierda()
	{	
		return new Posicion2D(centro.getX() - ancho/2, centro.getY() - largo/2);
	}
	
	public Posicion2D inferiorDerecha()
	{	
		return new Posicion2D(centro.getX() + ancho/2, centro.getY() + largo/2);
	}
	
	public Posicion2D inferiorIzquierda()
	{	
		return new Posicion2D(centro.getX() - ancho/2, centro.getY() + largo/2);
	}
	
	public boolean estaColisionandoCon(RectColision rect)
	{
		int bordeIzq = this.bordeIzquierdo();
		int bordeDer = this.bordeDerecho();
		int bordeSup = this.bordeSuperior();
		int bordeInf = this.bordeInferior();
		
		Posicion2D topLeft2 = rect.superiorIzquierda();
		Posicion2D topRight2 = rect.superiorDerecha();
		Posicion2D bottomLeft2 = rect.inferiorIzquierda();
		Posicion2D bottomRight2 = rect.inferiorDerecha();
		
		if ((topLeft2.getX() >= bordeIzq && topLeft2.getX() <= bordeDer) && 
			(topLeft2.getY() >= bordeSup && topLeft2.getY() <= bordeInf))
			return true;
		
		if ((topRight2.getX() >= bordeIzq && topRight2.getX() <= bordeDer) && 
			(topRight2.getY() >= bordeSup && topRight2.getY() <= bordeInf))
			return true;
		
		if ((bottomRight2.getX() >= bordeIzq && bottomRight2.getX() <= bordeDer) && 
			(bottomRight2.getY() >= bordeSup && bottomRight2.getY() <= bordeInf))
				return true;
		
		if ((bottomLeft2.getX() >= bordeIzq && bottomLeft2.getX() <= bordeDer) && 
			(bottomLeft2.getY() >= bordeSup && bottomLeft2.getY() <= bordeInf))
				return true;
		
		return false;
	}
}
