package Negocio;

public class ObjetoJuegoView 
{
	private int x;
	private int y;
	private int ancho;
	private int largo;
	
	public ObjetoJuegoView()
	{
		
	}
	
	public ObjetoJuegoView(int pX, int pY, int pAncho, int pLargo)
	{
		this.x = pX;
		this.y = pY;
		this.ancho = pAncho;
		this.largo = pLargo;
	}
	
	public int getX() { return this.x; };
	public int getY() { return this.y; };
	public int getAncho() { return this.ancho; };
	public int getLargo() { return this.largo; };
}
