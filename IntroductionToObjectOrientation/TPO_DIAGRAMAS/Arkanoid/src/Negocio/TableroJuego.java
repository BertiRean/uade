package Negocio;


import java.util.ArrayList;

public class TableroJuego 
{
	private enum EstadoJuego
	{
		NO_INICIADO,
		ESPERANDO_INICIO,
		EN_PROGRESO,
		PAUSADO,
		FIN_DE_JUEGO,
		NIVEL_COMPLETADO,
	}
	
	private static final int BLOQUE_ANCHO 	= 60;
	private static final int BLOQUE_ALTO 	= 20;
	
	private static final int BALL_WIDTH		= 20;
	private static final int BALL_HEIGHT	= 20;
	
	private static final int BARRA_ANCHO	= 120;
	private static final int BARRA_ALTO		= 20;
	
	private int anchoTablero;
	private int largoTablero;
	private int nivelActual;
	
	private EstadoJuego estadoJuego;
	
	private Bola bola;
	private Barra barra;
	private Jugador jugador;
	private Ladrillo[] bloques;
	
	
	public TableroJuego(int pAnchoTablero, int pLargoTablero)
	{
		this.estadoJuego = EstadoJuego.NO_INICIADO;
		this.anchoTablero = pAnchoTablero;
		this.largoTablero = pLargoTablero;
	}
	
	public boolean estaPausado()
	{
		return this.estadoJuego == EstadoJuego.PAUSADO;
	}
	
	public boolean estaIniciadaLaPartida()
	{
		return this.estadoJuego != EstadoJuego.NO_INICIADO;
	}
	
	public boolean estaNivelCompletado()
	{
		return this.estadoJuego == EstadoJuego.NIVEL_COMPLETADO;
	}
	
	public boolean esFinDeJuego()
	{
		return this.estadoJuego == EstadoJuego.FIN_DE_JUEGO;
	}
	
	public boolean estaNivelEnProgreso()
	{
		return this.estadoJuego == EstadoJuego.EN_PROGRESO;
	}
	
	public boolean estaEsperandoInicio()
	{
		return this.estadoJuego == EstadoJuego.ESPERANDO_INICIO;
	}
	
	public void setEstadoJuego(EstadoJuego estado)
	{
		this.estadoJuego = estado;
	}
	
	public void actualizar(long deltaT)
	{
		if (!this.estaNivelEnProgreso())
			return;
		
		if (this.bola.estaFueraDelTablero())
		{
			this.jugador.perderVida();
			
			if (this.jugador.getVidas() > 0)
			{
				this.inicializarBarra();
				this.inicializarBola();
				setEstadoJuego(EstadoJuego.ESPERANDO_INICIO);
			}
			else
			{
				setEstadoJuego(EstadoJuego.FIN_DE_JUEGO);
			}
			
			return;
		}
		
		for (int i = 0; i < bloques.length; i++)
		{
			Ladrillo bloque = bloques[i];
			
			if (bloque == null)
				continue;
			
			RectColision bloqueArea = bloque.getAreaColision();
			
			if (!bloque.estaDestruido() && bola.estaColisionandoCon(bloqueArea))
			{
				bola.rebotar(bloque);
				bloque.destruir();
			}
		}
		
		if (bola.estaColisionandoCon(barra.getAreaColision()))
			bola.rebotar(barra);
		
		bola.actualizarPosicion(deltaT);
		barra.actualizarPosicion(deltaT);
		
		this.comprobarPuntajesPorEntregar();
	}
	
	private void inicializarBola()
	{
		int ballX = (int) (anchoTablero * 0.45);
		int ballY = (int) (largoTablero * 0.8);
		
		bola = new Bola(ballX, ballY, BALL_WIDTH, BALL_HEIGHT);
		bola.setBordesTablero(anchoTablero, largoTablero);
	}
	
	private void inicializarBarra()
	{
		int barraX = (int) (anchoTablero * 0.45);
		int barraY = (int) (largoTablero * 0.83);
		
		barra = new Barra(barraX, barraY,  BARRA_ANCHO, BARRA_ALTO, anchoTablero);
	}
	
	private void inicializarBloques()
	{
		int bloquesCount = 25;
		bloques = new Ladrillo[bloquesCount];
		
		float xFactor = 0.125f;
		float yFactor = 0.05f;
		
		int bloqueX = (int) (anchoTablero * 0.25);
		int bloqueY = (int) (largoTablero * 0.15);
		
		for (int i = 0, j = 0; i < bloquesCount; i++, j++)
		{
			if (j > 4)
			{
				bloqueX  = (int)(anchoTablero * 0.25);
				bloqueY += (int)(largoTablero * yFactor);
				j = 0;
			}
			
			Ladrillo bloque = new Ladrillo(i, bloqueX, bloqueY, BLOQUE_ANCHO, BLOQUE_ALTO);
			bloques[i] = bloque;
			
			bloqueX += (int) (anchoTablero * xFactor);
		}
	}
	
	public void inicializarTablero()
	{	
		this.jugador = new Jugador();
		this.nivelActual = 1;
		
		this.inicializarBarra();
		this.inicializarBloques();
		this.inicializarBola();
		this.setEstadoJuego(EstadoJuego.ESPERANDO_INICIO);
	}

	public void iniciarPartida()
	{
		this.setEstadoJuego(EstadoJuego.EN_PROGRESO);
	}
	
	public void moverBarra(boolean moverDerecha)
	{
		if (moverDerecha)
			this.barra.moverDerecha();
		else
			this.barra.moverIzquierda();
	}
	
	public void pausar(boolean pausa)
	{
		if (this.estaPausado())
			this.setEstadoJuego(EstadoJuego.EN_PROGRESO);
		else
			this.setEstadoJuego(EstadoJuego.PAUSADO);
	}
	
	public void comprobarPuntajesPorEntregar()
	{
		int puntaje = 0;
		int destruidos = 0;
		
		for (Ladrillo itr : bloques)
		{
			if (itr == null)
				continue;
			
			if (itr.estaDestruido())
				destruidos += 1;
			
			if (itr.estaDestruido() && !itr.entregoPuntos())
				puntaje += itr.calcularPuntaje();
		}
		
		if (destruidos >= bloques.length)
			this.setEstadoJuego(EstadoJuego.NIVEL_COMPLETADO);
		
		jugador.sumarPuntos(puntaje);
	}

	public void avanzarDeNivel()
	{
		this.inicializarBola();
		this.inicializarBarra();
		this.inicializarBloques();
		this.bola.incrementarVelocidad();
		this.nivelActual += 1;
		this.setEstadoJuego(EstadoJuego.ESPERANDO_INICIO);
	}
	
	public void detenerBarra() {
		// TODO Auto-generated method stub
		this.barra.detenerBarra();
	}

	public ObjetoJuegoView getBolaView() {
		// TODO Auto-generated method stub
		return this.bola.getBolaView();
	}

	public ObjetoJuegoView getBarraView() {
		// TODO Auto-generated method stub
		return this.barra.getBarraView();
	}

	public ObjetoJuegoView[] getBloquesViews() {
		// TODO Auto-generated method stub
		ArrayList<ObjetoJuegoView> bloquesViews = new ArrayList<ObjetoJuegoView>();
		
		for (Ladrillo itr : this.bloques)
		{
			if (!itr.estaDestruido())
				bloquesViews.add(itr.getBloqueView());
		}
		
		ObjetoJuegoView[] views = new ObjetoJuegoView[1];
		return bloquesViews.toArray(views);
	}

	public int getPuntaje() {
		// TODO Auto-generated method stub
		return this.jugador.getPuntos();
	}

	public int getVidas() {
		// TODO Auto-generated method stub
		return this.jugador.getVidas();
	}

	public int getNivelActual() {
		// TODO Auto-generated method stub
		return this.nivelActual;
	}
}
