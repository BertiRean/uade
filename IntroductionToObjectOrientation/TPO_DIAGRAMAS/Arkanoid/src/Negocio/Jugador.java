package Negocio;

public class Jugador 
{
	private static final int VIDAS_INICIALES = 3;
	private int vidas;
	private int puntos;
	private int puntosParaSgtVida;
	
	public Jugador()
	{
		vidas = VIDAS_INICIALES;
		puntos = 0;
		puntosParaSgtVida = 1000;
	}
	
	public void sumarPuntos(int cantidad)
	{
		this.puntos += cantidad;
		
		if (puntos >= puntosParaSgtVida)
		{
			puntosParaSgtVida += 1000;
			this.agregarVidas(1);
		}
	}
	
	public void agregarVidas(int cantidad)
	{
		this.vidas += cantidad;
	}
	
	public Boolean estaMuerto() 
	{
		return this.vidas < 0;
	}
	
	public void perderVida()
	{
		this.vidas -= 1;
	}

	public int getVidas() {
		// TODO Auto-generated method stub
		return this.vidas;
	}

	public int getPuntos() {
		// TODO Auto-generated method stub
		return this.puntos;
	}
}
