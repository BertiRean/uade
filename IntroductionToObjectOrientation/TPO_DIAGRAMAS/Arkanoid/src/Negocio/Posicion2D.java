package Negocio;

public class Posicion2D 
{
	private int x;
	private int y;
	
	public Posicion2D()
	{
		x = y = 0;
	}
	
	public Posicion2D(int p_X, int p_Y)
	{
		this.x = p_X;
		this.y = p_Y;
	}
	
	public int getX() { return this.x; }
	public int getY() { return this.y; }
	
	public void setX(int nuevoX)
	{
		x = nuevoX;
	}
	
	public void setY(int nuevoY)
	{
		y = nuevoY;
	}
}
