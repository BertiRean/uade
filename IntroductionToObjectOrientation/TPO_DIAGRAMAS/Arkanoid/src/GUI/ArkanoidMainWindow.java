package GUI;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferStrategy;
import java.time.Duration;
import java.time.Instant;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import Negocio.Controlador;
import Negocio.ObjetoJuegoView;

public class ArkanoidMainWindow extends JFrame implements KeyListener
{
	public static final int SCREEN_WIDTH 	= 1024;
	public static final int SCREEN_HEIGHT	= 768;
	
	Controlador controlador;
	
	public ArkanoidMainWindow()
	{
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setUndecorated(false);
		this.setResizable(false);
		this.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
		this.setVisible(true);
		this.addKeyListener(this);
		this.setLocationRelativeTo(null);

		this.createBufferStrategy(2);
		
		controlador = new Controlador();
	}
	
	private void dibujarBola(Graphics g)
	{
		ObjetoJuegoView bola = controlador.getBolaView();
		
		g.setColor(Color.red);
		g.fillRect(bola.getX(), bola.getY(), bola.getAncho(), bola.getLargo());
	}
	
	private void dibujarBarra(Graphics g)
	{
		ObjetoJuegoView barra = controlador.getBarraView();
		
		g.setColor(Color.yellow);
		g.fillRect(barra.getX(), barra.getY(), barra.getAncho(), barra.getLargo());
	}
	
	private void dibujarBloques(Graphics g)
	{
		ObjetoJuegoView[] bloques = controlador.getBloquesViews();
		
		g.setColor(Color.green);
		
		for (ObjetoJuegoView bloque : bloques)
		{
			if (bloque != null)
				g.fillRect(bloque.getX(), bloque.getY(), bloque.getAncho(), bloque.getLargo());
		}	
	}
	
	private void dibujarTextosTableroJuego(Graphics g)
	{
		// Dibujamos el puntaje y la vida
		Font font = new Font("Courier New", Font.PLAIN, 18);
		font.deriveFont(34f);
		g.setColor(Color.pink);
		g.setFont(font);
		
		int anchoTablero = SCREEN_WIDTH;
		int largoTablero = SCREEN_HEIGHT;
		
		String puntosStr = "Puntaje: " + String.valueOf(this.controlador.getPuntaje());
		String vidasStr = "Vidas: " + String.valueOf(this.controlador.getVidas());
		String nivelStr = "Nivel: " + String.valueOf(this.controlador.getNivelActual());
		
		int scoreX = (int) (anchoTablero * 0.01);
		int scoreY = (int) (largoTablero * 0.1);
		
		int vidaX = (int) (anchoTablero * 0.01);
		int vidaY = (int) (largoTablero * 0.125);
		
		int nivelX = (int) (anchoTablero * 0.01);
		int nivelY = (int) (largoTablero * 0.15);
		
		g.drawString(puntosStr, scoreX, scoreY);
		g.drawString(vidasStr, vidaX, vidaY);
		g.drawString(nivelStr, nivelX, nivelY);
		
		String msj = "";
		int msjX = 0;
		int msjY = 0;
		
		if (this.controlador.estaPausado())
		{
			msj = "P A U S E";
			msjX = (int) (anchoTablero * 0.4);
			msjY = (int) (largoTablero * 0.5);
			
			font = new Font("Courier New", Font.PLAIN, 32);
			g.setFont(font);
			
		}
		else if (controlador.estaNivelCompletado())
		{
			msj = "NIVEL COMPLETADO PRESIONE ESPACIO PARA CONTINUAR";
			msjX = (int) (anchoTablero * 0.1);
			msjY = (int) (largoTablero * 0.5);
			
			font = new Font("Courier New", Font.PLAIN, 28);
			g.setFont(font);
			g.setColor(Color.green);
		}
		else if (controlador.esFinDeJuego())
		{
			msj = "FIN DE JUEGO";
			msjX = (int) (anchoTablero * 0.3);
			msjY = (int) (largoTablero * 0.5);
			font = new Font("Courier New", Font.PLAIN, 32);
			g.setFont(font);
			g.setColor(Color.red);
		}
		else if (controlador.estaEsperandoInicio())
		{
			msj = "PRESIONE ESPACIO PARA LANZAR LA BOLA";
			msjX = (int) (anchoTablero * 0.1);
			msjY = (int) (largoTablero * 0.5);
			font = new Font("Courier New", Font.PLAIN, 32);
			g.setFont(font);
			g.setColor(Color.green);
		}
		
		
		if (msj != "")
			g.drawString(msj, msjX, msjY);
	}
	
	private void dibujarTextosTableroPuntaje(Graphics g)
	{
		// TODO Auto-generated method stub
		int anchoPantalla = SCREEN_WIDTH;
		int largoPantalla = SCREEN_HEIGHT;
		
		int puntoInfoX = (int) (anchoPantalla * 0.3f);
		int puntoInfoY = (int) (largoPantalla * 0.1f);
		String msj = "";
		
		Font font = new Font("Courier New", Font.PLAIN, 25);
		g.setFont(font);
		g.setColor(Color.green);
		
		msj = "Tablero de Puntajes: ";
		g.drawString(msj, puntoInfoX, puntoInfoY);
		
		puntoInfoY += (int) (largoPantalla * 0.075f);
		
		font = new Font("Courier New", Font.PLAIN, 15);
		g.setFont(font);
		g.setColor(Color.red);
		
		for (String puntoInfo : this.controlador.getTop20Puntajes())
		{
			if (puntoInfo != null)
				g.drawString(puntoInfo, puntoInfoX, puntoInfoY);
			
			puntoInfoY += (int) (largoPantalla * 0.025f);
		}
		
		font = new Font("Courier New", Font.PLAIN, 25);
		g.setFont(font);
		g.setColor(Color.green);
		
		puntoInfoY = (int) (largoPantalla * 0.95f);
		msj = "PRESIONE ESPACIO PARA SALIR";
		g.drawString(msj, puntoInfoX, puntoInfoY);
	}
	
	private void dibujarEscena()
	{
		// Code for the drawing goes here.
		BufferStrategy bf = this.getBufferStrategy();
		Graphics g = null;

		try {

			g = bf.getDrawGraphics();

			g.setColor(Color.black);
			g.fillRect(0, 0, getWidth(), getHeight());
			
			
			if (controlador.estaEnTableroPuntaje())
			{
				dibujarTextosTableroPuntaje(g);	
			}
			else
			{
				dibujarBola(g);
				dibujarBloques(g);
				dibujarBarra(g);
				dibujarTextosTableroJuego(g);
			}

		} finally {
			g.dispose();
		}

		bf.show();

		Toolkit.getDefaultToolkit().sync();
	}
	
	public void pedirNombreJugador()
	{	
		String nombreJugador = JOptionPane.showInputDialog(this, "Has superado el puntaje record registrado, por favor escribe tu nombre para registrarte en el ranking", "Puntaje Record Superado", JOptionPane.INFORMATION_MESSAGE);
		
		if (nombreJugador != null)
			this.controlador.agregarPuntaje(nombreJugador);
	}
	
	public void run()
	{
		BufferStrategy bf = this.getBufferStrategy();
		Graphics g = bf.getDrawGraphics();
		
		g.setColor(Color.black);
		g.fillRect(0, 0, getWidth(), getHeight());
		
		Duration deltaTime = Duration.ZERO;
		Instant beginTime = Instant.now();
		
		controlador.inicializarTablero(SCREEN_WIDTH, SCREEN_HEIGHT);
		
		
		while (true)
		{
			if (controlador.esFinDeJuego())
			{
				if (controlador.entraEnRanking() && !this.controlador.estaEnTableroPuntaje())
					this.pedirNombreJugador();
				
				this.controlador.setEstaTableroPuntaje(true);
			}

			controlador.actualizarObjetos(deltaTime.toMillis());
			dibujarEscena();
			
			var oldDelta = Instant.now();
			deltaTime = Duration.between(beginTime, oldDelta);
			beginTime = oldDelta;
		
			
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}	
	}
	
	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
		if (arg0.getKeyCode() == KeyEvent.VK_SPACE)
			controlador.teclaEspacioPresionada();
		else if (arg0.getKeyCode() == KeyEvent.VK_RIGHT)
			controlador.flechaDerechaPresionada();
		else if (arg0.getKeyCode() == KeyEvent.VK_LEFT)
			controlador.flechaIzquierdaPresionada();
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		if (arg0.getKeyCode() == KeyEvent.VK_RIGHT || 
				arg0.getKeyCode() == KeyEvent.VK_LEFT)
			controlador.detenerBarra();
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
