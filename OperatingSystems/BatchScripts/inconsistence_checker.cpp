#include <iostream>
#include <vector>

vector<int> has_inconsistences(string const & free_blocks, string const & used_blocks, string const & bad_blocks)
{
	vector<int> inconsistence_blocks;

	assert(free_blocks.size() != used_blocks.size())

	for (size_t i = 0; i < free_blocks.size(); ++i)
	{
		if (free_blocks[i] != used_blocks[i])
			continue;
		else if (bad_blocks[i] != '1')
			inconsistence_blocks.emplace_back(i);
	}

	return inconsistence_blocks;
}

// convert Hexadecimal to Binary Number
string hex_to_bin(string const & hexa)
{

	string bin_str;

   long int i = 0;

   while (hexa[i])
   {
      switch (hexa[i])
      {
	      case '0':
	         bin_str.append("0000");
	         break;
	      case '1':
	         bin_str.append("0001");
	         break;
	      case '2':
	         bin_str.append("0010");
	         break;
	      case '3':
	         bin_str.append("0011");
	         break;
	      case '4':
	         bin_str.append("0100");
	         break;
	      case '5':
	         bin_str.append("0101");
	         break;
	      case '6':
	         bin_str.append("0110");
	         break;
	      case '7':
	         bin_str.append("0111");
	         break;
	      case '8':
	         bin_str.append("1000");
	         break;
	      case '9':
	         bin_str.append("1001");
	         break;
	      case 'A':
	      case 'a':
	         bin_str.append("1010");
	         break;
	      case 'B':
	      case 'b':
	         bin_str.append("1011");
	         break;
	      case 'C':
	      case 'c':
	         bin_str.append("1100");
	         break;
	      case 'D':
	      case 'd':
	         bin_str.append("1101");
	         break;
	      case 'E':
	      case 'e':
	         bin_str.append("1110");
	         break;
	      case 'F':
	      case 'f':
	         bin_str.append("1111");
	         break;
	      default:
         	cout << "\nPlease enter valid hexadecimal digit "<< hexa[i];
      }

      i++;
   }


   return bin_str;
}

int main()
{

	string free_blocks = hex_to_bin("09E5C8D5C");
	string used_blocks = hex_to_bin("064A372A3");
	string bad_blocks	 = hex_to_bin("040008000");

	vector<int> inconsistence_blocks = has_inconsistences(free_blocks, used_blocks, bad_blocks);

	if (inconsistence_blocks.empty())
		cout << "Free of Inconsistence" << '\n';
	else
	{
		cout << "Inconsistence Indexes" << '\n';
		
		for (auto const & it : inconsistence_blocks)
			cout << it << ',';
	}

	return 0;
}