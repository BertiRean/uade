#include <iostream>
#include <string>
#include <vector>
#include <functional>
#include <list>
#include <cassert>

using namespace std;


size_t get_hash(size_t val)
{
    hash<size_t> h;
    return h(val);
}

size_t get_hash(string const & val)
{   
    hash<string> h;
    return h(val);
}

struct MerkleNode
{
    string str;
    size_t nodeHash;
    MerkleNode* parent;
    MerkleNode* left;
    MerkleNode* right;

    MerkleNode()
    {
        nodeHash = 0;
        left = nullptr;
        right = nullptr;
        parent = nullptr;
    }
    
    bool isLeaf() const { return left == nullptr && right == nullptr; }

    MerkleNode* getBrotherOf(MerkleNode* child)
    {
        if (child == left)
            return right;
        else if (child == right)
            return left;

        return nullptr;
    }

    MerkleNode(char value) : parent(nullptr), left(nullptr), right(nullptr)
    {
        str.push_back(value);
        nodeHash = get_hash(value);
    }

    MerkleNode(string const & value) : parent(nullptr), left(nullptr), right(nullptr)
    {
        str = value;
        nodeHash = get_hash(value);
    }

    string toString()
    {
        string out;

        out += "HashNode: " + std::to_string(nodeHash) + " --- H" + str;

        return out;
    }
};

using NodeFull = std::pair<MerkleNode*, vector<MerkleNode*>>; 

MerkleNode* make_merkle_node(char left_key, char right_key)
{
    MerkleNode* left_tree = new MerkleNode(left_key);
    MerkleNode* right_tree = new MerkleNode(right_key);

    MerkleNode* parent = new MerkleNode();
    parent->nodeHash = get_hash(left_tree->nodeHash | right_tree->nodeHash);
    parent->left = left_tree;
    parent->right = right_tree;
    parent->str = left_tree->str + right_tree->str;
    return parent;
}

void foreach_tree(MerkleNode* root)
{
    if (root == nullptr)
        return;

    foreach_tree(root->left);
    cout << root->toString() << endl;
    foreach_tree(root->right);
}

void destroy_tree(MerkleNode* root)
{
    if (root == nullptr)
        return;

    if (root->isLeaf())
        delete root;
    
    destroy_tree(root->left);
    root->left = nullptr;
    destroy_tree(root->right);
    root->right = nullptr;

    return;
}

int binary_search(MerkleNode*& key, vector<MerkleNode*> const & a, size_t l, size_t r)
{
    if (l - r <= 1)
        return - 1;

    if (a[r] == key)
        return r;

    if (a[l] == key)
        return l;

    size_t m = (l + r) / 2;

    if (a[m] == key)
        return m;

    if (key < a[m])
        return binary_search(key, a, l, m);
    else if(key > a[m])
        return binary_search(key, a, m + 1, r);

    return -1;
}

void get_path_to_transaction(NodeFull const & nodeFull, string const & transaction, vector<MerkleNode*> & path)
{
    MerkleNode* nodePath = nullptr;

    for (auto const & node : nodeFull.second)
    {
        if (node->nodeHash == get_hash(transaction))
        {
            nodePath = node;
            break;
        }
    }

    if (nodePath == nullptr)
        return;

    MerkleNode* brother = nullptr;
    while (nodePath->parent != nullptr)
    {
        brother  = nodePath->parent->getBrotherOf(nodePath);
        nodePath = nodePath->parent;
        path.push_back(brother);
    }
}

MerkleNode* build_full_tree(list<MerkleNode*>& nodes)
{
    if (nodes.size() < 2)
        return nodes.front();

    cout << "Iteration:" << endl;
    for (auto it : nodes)
    {
        cout << it->toString() << endl;
    }
    cout << "End Iteration" << endl;

    list<MerkleNode*> parents;

    while (!nodes.empty())
    {
        MerkleNode* left_tree = nodes.front();
        nodes.pop_front();

        if (nodes.empty())
        {
            parents.push_back(left_tree);
            continue;
        }

        MerkleNode* right_tree = nodes.front();
        nodes.pop_front();

        MerkleNode* parent = new MerkleNode();
        parent->nodeHash = get_hash(left_tree->nodeHash + right_tree->nodeHash);

        parent->left = left_tree;
        parent->right = right_tree;
        parent->str = left_tree->str + right_tree->str;

        left_tree->parent = parent;
        right_tree->parent = parent;

        parents.push_back(parent);

    }

    return build_full_tree(parents);
}

NodeFull hash_blocks(vector<char>& blocks)
{
    list<MerkleNode*> nodes;
    vector<MerkleNode*> transactions;

    for (char & it : blocks)
    {
        MerkleNode* node = new MerkleNode(it);
        nodes.push_back(node);
        transactions.push_back(node);
    }

    cout << "After Hash Blocks" << endl;
    for (auto it : nodes)
    {
        cout << it->toString() << endl;
    }
    cout << "End For After Hash Blocks\n\n" << endl;

    MerkleNode* hashRoot = build_full_tree(nodes);
    return NodeFull(hashRoot, transactions);
}

NodeFull hash_blocks(vector<string>& blocks)
{
    list<MerkleNode*> nodes;
    vector<MerkleNode*> transactions;

    for (string & it : blocks)
    {
        MerkleNode* node = new MerkleNode(it);
        nodes.push_back(node);
        transactions.push_back(node);
    }

    cout << "After Hash Blocks" << endl;
    for (auto it : nodes)
    {
        cout << it->toString() << endl;
    }
    cout << "End For After Hash Blocks\n\n" << endl;

    MerkleNode* hashRoot = build_full_tree(nodes);
    return NodeFull(hashRoot, transactions);
}

int main()
{
    vector<string> v = {"ab", "cd", "ef", "gh", "ij", "kl"};

    NodeFull full_node = hash_blocks(v);

    //foreach_tree(hash_root);

    vector<MerkleNode*> path;
    get_path_to_transaction(full_node, "gh", path);

    size_t hash = get_hash("gh");

    for (auto & it : path)
    {
        cout << it->toString() << endl;
        hash = get_hash(hash + it->nodeHash);
    }

    assert(hash == full_node.first->nodeHash);

    cout << "Hash : " << hash << endl;
    cout << "Root Hash: " << full_node.first->nodeHash;

    if (not path.empty() and full_node.first->nodeHash == path.back()->nodeHash)
        cout << "Transaction Founded";

    cout << "Size: " << path.size() << endl;

    destroy_tree(full_node.first);

    return 0;
}
