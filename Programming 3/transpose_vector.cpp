#include <iostream>
#include <vector>

using namespace std;

void transpose_k_elements(vector<int> & array, int k)
{
	size_t n = array.size();

	for (size_t i = 0; i < n; ++i)
	{
		int temp = array[i];
		array[i] = array[k];
		array[k] = temp;

		if (k + 1 < n)
			k += 1;
	}

	print_vector(array);
}

vector<int> slow_transpose(vector<int> array, int k)
{
	vector<int> copy_array;
	copy_array.reserve(k);

	for (size_t i = 0; i < k; ++i)
		copy_array[i] = array[i];

	vector<int> prev_vector;

	for (size_t i = k; i < array.size() - k; i++)
		prev_vector.push_back(array[i]);

	for (auto it : copy_array)
		prev_vector.push_back(it);

	print_vector(prev_vector);
	return prev_vector;
}

void print_vector(vector<int> const & v)
{
	for (auto it : v)
		cout << it << ',';

	cout << '\n';
}

void tests()
{
	vector<int> test = { 3, 5, 12, 8, 9, 12, 4, 7, 13, 21};

	assert(slow_transpose(test, 3) == transpose_k_elements(test, 3));
}

int main()
{
	tests();
	return 0;
}

