#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

const string upper_caps = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const string lower_caps = "abcdefghijklmnopqrstuvwxyz";
const string digits = "0123456789";
const string special = "+-*/%&";
const string total_set = upper_caps + lower_caps + digits;

vector<string> generate_passwords()
{
	size_t n = 155912640;

	string pass = "xxxxxx";
	vector<string> keys;

	for (size_t i = 0; i < n; ++i)
	{
		for (auto upper : upper_caps)
		{
			pass[0] = upper;

			for (auto lower : lower_caps)
			{
				pass[1] = lower;
				for (auto digit : digits)
				{
					pass[2] = digit;
					for (auto spec : special)
					{
						pass[3] = digit;

						for (auto rand_x : total_set)
						{
							pass[4] = rand_x;
							for (auto rand_y : total_set)
							{
								pass[5] = rand_y;
								cout << pass << endl;
							}
						}
					}
				}
			}
		}
	}
}

void generate_permutations()
{
	string password = "ulds?$";

	sort(password.begin(), password.end());

  	size_t counter = 0;

	do
	{
		cout << password << endl;
    counter++;
	}
	while(next_permutation(password.begin(), password.end()));

  cout << "Total Permutations: " << counter << endl;
}

void test()
{
	vector<string> keys = generate_passwords();

	for (auto pass : keys)
		cout << pass << endl;
}

int main()
{
	test();
	return 0;
}