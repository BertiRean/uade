#include <iostream>
#include <vector>
#include <random>
#include <algorithm>
#include <cassert>

using namespace std;

void print_vector(vector<int>& a)
{
  for (auto it : a)
    cout << it << ',';

  cout << endl;
}

size_t get_random_number(size_t min, size_t max)
{
  return rand() % max + min;
}

void print_partition(vector<int>& a, size_t l, size_t r)
{
  for (size_t i = l; i < r; ++i)
    cout << a[i] << ',';
  cout << endl;
}

size_t partition(vector<int>& A, size_t l, size_t r)
{
  auto & pivot = A[l];
  size_t j = l;

  for (size_t i = l + 1; i < r; ++i)
  {
    if (A[i] <= pivot)
    {
      j++;
      swap(A[i], A[j]);
    }
  }

  swap(A[l], A[j]);
  return j;
}

void random_quicksort(vector<int>& A, size_t l, size_t r)
{
	if (l >= r)
    return;
  
  auto k = get_random_number(l, r);
  swap(A[l], A[k]);

  auto m = partition(A, l, r);
  random_quicksort(A, l, m);
  random_quicksort(A, m + 1, r);
}

void quicksort(vector<int>& A, size_t l, size_t r)
{
  if (l >= r)
    return;
  
  auto pivot_index = partition(A, l, r);
  quicksort(A, l, pivot_index);
  quicksort(A, pivot_index + 1, r);
}

void tests()
{
  vector<int> t = { 7, 5, 2, 2, 6 };
  random_quicksort(t, 0, t.size());
  print_vector(t);
  return;
  for (size_t i = 0; i < 20; ++i)
  {
    vector<int> a;
    size_t n = get_random_number(5, 5);

    for (size_t j = 0; j < n; ++j)
      a.emplace_back(get_random_number(1, 10));

    cout << "Input: " << endl;
    print_vector(a);
    cout << endl;
    random_quicksort(a, 0, a.size());
    cout << "After Sort" << endl;
    print_vector(a);
    assert(is_sorted(a.begin(), a.end()));
  }
}

int main() 
{
  tests();
  return 0;
}