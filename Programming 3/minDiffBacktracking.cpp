#include <iostream>
#include <vector>
#include <utility>
#include <climits>

using namespace std;

using MinPair = std::pair<int, vector<int>>;

struct OutInfo
{
	OutInfo() : diff(0)
	{

	}

	vector<int> s1;
	vector<int> s2;

	int diff;
};

void min_diff_rec(vector<int> & S, int level, MinPair & S1, MinPair & S2, int m, OutInfo & out);


void print_set(vector<int>& s);

void min_diff(vector<int> S)
{
	int level = 0;

	MinPair S1, S2;

	S1.first = 0;
	S2.first = 0;

	OutInfo out;

	out.diff = INT_MAX;

	int m = 0;
	m = S.size() / 2;


	min_diff_rec(S, level, S1, S2, m, out);

	cout << "Difference: " << out.diff << endl;
	cout << "Set 1" << '\n';
	print_set(out.s1);
	cout << "Set 2: " << '\n';
	print_set(out.s2);
}

void print_set(vector<int> & s)
{
	for (auto i : s)
		cout << i << ',';

	cout << '\n';
}

void min_diff_rec(vector<int> & S, int level, vector<vector<int>> & subsets, vector<int>& path)
{
	if (level >= S.size())
		return;

	for (size_t i = 0; i < 2; ++i)
	{
		if (level > m)
		{
			subsets.push_back(path);
			path.pop_back();
		}
		else
		{
			if (i == 0)
			{
				path.push_back(S[level]);
			}

			min_diff_rec(S, level + 1, S1, S2, m, out);
		}
	}


	if (S1.first > 0)
		S1.first -= S[level];

	if (S2.first > 0)
		S2.first -= S[level];

  if (!S1.second.empty())
    S1.second.pop_back();

  if (!S2.second.empty())
  	S2.second.pop_back();
}



int main()
{
	std::vector<int> v = {4, 100, 1, 23, 20};

  min_diff(v);

  return 0;

}