#include <iostream>
#include <string>

using namespace std;

size_t lcs(string  a, string  b)
{
  size_t freq_a[256] = {0};
  size_t freq_b[256] = {0};

  for (char it : a)
    freq_a[it]++;

  for (char it : b)
    freq_b[it]++;

  size_t count = 0;

  for (size_t i = 0; i < 256; ++i)
    count += min(freq_a[i], freq_b[i]);

  return count;
}

int main() 
{
  cout << "LCS: " << lcs("abcbda", "abacadac") << endl;
  cout << "LCS: " << lcs("aggtab", "gxtxayb") << endl;
  cout << "LCS: " << lcs("antonio", "candela") << endl;
  cout << "LCS: " << lcs("jose", "pedro") << endl; 
  cout << "LCS: " << lcs("caballo", "comadreja") << endl;
}