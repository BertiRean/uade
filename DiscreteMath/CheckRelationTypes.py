def is_transitive(p_R):
  l_IsTransitive = True

  for p in p_R:
    for s in p_R:
      if p[1] == s[0]:
        l_IsTransitive = (p[0], s[1]) in p_R

        if not l_IsTransitive:
          return False

  return True

def is_reflexive(p_R):

  l_IsReflexive = True

  for p in p_R:
    
    l_IsReflexive = (p[0], p[0]) in p_R

    if not l_IsReflexive:
      return False      
  
  return True


def is_symetric(p_R):

  l_IsSymetric = True

  for p in p_R:
    l_IsSymetric = (p[1], p[0]) in p_R

    if not l_IsSymetric:
      return False

  return True

def is_anti_symetric(p_R):

  l_IsAntiSymetric = True

  for p in p_R:
    if p[0] == p[1]:
      continue
      
    l_IsAntiSymetric = (p[1], p[0]) not in p_R

    if not l_IsAntiSymetric:
      return False

  return True


def is_equivalence(p_R):
  return is_reflexive(p_R) and is_symetric(p_R) and is_transitive(p_R)

def is_order(p_R):
  return is_reflexive(p_R) and is_anti_symetric(p_R) and is_transitive(p_R)

def get_equivalence_class(p_Key, p_Relation):

  l_Ret = []

  if not is_equivalence(p_Relation):
    print("The Relation Set isn't a equivalence relation")
    return []

  for l_P in p_Relation:
    if l_P[0] == p_Key:
      l_Ret.append(l_P[1])

  print("Equivalence Class of ", p_Key, " -> ", l_Ret)
  return l_Ret


def get_quotient_set(p_Relation):

  l_Quotient = []
  l_Set = set()

  for l_Pair in p_Relation:
    if l_Pair[0] not in l_Set:
      l_Set.add(l_Pair[0])

  for l_SetElement in l_Set:
    l_EquiClass = get_equivalence_class(l_SetElement, p_Relation)

    if l_EquiClass and l_EquiClass not in l_Quotient:
      l_Quotient.append(l_EquiClass)


  print("Quotient Set is ", l_Quotient)


R = []

for x in range(200):
	
