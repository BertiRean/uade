Ventas
	1. Los vendedores concurren al domicilio de sus cliente e intentan venderle al cliente algun producto.
		1.1 Si el cliente concreta la venta, el vendedor emite  notas de pedido por cuadruplicado, solicitandole al cliente su firma en el original, y, entregando tambien el triplicado al cliente. El vendedor se queda con las copias de las notas de pedido restantes.
		1.2 Si no se concreta la venta -> PROCESO NO RELEVADO.
	2. Al terminar el recorrido de ventas, el vendedor controla las notas de pedido que tiene.
		2.1 Si tiene notas de pedido por ventas, remite a Ventas el original y el duplicado de las Notas de pedidos y archiva numéricamente la última copia.
		2.2 Si no tiene notas de pedido --> PROCESO NO RELAVADO

	3. Ventas (NO HAY ACTOR O PERSONA ENCARGADA) recibe las notas de pedido y controla los precios de estas, analizando tambien las bonificaciones que cada vendedor ha otorgado a criterio personal. (NORMA DE CONTROL FALLIDA -> Otorgamiento de Bonificaciones).
		3.1 Si las notas de pedido realizadas estan bien hechas, se envia el original de cada nota de pedido a Créditos y el duplicado de la nota es archivado.
		3.2 Si no estan bien hechas --> PROCESO NO RELEVADO
Créditos
	4. Luego de haber recibido las notas de pedidos por parte de Ventas, controla la situación crediticia de cada cliente con el Sistema de Cuentas Corrientes de Clientes.
		4.1 Si su situación crediticia esta bien, autoriza la operación y envia el original de la nota de pedido a Almacén.
		4.2 Si su situación crediticia no esta bien, entonces devuelve la nota de pedido rechazada a Ventas.
Almacen
	5. Con la nota de pedido recibida por parte de Créditos, se controla el stock de productos requeridos.
		5.1 Si hay stock devuelve a Ventas el original de la nota pedido conformada.
		5.2 Si no hay stock devuelve a ventas el original de la nota de pedido observada.
Ventas
	6. Recibe la nota de pedido de Almacen y controla su estado.
		6.1 Si fue observada, se contacta via telefono al cliente y se le comunica las observaciones en lo solicitado. Dependiendo de la opción del cliente
		se hace:
			6.1.1 Si el cliente da conformidad, se confecciona el remito por quintuplicado, distibuyendolo de la siguiente manera
				- Original y 3 copias a Almacen.
				- La ultima copia la envia a Facturación junto con el original de la nota de pedido.
			6.1.2 Si no da conformidad el cliente --> PROCESO NO RELEVADO
		6.2 Si no fue observada -> PROCESO NO RELEVADO

Almacen
	7. Se prepara el pedido con el remito recibido, se registra la salida de productos en sus archivos y se envia a Reparto la mercadería junto a el original del remito y sus 3 copias.

Reparto
	8. Se recibe los pedidos con la mercadería y sus remitos e inicia la distribucion de estos, entregando cada pedido en el domicilio del cliente. A este
	ultimo se le solicita que firme el remito al momento de la entrega del pedido.
	9. Al final de las entregas, se envia a Facturación los remitos conformados (Firmados) por los clientes.

Facturación.
	10. Se toma del archivo la nota de pedido original y se verifica.
		10.1 Si esta todo bien, se prepara una factura por cuadruplicado, archivando la ultima copia y enviando a Contaduría el resto de la documentación
		(original de la nota de pedido, duplicado del remito y los tres ejemplares de la factura).
		10.2 Si no entonces --> PROCESO NO RELEVADO

Contaduría
	11. Con la documentación recibida de Facturación, registra contablemente la operacion y remite las facturas recibidas a Cobranzas.

Cobranzas
	12. Con la facturas recibidas, se envia por correo, el original de la factura al cliente y los duplicados a los vendedores.

Ventas
	13. Con los duplicados de la facturas recibidos, el vendedor se presenta en el domicilio de cada cliente y cobra la factura. (NO HAY CONTROL 
	POR PARTE DEL VENDEDOR O CLIENTE ENTRE LA FACTURA ORIGINAL DEL CLIENTE Y EL DUPLICADO DEL VENDEDOR).
	14. Los vendedores segun se organizen, rinden lo cobrado los quince dias posteriores a la cobranza en las oficinas de la empresas. (NO HAY
	ACLARACIÓN DE QUE AREA ES DONDE RINDEN LO COBRADO)

