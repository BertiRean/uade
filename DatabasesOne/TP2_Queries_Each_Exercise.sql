use ZRESTAURANTE;

-- Ejercicio 1
-- select COUNT(*) AS Cantidad from Facturas where horaApertuta >= '21:00:00' and horaApertuta <= '22:00:00';

-- Ejercicio 2
-- select IdMozo, SUM(total) as TotalXMozo from Facturas GROUP BY IdMozo;

-- Ejercicio 3
--SELECT IdMozo, Sum(total) as VentaTotal from Facturas Group by IdMozo having Sum(total) > 200;

-- Ejercicio 4
--select IdIngrediente, descripcion, stock from Ingredientes where IdIngrediente in (116, 300, 301);

-- Ejercicio 5
-- select sum(stock) as StockHelado from Ingredientes where descripcion LIKE '%Helado%';

-- Ejercicio 6
-- select descripcion from Ingredientes where stock > 10000 and precComp < 50 and puntoRep > 10;

-- Ejercicio 7
-- select NroFactura, sum(cantida) from ItemsFactura group by NroFactura;

-- Ejercicio 8
-- select NroFactura, total as ImporteTotal from Facturas where total > 100;

-- Ejercicio 9
-- select IdUnidad, descripcion from Unidades where descripcion like '%Bolsa%'

-- Ejercicio 10
-- select * from TelefonoMozo where celular != 0

-- Ejercicio 11
-- select * from TelefonoProveedor where codArea = 64;

-- Ejercicio 12
select CONCAT(codArea, '-', central, '-', numero) AS Telefono from TelefonoProveedor where IdProveedor in (50, 60)