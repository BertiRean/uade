/* El SQL no es case sensitive ,puedo poner CREATE TABLE y lo toma igual

creacion de objetos:
create tipo_de_objeto identificador ( descripcion )

las tablas no pueden tener menos de un atributo por lo tanto necesitan descripcion
varchar (es una cadena de caracteres)
decimal (digitos,cantidad de enteros)
*/
create table ejemplo1( 
	nombre_campo_01 int,
	nombre_campo_02 float,
	nombre_campo_03 varchar(150),
	nombre_campo_04 decimal(18,2)
	constraint pk_ejemplo1 primary key(nombre_campo_03,nombre_campo_01) /*Genero una clave primaria*/
	/* el constraint me permite agregar un nombre a la clave. PK= primary key */
)

create table ejemplo2( /* la clave foranea me vincula dos tablas ,en este caso ejemplo2 con ejemplo 1*/
	nombre_campo_01 int,
	nombre_campo_03 varchar(150),
	nombre_campo_10 int,
	nombre_campo_11 int,
	nombre_campo_12 varchar(10),
	nombre_campo_13 decimal(18,2)
	constraint pk_ejemplo2 primary key (nombre_campo_10),
	constraint fk_ejemplo2_ejemplo1 foreign key (nombre_campo_03,nombre_campo_01) references ejemplo1 /* se colocan los nombres de los campos que coinciden en ambas tablas
	la clave tiene que estar en el mismo orden que la clave pirmaria del ejemplo1 : 'nombre_campo_03,nombre_campo_01' si lo pongo asi: 
	constraint fk_ejemplo2_ejemplo1 foreign key (nombre_campo_01,nombre_campo_03) references ejemplo1
	tiraria error */
	)

	