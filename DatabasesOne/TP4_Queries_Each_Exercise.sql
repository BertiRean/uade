CREATE FUNCTION MOSTRAR_PROPINAS()
RETURNS TABLE
AS
RETURN
(
	SELECT Mozos.IdMozo, Mozos.nombre, Mozos.apellido, SUM(Facturas.total) AS TotalFacturado, SUM(Facturas.total) * 0.1 AS ValorPropina
	FROM Facturas
	INNER JOIN Mozos
	ON Mozos.IdMozo = Facturas.IdMozo
	GROUP BY Mozos.IdMozo, Mozos.nombre, Mozos.apellido
);