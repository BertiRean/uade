
/*************************************************************************************************** SQL DINAMICO *********************************************************************************************************
*	-Es la contruccion de una sentencia sql como cadena de caracteres y su posterior ejecucion																															  *	
*	-Me permite la escrituria de sentencias en tiempo de ejecucion compilando y realizando la ejecucion en otro proceso																									  *
*	-Se utiliza en precedimientos almacenados y en los lenguajes de programacion																									                                      *
*																										  																									  			  *
*	Pasos para realizar SQL Dinamico:                                                         																									                          *
*	-Declarar la variable para almacenar la sentencia. Se recomienda que el tipo de la variable sea "nvarchar" por utilizar caracteres extendidos 															              *
*	ademas este tipo de variables tienen longitud variable.																									  													          *
*	-Completar la sentencia																									  																							  *
*	-Ejecutar la sentencia. Mediante exec sp_executeSQL cadena / execute																									                                              *
*																										  																									              *
***************************************************************************************************************************************************************************************************************************/

/*Ejemplo:*/

create or alter procedure testDinamico 
	@nombreTabla varchar(100),
	@criterio varchar(100)
as
begin
	declare @sentencia nvarchar(1000)
	select @sentencia = 'select * from ' + @nombreTabla + ' where ' + @criterio
	print @sentencia
	exec sp_executeSQL @sentencia
end

testDinamico ' clubes ',' nroZona = 1 '


/************************************************************************************************** CURSORES *****************************************************************************************************************
*																									  																									                     * 
*	-Es la representacion de una consulta que es iterable (Me puedo mover dentro de esa consulta registro a registro como si fuese un archivo)																				 *
*																										  																									  				 *
*	Pasos:																									  																									  			 *
*	-Declarar el cursor con su tipo y la sentencia select asociada								(Declare)																									                 *
*	-Abrirlo (Como si fuese un archivo)															(Open)																									                     *
*	-Uso (recuperar el contenido de ese registro)												(Fetch)																									                     *
*	-Cerrarlo, no tengo mas acceso a la informacion pero el cursor todavia existe			    (Close)																									                     *
*	-Liberarlo																					(Dellocate)																									                 *
******************************************************************************************************************************************************************************************************************************/

/*Declaracion minima de un cursor */

--Este cursor se comporta de forma secuencial
DECLARE nombreCursor CURSOR 
	FOR sentencia_SQL_recuperacion 

/********************************************************************************************** TIPOS DE CURSORES ************************************************************************************************************
*	--Alcance:																									  																									         *
*		-Local: el cursor solo existe dentro del bloque de codigo																									  														 *
*		-Global: Se puede tener acceso al mismo dentro de todo el codigo, pero al quedar abiertos quedan siempre																									         *
*		tomados los recuros																									  																							     *
*	--																									  																									  				 *
*		-Forward-Only: Van solo hacia delante																									  																			 *
*		-Scroll: Me permite abrirlo y moverme a lo largo de ese cursor 																									  													 *
*	--																									  																									  				 *
*		-Read-Only: Solo lectura solo los puedo leer y mover hacia delante (Son los de menor coste																									   						 *
*		-Scroll-Lock: Voy a poder traer un registro y hacer una operacion de update sobre alguno de los campos de esa 																									     *
*		sentencia de recuperacion																									  																						 *
*																									  																									  					 *
*	-- Forma de acceder a los datos																									  																						 *
*		-Static: Ejecuto una sentencia y hago una copia, creo una tabla temporal y la asocio a mi cursor																									  				 *
*		si la tabla original se modifica esta no cambia.																									  																 *
*		-Key-Set: Es un cursor estatico de las direcciones donde se encuentran los registros que conformarian esa sentencia select (Cada vez																				 *
*		que quiero recuperar un valor con la clave va a los origenes de datos y recupera los valores, es decir que si alguien modifica los origenes																		     *
*		de datos voy a ver esos datos actualizados) Si me borran los registros no me entero																									  								 *
*		-Dynamic: Cualquier cambio que se realice en la tabla original se van ver reflejados en el cursor (ES EL CURSOR MAS COSTOSO) 																						 *
*																									  																									  					 *
*	Resumen:																									  																									         *
*	Lista de opciones:																									  																									 *
*	> Local - global 																									  																									 *
*	> Forward-Only - Scroll																									  																								 *
*	> Read-Only - Scroll-Only																									  																							 *
*	> Static - Key-Set - Dynamic																									  																					     *
*																									  																									                     *
*	Si no se le aclara ninguna opcion el cursor es el mas eficiente (local, forward-only, Read-Only y Static)																									             *
******************************************************************************************************************************************************************************************************************************/
declare @tipo varchar(3) , @numero int , @nombre varchar(50) ,@fechaNac datetime, @idclub int , @categoria int
declare @contador int

Declare miCursor Cursor for select * from Jugadores order by Id_Club
Open miCursor
	--lee el primer registro (Porque no estoy parado en el primer registro, por eso el next) 
	fetch next from miCursor into @tipo, @numero, @nombre,@fechaNac, @idclub, @categoria   --El fetch funciona como un read de un lenguaje de programacion, fech next me permite leer un archivo secuencial
	--Variable de estado @@Fetch-Status (Si devuelve un 0 es correcto, si da cualquier otro numero es porque se produjo un error)
	select @contador = 1
	while (@@FETCH_STATUS = 0)
	begin
		if(@contador = 3)
		begin
			print @nombre
			select @contador = 0
		end
	fetch next from miCursor into @tipo, @numero, @nombre,@fechaNac, @idclub, @categoria
	select @contador=@contador+1
	end

close miCursor
Deallocate miCursor

/**************************************************************************** CURSORES DEL TIPO SCROLL **********************************************************************************************************************
*	 																						  																						                                        *
*	Estos cursores se usan cuando tenemos una idea de lo que estamos buscando para realizar alguna operacion 																						                        *
* 																						                                                                                                                                    *
*	fetch (next | Last | Prior | First | relative n | absolute n ) 																						                                                                    *
*		>El absolute me lleva al registo "n" sin importar donde yo este parado. No se pueden usar numeros negativos, son posiciones absolutas 																				*
*		>El relative me lleva n veces adelante desde donde yo este parado, si el argumento es negativo me lleva n veces para atras 																						    *
*		>Prior me devuelve el anterior donde yo estoy parado 																						                                                                        *
*		>Next me devuelve el siguiente desde donde yo estoy parado 																						                                                                    *
*		>First me devuelve el primero de la lista	 																						                                                                                *
* 																						 																						 										    *
*****************************************************************************************************************************************************************************************************************************/

/*Ejemplo de cursores de tipo scroll */
select * from Jugadores

declare @idClub int , @nombre varchar(50) , @nrozona int

declare miCursor Scroll cursor for select * from Clubes
open miCursor

	fetch last from miCursor into @idClub , @nombre , @nrozona 
	print @nombre

	fetch prior from miCursor into @idClub , @nombre , @nrozona 
	print @nombre

	fetch absolute 10 from miCursor into @idClub , @nombre , @nrozona 
	print @nombre

close miCursor
deallocate miCursor

/*************************************************************************************** EJERCICIO DE LISTADO **************************************************************************************************************** 
*																																										                                                     *
																				listado de jugadores de cada club																						                     *
*																						                        																						                     *
*																					Club: XXXXXXXXXXXXXXXXXX																						                         *
*																						XXX 999999999 XXXXXXX																						                         *
*																						XXX 999999999 XXXXXXX																						                         *
*																						XXX 999999999 XXXXXXX																						                         *
*																						XXX 999999999 XXXXXX																						                         *
*																																														                                     *
*																					Club: XXXXXXXXXXXXXXXXXX																						                         *
*																						XXX 999999999 XXXXXXX																						                         *
*																						XXX 999999999 XXXXXXX																						                         *
*																						XXX 999999999 XXXXXXX																						                         *
*																						XXX 999999999 XXXXXXX																						                         *
*																																														                                     *
******************************************************************************************************************************************************************************************************************************/
declare @NombreClub varchar(50) , @tipoDoc varchar(3) , @nroDoc int, @nombreJugador varchar(50)
declare @clubAnterior varchar(50)

declare datos cursor for  --Cursor de tipo simple, forward only y read only
	select Clubes.Nombre, tipodoc , Nrodoc,Jugadores.Nombre
		from jugadores , Clubes
		where Jugadores.Id_Club=Clubes.Id_Club
		and Tipodoc = 'DNI'
		order by Clubes.Nombre, Jugadores.Nombre
		--Termina mi cursor ,el cursor esta definido para esta sentencia select, (Si ejecutas solo el select ves la tabla
open datos

	fetch next from datos into @NombreClub, @tipoDoc, @nroDoc, @nombreJugador --Me permite acceder al primer registro
	print 'Club: ' + @nombreClub
	print '      ' + @tipoDoc + ' ' + cast(@nroDoc as varchar(8)) + ' ' + @nombreJugador --NroDoc lo tengo que convertir de int a cadena de caracteres
	select @clubAnterior = @NombreClub
	while (@@FETCH_STATUS = 0)
	begin
		fetch next from datos into @NombreClub, @tipoDoc, @nroDoc, @nombreJugador --Leo el siguiente registro
		if(@clubAnterior=@NombreClub) --Todavia estoy en el mismo club
		begin
			print '      ' + @tipoDoc + ' ' + cast(@nroDoc as varchar(8)) + ' ' + @nombreJugador --NroDoc lo tengo que convertir de int a cadena de caracteres
		end
		else --Si no imprimo el siguiente club luego de una linea en blanco
		begin
			print ' ' --Imprimo una linea en blanco y luego el otro club
			print 'Club: ' + @nombreClub
			print '      ' + @tipoDoc + ' ' + cast(@nroDoc as varchar(8)) + ' ' + @nombreJugador --NroDoc lo tengo que convertir de int a cadena de caracteres
			select @clubAnterior = @NombreClub
		end
	end
close datos
deallocate datos