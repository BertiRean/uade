
/*************************************************************************************************** Identificadores *******************************************************************************************************
*																																												   										   *
*	>Estan formados por letras y numeros																																												   *
*	>son sin espacios																																												                       *
*	>Por costumbre se utiliza camelCase																																												       *
*	>Se pueden utilizar caracteres especailes para iniciar el identificador, cada uno tiene una funcion 																												   *
*		-"@"(Lo que viene a continuacion es una variable) 																																								   *
*		-"#"(tabla temporar) 																																												               *
*		-"_"																																												                               * 
*																																														                                   *
*	Ejemplos: jugadores (Nombre de tabla o campo)																																										   *
*			  total																																												                           *
*			  asignacion10																																												                   *
*																																												                                           *
***************************************************************************************************************************************************************************************************************************/




/*************************************************************************************************** Variables ************************************************************************************************************
*																																												                                          *
*	> Es una porcion de memoria con nombre y tipo en la cual puedo hacer determinadas operaciones																														  *
*	>Al definirla siempre va a tener un nombre y un tipo y es inmutable (Si es entera siempre va a ser entera) ,solo puedo mutar el contenido																			  *
*	>Las variables se declaran																																												              *
*	>El nombre comienza siempre con @																																												      *
*	>Hay distintos tipos de variables: tipos_de_dato (int,float,varchar,char,date) ; funciones cursor ; tablas (solo posee el aspecto conceptual																		  *
*	pero no fisicio, su tama�o esta en memoria por lo tanto es de rapido acceso pero su crecimiento esta limitado a la cantidad de espacio asignado																		  *
*	al DBMS)																																												                              *
*																																												                                          *
*	ejemplo: 																																												                              *
*	declare @miVariable int , @subVariable float																																										  *
*																																												                                          *
**************************************************************************************************************************************************************************************************************************/




/*************************************************************************************************** Bloques de codigo ****************************************************************************************************
*																																												                                          *
*	>el bloque se define mediante un begin y un end																																									      *
*	>El alacnce de una variable es el bloque 																																										      *
*																																												                                          *
**************************************************************************************************************************************************************************************************************************/




/*************************************************************************************************** Instrucciones ********************************************************************************************************
*																																			                                                                              *
*	>If else 																																			                                                                  *
*	>while (Condicion boolean)																																			                                                  *
*	>Switch Solo se puede utilizar dentro de una sentencia select 																																			              *
*																																			                                                                              *
**************************************************************************************************************************************************************************************************************************/




/************************************************************************************************* Control de errores ******************************************************************************************************
*																																			                                                                               *
*	>Se hacen a travez de una variable de estado que informan o guardan el resultado de ciertas cosas																								                       *
*	>Siempre empiezan con dos @@ por ser una variable del sistema																																			               *
*	>@@error indica el resultado de la ultima ejecucion de una sentencia																																			       *
*																																			                                                                               *
***************************************************************************************************************************************************************************************************************************/




/************************************************************************************************** Interaccion con el ambiente *********************************************************************************************
*																																			                                                                                *
*	>Solo esta definido con la visualizacion de resultados																																			                        *
*	>Se realiza mediante la instruccion "print" mostrando en consola el valor 																																			    *
*	>no se puede mezclar tipos de datos en un "print" se debe convertir todo a numeros o todo a cadenas en el caso de 																							            *
*	querer concatenar																																			                                                            *
*																																				                                                                            *
****************************************************************************************************************************************************************************************************************************/




/*************************************************************************************************** Funciones Integradas ***************************************************************************************************
*																																			                                                                                *
*	>Me permiten manejar o manipular los datos																																			                                    *
*	>Poseen distintas clasificaciones:																																			                                            *
*		>Caracteres (Len (Devuelve el tama�o de la cadena) / ltim (Saca los espacios a izquierda) / rtrim (saca los espacios a derecha) / substring )															            *
*		>Fechas, horas (dateadd (Me permite sumar fechas y si es negativa me la resta) / get date (Devuelve la fecha del sistema) )																						    *
*		>Conversion (cast / convert (Me permiten pasar el contenido de una variable o valor a otro formato) )																												*
*		>Etc.																																			                                                                    *
*																																			                                                                                *
*	ejemplo ejercicio 3																																			                                                            *
****************************************************************************************************************************************************************************************************************************/




/**************************************************************************************** Almacenar codigo en una base de datos **********************************************************************************************
*																															                                                                                                 *
*	Es un objeto dentro de la base de datos por lo tanto tengo que utilizar "create"																															             *
*	>Procedimiento formas:																															                                                                         *
*		>Procedimiento almacenado																															                                                                 *
*			>Mas poderoso porque la funciones de usuario son mas restrictivas																															                     *
*			>aceptan cualquier tipo de operacion																															                                                 *
*			>Su extension esta dada solamente por su capacidad																															                                     *
*			>tienen la capacidad de llamar a otros procedimientos (Hasta 32 niveles de anidamiento)																															 *
*			>Pueden resivir parametros (maximo 2048) (estos parametros pueden ser de entrada o salida)																													     *
*			>Puedo actualizar una tabla en particular (Modificarla)																															                                 *
*																																		                                                                                     *
*		>Funciones de usuario 																															                                                                     *
*			>Toda funcion lleva un par de parentesis pero puedo no tener parametros																															                 *
*			>Pueden ser recursivas																															                                                                 *
*			>Poseen parametros solo de entrada																															                                                     *
*			>Los tipos de salida pueden ser:																															                                                     *
*				>Tipo_de_dato																															                                                                     *
*				>Tabla (Variable de tipo tabla, no es una tabla real,no modifica a ninguna tabla)																															 *
*				>NO pueden devolver un cursor																															                                                     *
*																															                                                                                                 *
*	Ejemplo ejercicio 4 y 5 (Procedimiento almacenado)																															                                             *
*	Ejemplo 6 y 7 (Funciones)																															                                                                     *
*																															                                                                                                 *
******************************************************************************************************************************************************************************************************************************/


/******************************************************************************* Ejercicio 1 *********************************************************************************************************************************/

declare @valor int,@i int

set @valor=8 --Forma 1 de darle valor a una variable
select @valor = 8 --Forma 2 se prefiere esta forma

select @i=1

while(@i<10)
begin
	select @valor=@valor*@i
	select @i=@i+1
end

print @valor

/*****************************************************************************************************************************************************************************************************************************/
/******************************************************************************* Ejercicio 2 *********************************************************************************************************************************/
select count(*) from Jugadores --Me devuelve una tabla

declare @cantJugadores int
select @cantJugadores = count(*) from Jugadores --Me devuelve un escalar


if @cantJugadores < 500
begin
	print @cantJugadores
	print 'Que pocos jugadores'
end
else
	print 'cuantos jugadores'

/*****************************************************************************************************************************************************************************************************************************/
/******************************************************************************* Ejercicio 3 *********************************************************************************************************************************/
select nombre , len(nombre) as longitud
	from Jugadores
	where len(nombre) > 17			-- me devuelve los jugadores con la longitud de su nombre mayor a 17
	order by longitud desc

select *
	from jugadores					--Me devuelve todos los jugadores nacidos un dia 18
	where day(fecha_nac)=18

/*****************************************************************************************************************************************************************************************************************************/
/******************************************************************************* Ejercicio 4 *********************************************************************************************************************************/
Create procedure primero
--Los parametros se pueden obviar si se necesita un procedimiento sin parametros
	@parametro1 int,
	@parametro2 varchar(50)
as 
begin
--instrucciones
	declare @i int
		select @i=0
	while (@i< @parametro1)
	begin
		print @parametro2
		select @i=@i+1
	end
end

--Ejecucion del procedimiento
declare @cantidad int
declare @texto varchar(100)

select @cantidad = 10
select @texto = 'otro texto'

exec primero @cantidad, @texto --Execute o exec me ejecuta el procedimiento

/*****************************************************************************************************************************************************************************************************************************/
/******************************************************************************* Ejercicio 5 *********************************************************************************************************************************/
/*
Procedimiento que controla que exista una tabla
	Si existe la borra y despues la vuelve a crear
	si no existe la crea
	inserta una cantidad de valores pasados como parametros
	lista el contenido
*/
select * from sysobjects --Sysobjet me devuelve una talba con todas las tablas,funciones y claves creadas ademas de tablas del sistema 

create procedure segundo
	@cantidadRegistros int
as
begin
	if exists (select * from sysobjects where xtype = 'U' and name = 'PruebaProcedure')
		drop table PruebaProcedure --elimina la tabla
		
	create table PruebaProcedure (campo1 int, campo2 varchar(50), constraint pk_PruebaProcedure primary key (campo1))
	
	declare @contador int
	select @contador = 0 

	while(@contador < @cantidadRegistros)
		begin
			insert into PruebaProcedure values (@contador,'Registro Nro' + CAST(@contador as varchar(2)))
			select @contador=@contador+1
		end

		select * from PruebaProcedure
end

exec segundo 8 --Prueba de ejecucion de la funcion

/*****************************************************************************************************************************************************************************************************************************/
/******************************************************************************* Ejercicio 6 *********************************************************************************************************************************/
create function funcion01 ( --Funcion de escalar (devuelve un valor)
	@parametro1 varchar(50)
)
returns int --tipo de valor que devuelve la funcion
as 
begin
	return len(@parametro1) --El valor que devuelve la funcion
end
--Ejecucion de la funcion:
select dbo.funcion01('Hola como estan') --Para ejecutar una funcion siempre adelante del nombre tengo que poner el creador 

/*****************************************************************************************************************************************************************************************************************************/
7/******************************************************************************* Ejercicio 7 *********************************************************************************************************************************/
create function funcionTabla01 ( --Funcion del tipo tabla
	@Categoria int
)
returns table
return select * from Jugadores where Categoria = @Categoria

--Ejecucion de la funcion:
	select * from funciontabla01 (85)

/*****************************************************************************************************************************************************************************************************************************/
/******************************************************************************* Ejercicio 8 *********************************************************************************************************************************/
create or alter function funcionTabla02 ( --Si pongo "alter" en vez de create me modifica el codigo viejo por el codigo nuevo (reemplaza todo el procedimiento junto)
										  -- tambien puedo poner create o alter , en el caso de tener la tabla creada la reemplaza y si no la crea
	@categoria int
)
returns @resultado table (campo1 varchar(5), campo2 int , campo3 varchar(50)) --Devuelvo una variable resultado del tipo tabla
as
begin
	insert into @resultado select tipodoc,nrodoc,nombre 
								from Jugadores 
								where Categoria = @categoria
	return --se pone el ruturn solo y me devuelve la tabla @resultado
end

--Ejecucion de la funcion
select * from funcionTabla02(85)

/*****************************************************************************************************************************************************************************************************************************/
/******************************************************************************* TAREA  *********************************************************************************************************************************/
/*Escribir un procedimiento que reciba una cantidad entera correspondiente a la cantidad de jugadores de un club
y realice las siguientes acciones:
	>Verifique si hay clubes que tiene al menos esa cantidad de jugadores.
		>Si ninguno tiene esa cantidad emita un mensaje.
		>Si al menos uno tiene esa cantidad o mas mostar el nombre de esos clubes
*/
create or alter procedure tarea 
	@cantidadRegistros int
as
begin
	if not exists ( select Id_Club, count(*) from jugadores group by id_club having count(*) >= @cantidadRegistros)
					print 'No existe ningun equipo con esa cantidad de jugadores'
	else
					begin
						select Id_Club, count(*) 
							from jugadores 
							group by id_club 
							having count(*) >= @cantidadRegistros
					end

end

exec tarea 20