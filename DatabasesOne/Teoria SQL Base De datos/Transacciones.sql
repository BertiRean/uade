/*********************************************************************************************** TRANSACCIONES ***************************************************************************************************************
*	-Me permite asegurar que las operaciones sean consistentes                                                                                                                                                               *
*	-Es un conjunto de operaciones que necesito que se cumplan todas y si no se pueden cumplir todas no se hacen                                                                                                             *
*	-es una unidad logica de trabajo                                                                                                                                                                                         *
*	-trabajar con transacciones es mas lento.                                                                                                                                                                                *
*   -Me permite recuperar una base de datos cuando hay una falla																																							 *
*																																																                             *	
*  TODOS LOS ARCHIVOS DE LA BASE DE DATOS DONDE TENGO LOS DATOS SE PONEN EN UN DISCO SEPARADO A LOS ARCHIVOS DE LOG YA QUE ESTO ME PERMITE RECUPERAR LA BD EN CASO DE PERDIDAS O MAL FUNCIONAMIENTO DEL DISCO                *
*																																																							 *
*	Demarcadores:                                                                                                                                                                                                            *
*		-Son palabras reservadas que me indican el inicio de una transaccion y el final de una transaccion                                                                                                                   *
*			=> Begin tran[saction] (inicio de la transaccion, se puede poner toda la palabra o tran)                                                                                                                         *
*			=> final de la transaccion -> Exito: commit                                                                                                                                                                      *
*									   -> Fracaso: rollback                                                                                                                                                                  *
*										Estado de la operacion se obtiene con @@error que me indica el resultado de la ultima sentecia ejecutada                                                                             *
*	                                                                                                                                                                                                                         *
*                                                                                                                                                                                                                            *
*	Propiedades de las transacciones:                                                                                                                                                                                        *
*		>Atomicidad: Cuando yo tabajo con las transacciones estas se completan o no se hacen                                                                                                                                 *
*		>Consistencia: Paso de un estado consistente a otro. No rompe ninguna regla de integridad                                                                                                                            *
*		>Aislamiento (Isolation): Ninguna transaccion sabe que esta haciendo otra transaccion simultanea                                                                                                                     *
*		>Durabilidad: Los datos van a persistir hasta la proxima transaccion                                                                                                                                                 *
*																																																							 *
*																																																							 *
*	Propiedades ACID ==> Contexto de transacciones serializadas.                                                                                                                                                             *
*						 (Las transacciones se ejecutan una despues de la otra)     																																		 *
*                                                                                                                                                                                                                            *
*                                                                                                                                                                                                                            *
*******************************************************************************************************************************************************************************************************************************/

begin tran --Demarcador de inicio de la transaccion
declare @control int --ESTA VARIABLE TIENE QUE ESTAR SIEMPRE, me permite corroborar si todas las operaciones son correctas
select @control=0 

insert into clubes values (99, 'prueba' , 3)
select @control=@control +@@error --Corroboro si la primera operacion es correcta

set dateformat 'dmy' --Coloco el formato dia mes a�o
insert into Jugadores values ('DNI' , 999999999, 'Juan perez' ,'20/12/2000' , 84 , 99)
select @control=@control +@@error --Corroboro si la segunda operacion es correcta

insert into clubes values (100, 'prueba' , 3)
select @control=@control +@@error --Corroboro si la tecera operacion es correcta

/*De aca para abajo es del demarcador de fin de la transaccion*/
if(@control = 0 )    --Si control es igual a 0 significa que ninguna de mis operaciones dio error 
	commit
else
	Rollback

/*Prueba para ver si se agregaron las filas*/
select * from Clubes
select * from Jugadores order by Id_Club

/*Para volver a probar ejecutar esto */
delete from Jugadores where Nrodoc = 999999999  
delete from Clubes where id_club >75