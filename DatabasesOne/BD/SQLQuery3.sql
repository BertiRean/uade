	-- Nombre de los clubes y la cantidad de jugadores que tienen
-- uso de una subconsulta en la clausula from
Select nombre,totalJugadores
	from clubes, (select id_club, count(*) as totalJugadores from jugadores group by id_club) x
	where clubes.Id_club = x.Id_club

--Uso de subconsultas en una restriccion
--Operadores de conjunto
--all
select * 
	from general 
	where puntos >= all (select puntos from general)

--in
select *
	from jugadores 
	where Id_Club in(select Id_Club from clubes where Nrozona = 1)

-- exists
select *
	from general
	where exists (select * from clubes where 1 = 2)

--Estructura: valor operador conjunto
select c.nombre 
	from clubes c, jugadores j
	where c.Id_Club = j.Id_Club
	group by j.Id_Club, c.Nombre
	having count(*) >= all (select count (*) from jugadores group by Id_Club)



