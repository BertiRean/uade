DBMS ==> DDL (lenguaje definicion de datos)
	 ==> DML (Lenguaje de manipulacion de datos)

SQL.
DDL: Create - Drop


Create tipo_a_crear identificador definicion_del_objeto 
Drop tipo_a_borrar identificador /*(ddl) me permite borrar  */
Alter tipo _a_modificar identificador definicion_de_la_modificacion /*(ddl) me permite cambiar cosas de la estructura,por ejemplo agregar o eliminar un campo o borrar
una clave primaria o foranea*/ 


Create table identificador ==> Es equivalente a una relacion base 
(
	a) Campos
	b) Restricciones
	C) Clave primaria
	[d) Clave foraneas] // opcional

)// Siempre tiene un  parentesis que abre y otro que cierra , lo que esta en items es lo que debe contener
//cada tabla (a,b,c,d).

Create table ejemplo01
(
	campo_01 decimal(10,2) not null, /*Tipo de campo con coma fija (2 decimales maximo), con "not null" especifica que este campo no acepta nullos*/
	campo_02 varchar(150) null, /*En este caso si acepta nulos*/
	campo_03 char(2) not null default 'Si', /* No acepto nulos y en el caso de no asignarle nada va a colocora 'Si' */
	campo_04 int constraint chk_validos check (campo_04 in (3,5,7,9)), /*me verifica si los valores que tomo el campo son 3,5,7 o 9 */
	/*las restricciones de campos o atributos solo hacen referencia a esos campo o atributos ,en el campo 04 no podria poner el campo_01 dentro de los parentesis */
	constraint pk_ejemplo01 primary key (campo_01) /*(clave primaria como convencion se le pone pk al principio del nombre)*/
	)

Create table ejemplo02
(
	campo_01 decimal(10,2) not null,
	campo_06 int not null,
	campo_07 char(2) not null default 'Si',
	campo_08 int constraint chk_valido_campo_08 check (campo_04 in (3,5,7,9)),
	constraint pk_ejemplo01 primary key (campo_06) /*(clave primaria)*/
	constraint fk_ejemplo02_ejemplo01 fereign key (campo_01) references ejemplo01 (campo_01) /* clave foranea ,se le pone fk al principio del nombre */
	
	)

	DML consulta , agregado ,eliminacion y modificacion ==> de la informacion /* estas operaciones me permiten interactuar con la informacion*/
		operaciones CRUD .

	
	
	Agregar informacion ==> Insert (union) /*las cabeceras deben ser iguales*/

	insert [into] identificador [(lista_de_campos)] values (lista_de_valores) /*El into es opcional*/ 
	lista_de_valores = {cabecera_tabla | lista_de_campos} /*La lista de valores es igual a la cabecera de la tabla o a la lista de campos */

	eliminar informacion ==> delete 

	delete [from] identificador [restriccion] /* borra el cotenido pero no la estructura) */



	restriccion ==> expresion booleana. (verdadero | falso) /* posee una logica de 3 estados*/
		where		logica de 3 estados ==> si en la expresion booleana interviene un null ==> El resultado es indeterminado
					expresion booleana. (verdadero | falso | indeterminado)
					(campo is null | campos is not null) /* pregunto si un campo es o no nulo*/
					cada campo nulo es distinto "null<>null" ==> Indeterminado
					las restricciones son solo para escalares.
	
	
	
	
	update indentificador set lista_de_operacion [restriccion] /* toma una tabla y para cada uno de los registros de esa tabla coloca esa restriccion*/

	operacion ==> campo = {valor| expresion | valor_de_otro_campo}
				  Todos los campos involucrados en el update corresponden al mismo registro



	Sentencia select: /* esta formada por distintas clausulas*/
	orden de ejecucion "1)":
	5) select {* (el asterisco son todos los campos) | lista_de_campos | expresion as identificador } /*me permite recuperar informacion de mi base de datos*/
	7)	 [into] -> opcional, nombre de una tabla para guardar el resultado , si la tabla no existe la crea 
	1)	 from lista_de_origenes_de_dato (tablas) -> debe figurar siempre,si coloco dos tablas es equivalete al producto cartesiano extendido (PCE) salvo
	que voy a tener columnas repetidas si las hay entre dos tuplas
	2)	 where restriccion -> Opcional, es una restriccion
	3)	 group by lista_de_campos (criterio) -> opcional, toma el conjunto de registros que se formo del PCE y con un criterio los procesa ,es parecido al summarizar del algebra
	4)	 having	-> opcional,es una restriccion para grupos (no acepta escalares)
	6)	order by {lista_de_campos| posicion_ordinal}-> opcional, se coloca una lista de campos por los que quiero ordenar mi resultado  o por su posicion ordinal

group by ++> en la clausula select solo puede estar el criterio y hacer las operaciones de totales



