-- inner join == reunion

select *  --reunion tradiciones
	from clubes, Jugadores
	where clubes.Id_Club = Jugadores.Id_Club

select * --Join interno 
	from clubes inner join jugadores on clubes.Id_Club = Jugadores.Id_Club

select * 
	from clubes left join jugadores on clubes.Id_Club = Jugadores.Id_Club

select * 
	from clubes right join jugadores on clubes.Id_Club = Jugadores.Id_Club

insert into clubes values (55, 'prueba_datos 55', 1)
insert into clubes values (60, 'prueba_datos 60', 2)
insert into clubes values (65, 'prueba_datos 65', 1)
insert into clubes values (70, 'prueba_datos 70', 3)
insert into clubes values (75, 'prueba_datos 75', 1)

--Cuales son los clubes que no tienen jugadores 

select clubes.nombre	
	from clubes left join jugadores on clubes.Id_Club = Jugadores.Id_Club
	where jugadores.Nombre is null 

--
Select * 
	from clubes full outer joun jugadores on clubes.id_club = jugadores.id_club 
	
--Diferencia 
sekect id_club from clubes
EXCEPT
select distinct id_club from jugadores

--otra forma de diferencia
Select * 
	from clubes
	where Id_Club not in (select Id_Club from jugadores)

-- subconsulta correlacionada:
	select * 
		from clubes where exists (select * from jugadores where jugadores.Id_Club = clubes.Id_Club)