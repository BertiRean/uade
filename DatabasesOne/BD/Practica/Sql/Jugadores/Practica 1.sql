--Determinar el equipo con la mayor cantidad de partidos ganados. 
	select * from General --Si ejecuto solo esta linea ya me imprime la tabla 
		where ganados >= all (select ganados 
								from General) 
	
--Determinar el equipo con el mejor y el peor promedio. 
	select * 
		from General
		where (promedio <= all (select promedio
								  from general)) 
		or	  (promedio >= all (select promedio
								  from general))


--Determinar el jugador con el n�mero de documento m�s bajo.
	select * 
		from Jugadores
		where Nrodoc <= all (select Nrodoc
							   from Jugadores)

--Listar los jugadores m�s j�venes y los m�s viejos del torneo. 
	select *
		from jugadores
		where Fecha_Nac <= all ( select Fecha_Nac
			   				       from Jugadores)
		or Fecha_Nac >= all ( select Fecha_Nac
								from Jugadores)

--Listar el jugador con el n�mero de documento m�s alto que pertenezca al equipo con el promedio m�s bajo. 
	select * 
		from jugadores 
		where Jugadores.Id_Club in (select Id_Club 
									from general 
									where Promedio<=all(select Promedio 
														from general))  --Debo reducir las subconsultas en from
		and Nrodoc >= all(select NroDoc
							from jugadores
							where Jugadores.Id_Club = (select Id_Club 
														 from general 
														 where Promedio<=all(select Promedio
																			   from general)))
--Listar nombre del equipo y promedio de todos los equipos con un promedio inferior al m�s alto. 
	select nombre,promedio 
	  from general
	  where Promedio<(select promedio
						from general 
						where promedio >=all (select promedio
											    from General))


--7 Listar nombre del equipo, nombre del jugador y fecha de nacimiento de los jugadores m�s viejos de aquellos equipos con un promedio inferior al promedio del equipo con la menor cantidad de partidos ganados.
	--1 Listar nombre del equipo,nombre del jugador y fecha de nacimiento de los jugadores mas viejos de los
	--2 equipos con un promedio inferior al promedio del equipo con
	--3 menor partidos ganados

	--Forma 1
	select * from general inner join Jugadores j1 on general.Id_Club=j1.Id_Club
	where promedio <= all(
	Select distinct promedio from general where ganados <= all (select ganados from general))
	and fecha_nac>= all (select fecha_nac from jugadores j2 where j2.Id_Club = j1.id_club)

	--Forma 2
	select Jugadores.Nombre as NombreEquipo , Jugadores.Fecha_Nac,Jugadores.Id_Club ,General.Nombre, General.Promedio
		from Jugadores right join General on general.Id_Club=Jugadores.Id_Club
		where (general.Id_Club in ( select id_club
									from General
									where Ganados <= all(select Ganados
									from General)
						 )
		and promedio <= all(select promedio
								from General
								where ganados <= all(select Ganados
													   from General) 
							)
		and Fecha_Nac >= all(select Jugadores.Fecha_Nac
							from Jugadores right join General on general.Id_Club=Jugadores.Id_Club
							where (general.Id_Club in ( select id_club
									from General
									where Ganados <= all(select Ganados
														   from General)
													  )
							and promedio <= all(select promedio
												  from General
												 where ganados <= all(select Ganados
																        from General) 
										       )
						       )

							)
			)

--8 Listar nombre y n�mero de documento, de aquellos jugadores con el menor n�mero de documento que pertenezcan a un equipo donde la cantidad de partidos ganados sea igual a la cantidad de partidos perdidos. 
	select Nrodoc ,Nombre
		from jugadores
		where (Id_Club in (select id_club
							from general
							where ganados=Perdidos
						 )
				)
		and Nrodoc <= all (select Nrodoc
								from jugadores
								where (Id_Club in (select id_club
														from general
														where ganados=Perdidos
						 )
				)
						)

--9 Determinar la categor�a con el mejor y peor promedio. 
	select categoria
	from (select 84 as categoria,* from poscate184
		  union
		  select 84 as categoria,* from poscate284
		  union
		  select 85 as categoria,* from poscate185
		  union
		  select 85 as categoria,* from poscate285
		) pepe
	where promedio >= all ( select promedio
								from (select 84 as categoria,* from poscate184
									  union
									  select 84 as categoria,* from poscate284
									  union
									  select 85 as categoria,* from poscate185
									  union
									  select 85 as categoria,* from poscate285
							) pepe2
						 )
	or promedio <= all ( select promedio
							from (select 84 as categoria,* from poscate184
								  union
								  select 84 as categoria,* from poscate284
								  union
								  select 85 as categoria,* from poscate185
								  union
								  select 85 as categoria,* from poscate285
							) pepe2
						)
		

-- Determinar la m�xima diferencia de goles (goles - golesV) de un partido en el que haya participado el equipo con el peor promedio del campeonato. 
	select * ,ABS(GolesL-GolesV) as diferencia
		from partidos
		where (Id_ClubL in( select Id_Club
							from general
							where promedio <= all( select promedio from general))
		or
				Id_ClubV in (select Id_Club
							from general
							where promedio <= all( select promedio from general))
				)
		and ABS(GolesL-GolesV) >= all (select ABS(GolesL-GolesV)
										from partidos
										where (Id_ClubL in( select Id_Club
															from general
															where promedio <= all( select promedio from general))
										or
												Id_ClubV IN (select Id_Club
												from general where Promedio <= all( select Promedio from General)))
							)
				

