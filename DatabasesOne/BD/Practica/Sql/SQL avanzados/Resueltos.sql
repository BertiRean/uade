﻿
/**********************************************************************************************************************************************/
/*
					  ____                                     _   _               _                  _                 
					 |  _ \   _ __    ___     ___    ___    __| | (_)  _ __ ___   (_)   ___   _ __   | |_    ___    ___ 
					 | |_) | | '__|  / _ \   / __|  / _ \  / _` | | | | '_ ` _ \  | |  / _ \ | '_ \  | __|  / _ \  / __|
					 |  __/  | |    | (_) | | (__  |  __/ | (_| | | | | | | | | | | | |  __/ | | | | | |_  | (_) | \__ \
					 |_|     |_|     \___/   \___|  \___|  \__,_| |_| |_| |_| |_| |_|  \___| |_| |_|  \__|  \___/  |___/

					 https://www.messletters.com/es/big-text/
                                                                                                    
*/.
/************************************************************************************************************************************************/




--1.Crear para cada una de las tablas un procedimiento almacenado para insertar datos. Debe generar el valor de la clave primaria, tomando como base el último valor ingresado.
--	Las tablas para las cuales hay que crear el procedimiento son: Localidades, Sectores, Mesas, Rubros, Unidades, FactorConversión, MotivosRechazo.

/*
select * from Localidades
*/


create or alter procedure creadorLocalidades
	@localidadRegistros varchar(100)
as
begin
	if exists (select * from sysobjects where xtype= 'U' and name = 'Localidades')
	begin
		declare @idAnterior int, @idActual int
		select @idAnterior = MAX(IdCodPos) from Localidades
		if (@idAnterior is null) --Si la tabla esta vacia
			begin
			select @idActual =1
			end
		else
			begin
				select @idActual=@idAnterior+1
			end
		
		insert into Localidades values (@idActual,@localidadRegistros)
		end
	else 
	begin
		print 'no existe la tabla Localidades'
	end
end

exec creadorLocalidades 'villa crespo'

/*El resto seria igual pero cambiando el nombre de localidades por otro*/

--2.Crear un procedimiento almacenado que reciba la descripción de un nuevo plato, el precio, el rubro al que pertenece
--	y la receta del mismo, genere el número de plato correspondiente y lo guarde en las tablas correspondientes.
select * from platos
select * from Recetas
select * from rubros
create or alter procedure PlatosYRubros
	@descripcion varchar (50),
	@precio float,
	@rubro varchar(50),
	@receta varchar(50)
as
begin
	declare @idAnteriorRubro int, @idActualRubro int
	select @idAnteriorRubro = MAX(IdRubro) from Rubros
	if exists (select * from sysobjects where xtype= 'U' and name = 'Rubros')
	begin
		if not exists (select * from Rubros where Rubros.descripcion = @rubro)
		begin
			if (@idAnteriorRubro is null) --Si la tabla esta vacia
				begin
				select @idActualRubro=1
				end
			else
				begin
					select @idActualRubro=@idAnteriorRubro+1
				end
		
			insert into Rubros values (@idActualRubro,@rubro)
			end
		end
	else 
	begin
		print 'no existe la tabla Rubro'
	end																																	
	if exists (select * from sysobjects where xtype= 'U' and name = 'Platos')
	begin
		if not exists (select * from Platos where descripcion = @descripcion)
		begin
			declare @idAnterior int, @idActual int
			select @idAnterior = MAX(IdPlato) from Platos
			if (@idAnterior is null) --Si la tabla esta vacia
				begin
				select @idActual=1
				end
			else
				begin
					select @idActual=@idAnterior+1
				end
		
			insert into Platos values (@idActual,@descripcion,@precio,@idActualRubro)
			end
		end
	else 
	begin
		print 'no existe la tabla Platos'
	end
	if not exists (select * from Recetas where @receta = receta )
	begin
		declare @IdAnteriorR int, @IdActualR int
		select @IdAnteriorR = MAX(IdReceta) from Recetas
		if (@IdAnteriorR is null)
			select @IdActualR = 1
		else
			select @IdActualR = @IdAnteriorR + 1
		insert into Recetas values (@IdActualR, @receta, @IdActual)
	end
	else
		print 'Error: Ya existe esta receta para este plato'
end

exec PlatosYRubros 'papas rellenas', 400, 'paperia','Papita'
select * from Platos
select * from Recetas
select * from Rubros

--3.Crear un procedimiento almacenado que reciba el idPlato, el  idIingrediente y la cantidad y verifique que ambos existan, de ser así inserte el registro en la tabla PlaIng. 
create or alter procedure procedimiento3
	@idPlatoParametro int,
	@idIngredienteParametro int,
	@cantidadParametro int
as
begin
	
	if exists (select * from platos,Ingredientes where platos.IdPlato=@idPlatoParametro and Ingredientes.IdIngrediente=@idIngredienteParametro)
		begin
		if not exists(select * from PlaIng where IdPlato=@idPlatoParametro and IdIngrediente=@idIngredienteParametro)
		begin	
			insert into PlaIng values (@idPlatoParametro , @idIngredienteParametro, @cantidadParametro)
		end
		end
end 

exec procedimiento3 12,13,2

--4.Crear un procedimiento almacenado que reciba la descripción de un proveedor y la de un ingrediente y genere la vinculación en la tabla correspondiente.
--	Debe verificar que los valores ingresados existan. 
select * from Proveedores
select * from Ingredientes where Ingredientes.descripcion='Gancia'
select * from ProIng
create or alter procedure proveedorIng
	@descripcionProv varchar(50),
	@descripcionIng varchar(50)
as
begin 
	declare @auxiliar1 int
	print @descripcionProv
	print @descripcionIng
	if exists (select * from Proveedores where Proveedores.IdProveedor=@descripcionProv) 
		and	exists (select * from Ingredientes where Ingredientes.descripcion=@descripcionIng)
		begin
			select @auxiliar1=(select IdIngrediente from Ingredientes where Ingredientes.descripcion=@descripcionIng)
			insert into ProIng values (@auxiliar1,@descripcionProv)
		end
	else
		print 'no existe el ingrediente o el proveedor ingresado'
end

exec proveedorIng '10','Leche'

--5.Crear un procedimiento que utilizando dos cursores realice un listado con el siguiente formato, ordenarlo por código de sector, nombre del mozo, fecha y número de factura
/*												  Sector: Descripción_Sector
            											  Mozo:	Nombre_Mozo
														Fecha		  NroMeza	NroFactura	Importe
														99/99/9999	    99		999-99999	99999.99
														99/99/9999	    99		999-99999	99999.99
														99/99/9999	    99		999-99999	99999.99
												  
												  Cantidad Facturas Mozo  :      999		Importe:	99999.99
												  Cantidad Facturas Sector:      999		Importe:	99999.99
*/


declare parte1 cursor for --cursor simple
	select Mozos.nombre, Mozos.apellido, Facturas.fechaApertura, Facturas.NroFactura ,Mesas.IdMesa, Facturas.total, x.CantidadFacturas, x.ImporteTotal
		from Mozos, Facturas,Mesas,Sectores,(select SUM(Facturas.NroFactura) AS CantidadFacturas, SUM(facturas.total) as ImporteTotal, idMozo
												from Facturas
												group by IdMozo) x
		where Mozos.IdMozo=Facturas.IdMozo and Mesas.IdMesa=Facturas.IdMesa and Mesas.IdSector=Sectores.IdSector and x.IdMozo=Facturas.IdMozo
		order by sectores.descripcion,Mozos.nombre
	
declare parte2 cursor for
	select Sectores.descripcion,SUM(y.CantidadFacturas) as cantidadFacturasSector , SUM(y.importeTotalMesa) as ImporteTotalSector from sectores,Mesas
	inner join (select COUNT(Facturas.NroFactura) AS CantidadFacturas,SUM(Facturas.total) as ImporteTotalMesa, IdMesa
					from Facturas
					group by IdMesa) y on y.IdMesa=Mesas.IdMesa
	where Mesas.IdSector=Sectores.IdSector
	group by sectores.descripcion
	order by Sectores.descripcion


declare @descripcionSector varchar(50) , @NombreMozo varchar(50),@ApellidoMozo varchar(50), @fecha datetime , @NroMeza int ,@NroFactura int , @Importe float , @cantidadFacMozo int , @cantidadFacSector int,@importeMozo float, @importeSector float
declare @mozoAnterior varchar(50), @sectorAnterior varchar (50)

set dateformat dmy

open parte2
	fetch next from parte2 into @descripcionSector ,@cantidadFacSector , @importeSector 
	print 'Sector: '+ @descripcionSector
	select @sectorAnterior=@descripcionSector
	while (@@FETCH_STATUS=0)
	begin
		if(@descripcionSector= @sectorAnterior) --Todavia estoy en el mismo sector
		begin
		set dateformat dmy
			open parte1
				fetch next from parte1 into @NombreMozo,@ApellidoMozo, @fecha , @NroFactura,@NroMeza,@Importe, @cantidadFacMozo,@importeMozo
				print '        '+'Mozo: '+ @NombreMozo
				print '        ' + 'Fecha' + '            '+ 'NroMeza'+'            '+'NroFactura'+'            '+ 'importe'
				print '        ' + CONVERT(varchar , @fecha,3) +  '            '+ cast(@NroMeza as varchar(50)) + '            '+cast(@NroFactura as varchar(50))+ '            '+ cast(@importe as varchar(50))
				select @MozoAnterior=@NombreMozo
				while( @@FETCH_STATUS=0)
				begin
					fetch next from parte1 into @NombreMozo,@ApellidoMozo, @fecha , @NroFactura,@NroMeza,@Importe, @cantidadFacMozo,@importeMozo
					if(@NombreMozo=@mozoAnterior) --Todavia estoy con el mismo mozo
					begin
						print '        ' + CONVERT(varchar , @fecha,3) +  '            '+ cast(@NroMeza as varchar(50)) + '            '+cast(@NroFactura as varchar(50))+ '            '+ cast(@importe as varchar(50))
					
					end
					else
						begin
							print 'Cantidad Facturas Mozo: ' + cast(@cantidadFacMozo as varchar(50)) + '      ' + 'Importe: ' + cast(@importeSector as varchar(50))
							print ' ' 
							print '        '+'Mozo: '+ @NombreMozo
							print '        ' + 'Fecha' + '            '+ 'NroMeza'+'            '+'NroFactura'+'            '+ 'importe'
							print '        ' + CONVERT(varchar , @fecha,3) +  '            '+ cast(@NroMeza as varchar(50)) + '            '+cast(@NroFactura as varchar(50)) + '            '+ cast(@importe as varchar(50))
							select @mozoAnterior=@NombreMozo
						end
				end
			close parte1
			deallocate parte1
		end
		else
		begin
			print 'Cantidad Facturas Sector: ' + cast(@cantidadFacSector as varchar(50)) + '      ' + 'Importe: ' + cast(@ImporteSector as varchar(50))
			print ' '
			print 'Sector: '+ @descripcionSector
			select @sectorAnterior=@descripcionSector
		end
		fetch next from parte2 into @descripcionSector ,@cantidadFacSector , @importeSector 
	end

close parte2
deallocate parte2

--6.Crear un procedimiento que reciba una fecha y genere una tabla conteniendo todas las facturas del mes correspondiente a la fecha ingresada, ordenadas por sector y mesa y devuelva en una variable de output 
--el total de los registros grabados. La tabla generada deberá contener el id del sector, el id de la mesa, el nombre del mozo, el numero de factura y el total de la misma. 
--El nombre de la tabla será Facturacion_XXXXXX donde XXXXXX corresponderá al nombre del mes. Si la tabla existe, debe eliminarse y crease nuevamente con las reglas de integridad correspondiente. 

create or alter procedure tablaFacturas
	@fecha datetime
as
begin
	declare @IdSector int, @Idmesa int, @mozo varchar(100), @fact int ,@totalFact int	
	declare fechas cursor for
		select Sectores.IdSector,Mesas.IdMesa,Mozos.IdMozo,Facturas.NroFactura,Facturas.total 
		from Facturas,Sectores,Mesas,Mozos
		where Facturas.IdMesa=Mesas.IdMesa and Mesas.IdSector=Sectores.IdSector and Mozos.IdMozo=Facturas.IdMozo and MONTH(Facturas.fechaApertura)=MONTH(@fecha)
		order by Sectores.IdSector,Mesas.IdMesa
	open fechas 
	
	declare @nombreMes varchar(50)
	select @nombreMes =DATENAME(month,@fecha)

	declare @nombreTabla nvarchar(50)
	select @nombreTabla = 'Facturacion'+@nombreMes

	declare @eliminarTabla as nvarchar(100)
	select @eliminarTabla= 'drop table '+@nombreTabla 

	--Crear Tabla Dinamica
	declare @crearTabla nvarchar(1000)
	select @crearTabla= 'create table '+@nombreTabla+' (IdSector int, Idmesa int, mozo varchar(100), NroFact int ,totalFact int)'


	declare @mostrarTabla nvarchar(50)
	select @mostrarTabla= 'select * from '+ @nombreTabla 

	if exists (select * from sysobjects where xtype= 'U' and name = @nombreTabla)
		exec sp_executeSQL @eliminarTabla
	
	exec sp_executeSQL @crearTabla

	fetch next from fechas into @IdSector , @IdMesa, @mozo,@fact ,@totalFact
	declare @datos nvarchar(100)
	select @datos= 'insert into ' + @nombreTabla + ' values ( '+cast(@IdSector as varchar(50)) +','+cast(@IdMesa as varchar(50))+','+ cast(@mozo as varchar(50)) + ','+ cast( @fact as varchar(50))+ ','+ cast( @totalFact as varchar(50))+ ')'
	while(@@FETCH_STATUS=0)
	begin
		select @datos= 'insert into ' + @nombreTabla + ' values ( '+cast(@IdSector as varchar(50)) +','+cast(@IdMesa as varchar(50))+','+ cast(@mozo as varchar(50)) + ','+ cast( @fact as varchar(50))+ ','+ cast( @totalFact as varchar(50))+ ')'
		exec sp_executeSQL @datos
		fetch next from fechas into @IdSector , @IdMesa, @mozo,@fact ,@totalFact
	end
	exec sp_executeSQL @mostrarTabla
	close fechas
	deallocate fechas
end

exec tablaFacturas '20190114 00:00:00.000'

select * from FacturacionEnero

-- 7.Crear un procedimiento que genere los pedidos de cotización para cada uno de los proveedores. Los nombres de estas serán PC_XXXXXX donde XXXXXX corresponde al nombre del proveedor.
--	La tabla contendrá para artículo que el proveedor provea y que se encuentre su stock por debajo del punto de reposición los siguientes campos: idIngrediente, descripción, cantidad a comprar. 
--  El procedimiento deberá verificar que las tablas no existan, por lo que deberá eliminar todas las tablas de pedido de cotización antes de comenzar. 

create or alter procedure TablasPedidosDeCotizacion
	
as
begin
	declare @NombreProv varchar(50),@idIngrediente int , @descripcion varchar(50), @cantidad int
	declare PedidosDeCotizacion Cursor for 
		select Ingredientes.IdIngrediente,Ingredientes.descripcion,Ingredientes.cantComp, Proveedores.razsoc from Proveedores,ProIng,Ingredientes
			where Proveedores.IdProveedor=ProIng.IdProveedor and ProIng.IdIngrediente=Ingredientes.IdIngrediente and Ingredientes.stock<Ingredientes.puntoRep
			
		open PedidosDeCotizacion
			fetch next from PedidosDeCotizacion into @idIngrediente, @descripcion, @cantidad,@NombreProv
			
			declare @ProveedorAnterior varchar(50)
			select @ProveedorAnterior=@NombreProv
	
			--Crear tablaDinamica
			declare @nombreTabla nvarchar(550)
			select @nombreTabla= 'PC_'+@NombreProv
			select @nombreTabla=REPLACE(@nombreTabla,' ' ,'_')
			declare @crearTabla nvarchar(1000)
			select @crearTabla= 'create table '+@nombreTabla+' (IdIngrediente int, descripcion varchar(50), cantidad int) '
			
			--Eliminar tabla
			declare @eliminarTabla nvarchar(100)
			select @eliminarTabla= 'drop table '+@nombreTabla
			
			if exists (select * from sysobjects where xtype= 'U' and name = @nombreTabla)
				exec sp_executeSQL @eliminarTabla
	
			exec sp_executeSQL @crearTabla
			
			declare @IngresarDatos nvarchar(100)


			while(@@FETCH_STATUS=0)
			begin
				if(@ProveedorAnterior=@NombreProv)
				begin
					select @IngresarDatos= 'insert into ' + @nombreTabla + ' values ('+cast(@idIngrediente as varchar(50)) +','+''''+@descripcion+''''+','+ cast(@cantidad as varchar(50)) + ')'
					exec sp_executeSQL @IngresarDatos
					fetch next from PedidosDeCotizacion into @idIngrediente, @descripcion, @cantidad,@NombreProv
					select @ProveedorAnterior=@NombreProv
				end
			
				else
				begin
					
					--Crear tabla dinamica
					select @nombreTabla= 'PC_'+@NombreProv
					select @nombreTabla=REPLACE(@nombreTabla,' ' ,'_')
					select @IngresarDatos= 'insert into ' + @nombreTabla + ' values ('+cast(@idIngrediente as varchar(50)) +','+''''+@descripcion+''''+','+ cast(@cantidad as varchar(50)) + ')'
					
					if exists (select * from sysobjects where xtype= 'U' and name = @nombreTabla)
						exec sp_executeSQL @eliminarTabla

					exec sp_executeSQL @crearTabla

					select @ProveedorAnterior=@NombreProv

				end
			end
			
			
		close pedidosDeCotizacion
		Deallocate pedidosDeCotizacion
end

exec TablasPedidosDeCotizacion

select * from PC_San_Ignacio_

--8. Crear un procedimiento que reciba el idIngrediente, el idProveedor, la cantidad, el precio y la fecha de compra, verifique que el ingrediente y el proveedor existan y que no exista esa compra y la ingrese
select * from Ingredientes where IdIngrediente=116
select * from Proveedores
select * from compras

create or alter procedure ExisteCompra
	@idIngrediente int,
	@idProveedor int,
	@cantidad int,
	@precio float,
	@fechaCompra date
as
begin
	declare @idCompra int
	select @idCompra=(select max(idCompra) from compras)
	if exists(select * from Ingredientes where Ingredientes.IdIngrediente=@idIngrediente) and exists(select * from Proveedores where Proveedores.IdProveedor=@idProveedor) 
	and not exists(select * from compras where idIngrediente=@idIngrediente and idProveedor=@idProveedor and fechComp=@fechaCompra and precio=@precio)
	begin
		if (@idCompra is null)
                select @idCompra = 1
            else
                select @idCompra = @idCompra + 1
		insert into Compras values (@idCompra,@idIngrediente,@idProveedor,@cantidad,@precio,@fechaCompra)
	end
	else
	begin
		print 'El proveedor o el ingrediente son inexistentes'
	end
end

exec ExisteCompra 115,10,34,2231,'20000512'

--9.Crear un procedimiento que liste aquellas unidades que no tengan una equivalencia como mínimo. 
select * from FactorConversion
select * from Unidades


create or alter procedure SinEquivalenciaMinima
as
begin
	declare EqMin cursor for
		select * from Unidades
	open EqMin 
		
		declare @idUnidad int, @Descripcion varchar(50)
		fetch next from EqMin into @idUnidad, @descripcion
		
		declare @tablaAuxiliar table (idUnidad int ,descripcion varchar(50))
		
		while(@@FETCH_STATUS=0)
		begin
			if not exists(select * from FactorConversion where idUnidadOrigen=@idUnidad) and not exists(select * from FactorConversion where idUnidadDestino=@idUnidad)
			begin
				insert into @tablaAuxiliar values(@idUnidad,@descripcion)
			end 
			fetch next from EqMin into @idUnidad, @descripcion
		end
		select * from @tablaAuxiliar
	close EqMin
	Deallocate EqMin
end

exec SinEquivalenciaMinima

--10. Crear un procedimiento que reciba la descripción de un ingrediente y un entero correspondiente al mes y devuelva el importe total de las compras realizadas en el mes pasado como parámetro en una variable de output.
select * from Compras,Ingredientes where compras.idIngrediente=Ingredientes.IdIngrediente 
create or alter procedure ImporteTotalCompras 
	@descripcion varchar(50),
	@mes int,
	@resultado float output 
as
begin
	select @resultado=(select sum(precio)as importeTotal from Compras,Ingredientes where compras.idIngrediente=Ingredientes.IdIngrediente and descripcion=@descripcion and MONTH(compras.fechComp)=@mes)
end

declare @prueba int
exec ImporteTotalCompras 'Gancia',5,@prueba output 

print cast(@prueba as varchar(50))

select * from compras

--11.	Crear un procedimiento que realice el siguiente listado
/*						Ingrediente: Descripción_Ingrediente	
						Fecha	Nombre_Proveedor	Cantidad	Precio
						Fecha	Nombre_Proveedor	Cantidad	Precio							Ordenarlo por descripción del ingrediente. Si algún ingrediente no tiene compras, 
						Fecha	Nombre_Proveedor	Cantidad	Precio							colocar la leyenda “Sin Compras” y los totales en cero.
						Total						99999.99	99999.99

						Ingrediente: Descripción_Ingrediente
						Fecha	Nombre_Proveedor	Cantidad	Precio 
						Fecha	Nombre_Proveedor	Cantidad	Precio
						Fecha	Nombre_Proveedor	Cantidad	Precio
						Total						99999.99	99999.99
*/

create or alter procedure listadoIngredientes
as
begin
	declare ing cursor for 
		select IdIngrediente,descripcion from ingredientes order by descripcion
	declare prov cursor for 
		select fechComp,Proveedores.razsoc,cantidad,precio,descripcion from compras,Proveedores,Ingredientes where compras.idProveedor=Proveedores.IdProveedor and Ingredientes.IdIngrediente=Compras.idIngrediente order by descripcion
	declare tot cursor for 
		select totalCantidad,totalPrecio from Ingredientes
		inner join (select sum(cantidad) as totalCantidad,sum(precio) as totalPrecio ,idIngrediente from compras group by idIngrediente) g on g.idIngrediente=Ingredientes.IdIngrediente order by descripcion

	open ing
	open prov
	open tot
		declare @idIng int ,@descripcionIng varchar(50)
		declare @fecha date, @nombreProv varchar(50),@Cantidad int ,@precio float,@descripcionProv varchar(50)
		declare  @totCant int, @totPre float

		fetch next from ing into @idIng ,@descripcionIng
		fetch next from prov into @fecha, @nombreProv,@cantidad,@precio,@descripcionProv

		print 'Ingrediente: ' + @descripcionIng

		declare @descripcionAnt varchar(50)
		select @descripcionAnt= @descripcionIng
		while(@@FETCH_STATUS=0)
		begin
			if exists(select * from Compras where idIngrediente=@idIng)
			begin
				
				while(@descripcionAnt=@descripcionProv and @@FETCH_STATUS=0)
				begin
					print cast(@fecha as varchar(50))+ '  '+ @nombreProv+ '   '+Cast(@cantidad as varchar(50))+'              '+cast(@precio as varchar(50))
					fetch next from prov into @fecha, @nombreProv,@cantidad,@precio,@descripcionProv

				end

				fetch next from tot into @totCant , @totPre
				print 'Total:                       '+Cast(@totCant as varchar(50))+'              '+Cast(@totPre as varchar(50))
				
				fetch next from ing into @idIng ,@descripcionIng
				select @descripcionAnt= @descripcionIng
				print' '
				print 'Ingrediente: ' + @descripcionIng
			end
			else
			begin
				print '             Sin compras'
				print 'Total:                       0             0'
				fetch next from ing into @idIng ,@descripcionIng
				select @descripcionAnt= @descripcionIng
				print' '
				print 'Ingrediente: ' + @descripcionIng
				print cast(@idIng as varchar(50))
			end
		
		end
	close ing
	deallocate ing
	close prov
	deallocate prov
	close tot
	deallocate tot
end

exec listadoIngredientes

--12.	Crear un procedimiento que realice el siguiente listado.
/*						Rubro: Descripción_Rubro
							Nombre_Plato			Precio
							Nombre_Plato			Precio
							Nombre_Plato			Precio					Ordenarlo por rubro y dentro del rubro ordenado
						Total platos del Rubro	     99							 por precio de manera creciente
						Promedio Precios	       9999.99
						Rubro: Descripción_Rubro
							Nombre_Plato			Precio
							Nombre_Plato			Precio
							Nombre_Plato			Precio
						Total platos del Rubro	     99
						Promedio Precios	       9999.99
*/


create or alter procedure platosPorRubro
as 
begin
	declare RYP cursor for 
		select Rubros.descripcion as rubro,Platos.descripcion as plato,precio 
		from Platos,Rubros where rubros.IdRubro=Platos.IdRubro order by Rubros.descripcion,precio ASC
	declare Totales cursor for
		select PromedioPrecio,totalPlatos from rubros 
		inner join (select avg(precio) PromedioPrecio,count(descripcion) as totalPlatos,IdRubro from Platos group by IdRubro)g on g.IdRubro=Rubros.IdRubro  order by Rubros.descripcion
	open RYP 
	open Totales
		declare  @rubro varchar(50) ,@plato varchar(50),@precio float
		fetch next from RYP into @rubro ,@plato,@precio

		declare  @Promedio float,@platosTotales int
		fetch next from Totales into @Promedio,@platosTotales

		declare @RubroAnterior varchar(50)
		select @rubroAnterior=@rubro
		
		print 'Rubro: '+@rubro

		while (@@FETCH_STATUS=0)
		begin
			if(@RubroAnterior=@rubro)
			begin
					print '     '+@plato+'           '+cast(@precio as varchar(50))
					fetch next from RYP into @rubro ,@plato,@precio
			end
			else
			begin
				print 'Total platos del Rubro:'+ '           '+cast(@platosTotales as varchar(50))
				print 'Promedio precios:'+ '                 '+cast(@promedio as varchar(50))
				fetch next from Totales into @Promedio,@platosTotales
				print  'Rubro: '+@rubro
				select @rubroAnterior=@rubro
			end
		end 

	close RYP
	deallocate RYP
	close Totales
	deallocate Totales


end

exec platosPorRubro

--13.Crear un procedimiento que reciba el nombre de un plato y determine si el costo de este es menor a su precio de venta y cuál es el porcentaje de ganancia que el mismo tiene.
--   Devolver el porcentaje en una variable de output.
select * from platos 
select distinct  ItemsFactura.precio as costoPlato ,*from ItemsFactura,platos 

/*select g.descripcion,gasto as costo,platos.precio,((precio-gasto)*100/precio) as ganancia  from platos 
	inner join(select sum(cantida*precUso) as gasto, Platos.descripcion  
					from platos,PlaIng,Ingredientes
					where platos.IdPlato=PlaIng.IdPlato and PlaIng.IdIngrediente=Ingredientes.IdIngrediente group by (Platos.descripcion)) g on g.descripcion=Platos.descripcion
*/
select * from Platos
select * from ItemsFactura
create or alter procedure menorPrecio
	@nombrePlato varchar(50),
	@porcentaje float output
as 
begin 
	
	if exists (select  ItemsFactura.precio as costoPlato ,*
					from ItemsFactura,platos where platos.precio<ItemsFactura.precio and platos.IdPlato=ItemsFactura.IdPlato and Platos.descripcion=@nombrePlato  )
	begin
		print 'El costo del plato "'+@nombrePlato+'" es menor a su precio de venta'
		select @porcentaje=(select ( ((ItemsFactura.precio-platos.precio)*100)/ItemsFactura.precio) as porcentaje
								from ItemsFactura,platos where platos.precio<ItemsFactura.precio and platos.IdPlato=ItemsFactura.IdPlato and Platos.descripcion=@nombrePlato
							)
		return @porcentaje
	end
	else
	begin
		print 'El costo del plato "'+@nombrePlato+'" es mayor a su precio de venta'
		select @porcentaje=(select distinct ( ((ItemsFactura.precio-platos.precio)*100)/ItemsFactura.precio) as porcentaje
								from ItemsFactura,platos where platos.IdPlato=ItemsFactura.IdPlato and Platos.descripcion=@nombrePlato)
		return @porcentaje
	end
		
end

declare @prueba2 float

exec menorPrecio 'carpaccio' ,@prueba2 output 
print cast(@prueba2 as varchar(50))

--14.Crear un procedimiento que determine para un artículo el precio de compra promedio y determine cuál es el desvió de último precio con respecto al promedio obtenido.
--Informar el mismo mediante una variable de output.


create or alter procedure ej14
	@articulo varchar(50),
	@desvio float output
as
begin
	if exists(select * from Compras, Ingredientes where Ingredientes.IdIngrediente=Compras.idIngrediente and descripcion=@articulo)
	begin
		declare @promedioArticulo float =(select AVG(precio)as promedio
											from Compras,Ingredientes
											where compras.idIngrediente=Ingredientes.IdIngrediente and Ingredientes.descripcion=@articulo
											group by compras.idIngrediente)
		declare @PrecioUltimoArticulo float=(select precio as UltimoPrecio
												from compras,Ingredientes
												inner join(select max(idCompra) as ultimoComprado, idIngrediente from Compras group by idIngrediente) g on g.idIngrediente=Ingredientes.idIngrediente
												where Ingredientes.IdIngrediente=Compras.idIngrediente and g.ultimoComprado=Compras.idCompra and Ingredientes.descripcion=@articulo )

		select @desvio= @promedioArticulo-@precioUltimoArticulo

		return @desvio
	end
	else
	begin 
		print 'no existe ese articulo en la compra'
	end
end
select * from compras 

declare @prueba3 float

exec ej14 'Gancia' ,@prueba3 output 
print cast(@prueba3 as varchar(50))

--15.Crear un procedimiento que reciba la descripción de un proveedor un entero correspondiente al mes y devuelva el importe total de las compras realizadas en el mes pasado como parámetro en una variable de output.

create or alter procedure ej15
	@descripcion varchar(50),
	@mes int,
	@importeTotal float output 
as
begin
	if(@mes=1)
	begin
		select @mes= 12
	end
	else
	begin
		select @mes=@mes-1
	end
	if exists(select SUM(precio*cantidad) as importeTotal from compras,Proveedores where Proveedores.IdProveedor=Compras.idProveedor and razsoc=@descripcion and MONTH(compras.fechComp)=@mes group by razsoc)
	begin	
		select @importeTotal=(select SUM(precio*cantidad) as importeTotal from compras,Proveedores where Proveedores.IdProveedor=Compras.idProveedor and razsoc=@descripcion and MONTH(compras.fechComp)=@mes group by razsoc)

		return @importeTotal
	end
	else
	begin
		print 'No hubo compras en el mes: '+cast(@mes as varchar(50))
	end
end

declare @prueba4 float

exec ej15 'La Rural S.A.',7 ,@prueba4 output 
print cast(@prueba4 as varchar(50))

/************************************************************************************************************************************************/
/*
									  _____                          _                              
									 |  ___|  _   _   _ __     ___  (_)   ___    _ __     ___   ___ 
									 | |_    | | | | | '_ \   / __| | |  / _ \  | '_ \   / _ \ / __|
									 |  _|   | |_| | | | | | | (__  | | | (_) | | | | | |  __/ \__ \
									 |_|      \__,_| |_| |_|  \___| |_|  \___/  |_| |_|  \___| |___/


*/.
/************************************************************************************************************************************************/                                                                

-- 1.Crear una función que reciba como parámetro una cadena de 255 caracteres máximo y devuelva la misma cadena pero en minúsculas y capitalizada la primera letra de cada palabra.

create or alter function ProperCase(
    @Text varchar(300)    )

returns varchar(300)
as
begin
     declare @Reset bit;
     declare @Ret varchar(300);
     declare @i int;
     declare @c char(1);

  if @Text is null
    return null;

  select @Reset = 1, @i = 1, @Ret = '';

  while (@i <= len(@Text))                    --recorro toda la cadena
     select @c = substring(@Text, @i, 1),    -- tomo 1 solo caracter en pose i
     @Ret = @Ret + case                        --Ret acumula lo que retorno +  Cuando Reset=1  pongo en mayuscula el caracter y lo agrega
        when @Reset = 1 
            then UPPER(@c) 
            else LOWER(@c) 
        end,
    @Reset = case when @c like '[a-zA-Z]'
        then 0 
        else 1
        end,

    @i = @i + 1

  return @Ret
end

select dbo.ProperCase('hola soy')




--2.Escribir una función que reciba como parámetro una mesa y una fecha y devuelva el total facturado por esa mesa en esa fecha.
create or alter function ej2(
	@mesa varchar(50),
	@fecha date
)
returns float
as 
begin
	declare @totalFacturado float=(select SUM(f.total) from mesas m,facturas f where m.IdMesa=f.IdMesa and m.Descripcion=@mesa and f.fechaApertura=@fecha group by m.IdMesa)
	return @totalFacturado
end
declare @prueba2 varchar(50)
select @prueba2=(select dbo.ej2('Mesa Redonda','20090115'))
print @prueba2

--3.Escribir una función que reciba como parámetro un ingrediente y un rango de fechas y devuelva las compras realizadas de ese ingrediente durante ese lapso de tiempo.
create or alter function ej3(
	@ingrediente varchar(50),
	@desde date,
	@hasta date
)
returns table
as
return select idCompra,i.IdIngrediente,c.idProveedor,c.precio,c.fechComp from compras c , ingredientes i where c.idIngrediente=i.IdIngrediente and i.descripcion=@ingrediente and c.fechComp>=@desde and c.fechComp<=@hasta

SELECT * FROM DBO.ej3('Gancia','20000101','20200101')

--4.Escribir una función que reciba un rango de fechas y devuelva una tabla con los registros de log entre esas fechas, ordenado por usuario, fecha e ingrediente.
create or alter function ej4(
	@desde date,
	@hasta date
)
returns @tabla table (idLogAjuste int ,idIngrediente int, stockAnterior int, stockNuevo int ,fechaMovimiento date,observaciones varchar(50),usuario varchar(50))
as
begin
	insert into @tabla select l.idLogAjuste,l.idIngrediente,l.stockAnterior,l.stockNuevo,l.fechaMovimiento,l.observaciones,l.usuario 
						from LogAjustes l,Ingredientes 
						where fechaMovimiento>= @desde and fechaMovimiento<=@hasta and Ingredientes.IdIngrediente=l.idLogAjuste order by usuario,fechaMovimiento,Ingredientes.descripcion
	return  
end


select * from dbo.ej4('19990101','20210806')



--5.Escribir una función que reciba un mozo y un entero correspondiente a un mes, y devuelva el total facturado por ese mozo en ese mes.
create or alter function ej5(
	@nombreMozo varchar(50),
	@ApellidoMozo varchar(50),
	@mes int
)
returns float
as 
begin
	declare @aux float =(select SUM(f.total) from Facturas f,mozos m where m.IdMozo=f.IdMozo and m.nombre=@nombreMozo and m.apellido=@apellidoMozo and MONTH(f.fechaCierre)=@mes group by m.IdMozo)
	return @aux
end
declare @prueba5 float
select @prueba5=(select dbo.ej5('Pablo','Andrade',1))
print cast(@prueba5 as varchar(50))


--6.Escribir una función que reciba un mozo, un entero correspondiente a un mes y un decimal que corresponde al porcentaje de comisión del mozo y devuelva el importe que
--le corresponde en concepto de comisión a ese mozo en ese mes.
create or alter function ej6(
	@nombreMozo varchar(50),
	@ApellidoMozo varchar(50),
	@mes int,
	@porcentaje float
)
returns float
as 
begin
	declare @aux float =(select SUM(f.total) from Facturas f,mozos m where m.IdMozo=f.IdMozo and m.nombre=@nombreMozo and m.apellido=@apellidoMozo and MONTH(f.fechaCierre)=@mes group by m.IdMozo)
	select @aux= @aux*@porcentaje/100
	return @aux
end
declare @prueba6 float
select @prueba6=(select dbo.ej6('Pablo','Andrade',1,100))
print cast(@prueba6 as varchar(50))

--7.Escribir una función que reciba un motivo de rechazo y un proveedor y devuelva una tabla conteniendo las compras rechazadas a ese proveedor por ese motivo.
create or alter function ej7(
	@motivoRechazo varchar(50),
	@proveedor varchar(50)
)
returns table
return select c.idCompra,c.idIngrediente,c.idProveedor,c.cantidad ,c.precio,c.fechComp 
		from MotivosRechazo m,Recepcion r,Compras c,Proveedores p 
		where m.idMotivo=r.idMotivo and r.idProveedor=p.IdProveedor and r.fechaRecepcion=c.fechComp and p.razsoc=@proveedor and m.descripcion=@motivoRechazo

select * from dbo.ej7('Mal Producto','La Rural S.A.')


--8.Escribir una función que reciba como parámetro un rubro y devuelva el costo de cada plato de ese rubro y la fecha del ingrediente más viejo utilizado en su confección
--(considere la fecha de compra más nueva para cada ingrediente).
create or alter function ej8(
		@rubro varchar(50)
)
returns table
return select p.precio as costo,i.descripcion
		from Rubros r ,platos p ,plaing pl ,Ingredientes i
		inner join(select MAX(c.fechComp) as masViejo , i.idIngrediente from Compras c,Ingredientes i where c.idIngrediente=i.IdIngrediente group by i.IdIngrediente) g on g.IdIngrediente=i.IdIngrediente
		where r.IdRubro=p.IdRubro and p.IdPlato=pl.IdPlato and pl.IdIngrediente=i.IdIngrediente

select * from dbo.ej8('Entradas')


--9.Escribir una función que devuelva una tabla con todos los ingredientes que no fueron pedidos más de una vez en los últimos 6 meses ordenado por fecha de pedido descendente.
--Pedidos de venta
create or alter function ej9(
	
)
returns @aux table(idIngrediente int ,Descripcion varchar(100))
as
begin
	insert into @aux 
	select Ingredientes.IdIngrediente,Ingredientes.descripcion
		from Ingredientes
		inner join(select COUNT(i.descripcion) pedidos,i.IdIngrediente
					from Ingredientes i ,PlaIng pl,ItemsFactura itf,facturas f 
					where i.IdIngrediente=pl.IdIngrediente and pl.IdPlato=itf.IdPlato and itf.NroFactura=f.NroFactura and f.fechaCierre>=DATEADD(month,-6,f.fechaCierre) 
					group by i.IdIngrediente) z on z.IdIngrediente=Ingredientes.IdIngrediente
		inner join PlaIng on Ingredientes.IdIngrediente=PlaIng.IdIngrediente
		inner join ItemsFactura on ItemsFactura.IdPlato=PlaIng.IdPlato
		inner join Facturas on Facturas.NroFactura=ItemsFactura.NroFactura
		where z.pedidos=1 and Ingredientes.IdIngrediente=PlaIng.IdIngrediente and ItemsFactura.IdPlato=PlaIng.IdPlato and Facturas.NroFactura=ItemsFactura.NroFactura
		order by Facturas.fechaCierre asc  

		return
end

drop function ej9

select * from dbo.ej9()

--Pedidos de compra
create or alter function F9 ()
returns @resultado table(idIng int,descrip varchar(100) ,cant int,fechComp date)
as
begin
    insert into @resultado 
                        select compras.idingrediente,ingredientes.descripcion,cantidad,fechComp 
						from compras,Ingredientes 
						where compras.cantidad<=1 and  compras.fechComp<=DATEADD(MM, -6, GETDATE()) and compras.idIngrediente=ingredientes.IdIngrediente
                        order by compras.fechComp desc
    return
end

select * from dbo.F9()

--10.Escribir una función que reciba un valor entero y devuelva una tabla con todos los proveedores a los que se le realizaron más compras en el último mes que el valor pasado como parámetro ordenados por nombre del proveedor.
create or alter function ej10(
	@valor int
)
returns table
return
	select p.IdProveedor,p.razsoc, d.cantCompras 
		from proveedores p
		inner join (select COUNT(idCompra) as cantCompras,proveedores.idProveedor
						from Compras,Proveedores 
						where MONTH(fechComp)=(select MAX(MONTH(fechComp)) from compras) and Compras.idProveedor=Proveedores.IdProveedor
						group by proveedores.IdProveedor) d on d.IdProveedor=p.IdProveedor
		where d.cantCompras>@valor

select * from dbo.ej10(1)

--11.Escribir una función que reciba un rubro y un valor decimal correspondiente a un porcentaje de aumento y actualice los precios de los platos en ese porcentaje para todos los platos de ese rubro.
create or alter function ej11(
	@rubro as varchar(50),
	@porcentaje int
)
returns varchar(50)
as
begin
	declare @idRubro int=(select r.IdRubro from Rubros r,platos p where R.IdRubro=p.IdRubro and R.descripcion=@rubro)
	
		declare @valorPlato float
		declare valor cursor for
			select precio from Rubros r,platos p where R.IdRubro=p.IdRubro and R.descripcion=@rubro

		open valor
			fetch next from valor into @valorPlato

			while(@@FETCH_STATUS=0)
			begin
				select @valorPlato=@valorPlato*(1+(@porcentaje/100))
				update Platos
				set precio=@valorPlato
				where IdRubro=@idRubro

				fetch next from valor into @valorPlato
			end

		close valor
		deallocate valor

	return 
end

select * from Platos
--12.Escribir una función que devuelva la valorización del stock al momento actual.
create or alter function ej12(
)

--13.Escribir una función que reciba un ingrediente y devuelva una tabla que contenga el número de compra, el nombre del proveedor, la cantidad pedida, la unidad de medida, el precio de compra y la fecha de compra.
create or alter function ej13(
	@ingrediente varchar(50)
)
returns table
return
select c.idCompra,p.razsoc,c.cantidad,u.descripcion as unidadDeMedida,c.precio,c.fechComp 
	from Compras c,Ingredientes i,Proveedores p,Unidades u 
	where i.descripcion=@ingrediente and c.idIngrediente=i.IdIngrediente and p.IdProveedor=c.idProveedor and u.IdUnidad=i.cUniCompra

select * from dbo.ej13('Gancia')
--14.Escribir una función que devuelva los ingredientes a los que no se les realiza una compra desde hace 12 meses pero de los que aún se necesitan de acuerdo a lo que figura en la carta. Indicar el identificador del
--ingrediente, la descripción, la fecha de última compra, la cantidad comprada, el nombre del proveedor de esa compra y si posee existencia.
create or alter function f14()
	returns @tabla table (idIngrediente int, descripcion varchar(50),cantidad int, proveedor varchar(50), fecha date)
as
begin
	insert into @tabla select i.IdIngrediente,i.descripcion,c.cantidad,prov.razsoc,g.UltimaCompra from  Compras c,PlaIng p,Platos pla,Proveedores prov,ingredientes i
	inner join (select max(fechComp) UltimaCompra,idIngrediente from Compras group by idIngrediente) g on g.idIngrediente=i.IdIngrediente
	where i.IdIngrediente=p.IdIngrediente and c.idIngrediente=i.IdIngrediente and pla.IdPlato=p.IdPlato and prov.IdProveedor=c.idProveedor
			and i.IdIngrediente in (select idIngrediente from Compras where fechComp>=DATEADD(mm,-12,GETDATE())) 
			and i.puntoRep<= stock and g.idIngrediente=c.idIngrediente and g.UltimaCompra=c.fechComp
	return 

end

select * from dbo.f14()

--15.Escribir una función que coloque en cero el stock de un producto pasado como parámetro y genere los movimientos correspondientes en la tabla ajustes.
create or alter function ej15(
	@producto varchar(50)
)
returns varchar(50)
begin
	declare @stockAnterior int = (select stock from Ingredientes where descripcion=@producto)

	declare @id int=(select max(idLogAjuste) from LogAjustes)+1

	declare @idIng int= (select idIngrediente from Ingredientes where descripcion=@producto)

	update Ingredientes
	set stock=0
	where descripcion=@producto

	declare @fecha date=getdate()
	insert into LogAjustes values( @id , @iding , @StockAnterior , 0 , @fecha  , 'sin observaciones' , 'nico' )

	return 'Realizado con exito'
end
select * from LogAjustes
select * from Ingredientes


create or alter function eje15(
	@producto varchar(50)
)
returns varchar(50)
begin
	declare @stockAnterior int = (select stock from Ingredientes where descripcion=@producto)

	declare @id int=(select max(idLogAjuste) from LogAjustes)+1

	declare @idIng int= (select idIngrediente from Ingredientes where descripcion=@producto)

	declare @sentencia varchar(1000)= '
	update Ingredientes
	set stock=0
	where descripcion=@producto'
	
	exec sp_executeSQL @sentencia
	
	declare @observaciones varchar(50)='sin observaciones'
	declare @Usuario varchar(50)= 'nico'
	declare @sentencia2 varchar(1000)= '
	insert into LogAjustes values( @id , @iding , @StockAnterior , 0 , GETDATE()  ,@observaciones , @usuario )'

	exec sp_executeSQL @sentencia

	return 'Realizado con exito'
end

select dbo.eje15()

/************************************************************************************************************************************************/
/*
						  _____                                                      _                              
						 |_   _|  _ __    __ _   _ __    ___    __ _    ___    ___  (_)   ___    _ __     ___   ___ 
						   | |   | '__|  / _` | | '_ \  / __|  / _` |  / __|  / __| | |  / _ \  | '_ \   / _ \ / __|
						   | |   | |    | (_| | | | | | \__ \ | (_| | | (__  | (__  | | | (_) | | | | | |  __/ \__ \
						   |_|   |_|     \__,_| |_| |_| |___/  \__,_|  \___|  \___| |_|  \___/  |_| |_|  \___| |___/
                                                                                            
*/.
/************************************************************************************************************************************************/        
--1.Escribir un procedimiento que reciba dos valores enteros, correspondientes a un porcentaje y un valor máximo, mediante el uso de transacciones realizar las siguientes acciones cuando correspondan.
--A.Validar que el precio más caro en la tabla de platos sea menor al valor máximo ingresado. Caso contrario terminar el procedimiento informando sobre la situación.
create or alter procedure t1a
	@porcentaje int,
	@valorMax int
as
begin
	begin tran
		declare @esNull varchar(50)
		declare datos cursor for
			select max(precio) as precio from Platos where precio>=@valorMax
		open datos
		fetch next from datos into @esNull
		
		declare @control int=0
		if (@esNull!='NULL')
		begin
			select @control=@control +@@error 
			print 'El precio ingresado es mas chico que al mayor de los precios'
			if(@control != 0 )    --Si control es igual a 0 significa que ninguna de mis operaciones dio error 
				commit
			else
				Rollback
		end
		else 
		begin
			select @control=@control +@@error 
			print 'El precio ingresado es mas grande que al mayor de los precios'
			if(@control != 0 )    --Si control es igual a 0 significa que ninguna de mis operaciones dio error 
				commit
			else
				Rollback
		end

		close datos
		deallocate datos
end


--B.Aplicar el porcentaje de aumento ingresado sobre el precio de los platos.
create or alter procedure t1b
	@porcentaje float,
	@valorMax int
as
begin
	begin tran
		declare @control int=0

		declare @valorPlato float,@idPlato int
		declare @calculo float
		declare valor cursor for
			select precio,IdPlato from platos
		open valor
			
			fetch next from valor into @valorPlato, @idPlato

			while(@@FETCH_STATUS = 0)
			begin
				select @calculo=@valorPlato*(1+(@porcentaje/100))
				print cast(@calculo as varchar(50))
				update platos
				set platos.precio=@calculo
				where IdPlato=@idPlato

				select  @control=@control+@@ERROR
				fetch next from valor into @valorPlato, @idPlato
				select  @control=@control+@@ERROR

			end
		
		close valor
		deallocate valor
		print cast(@control as varchar(50))
		if(@control = 0 )    --Si control es igual a 0 significa que ninguna de mis operaciones dio error 
			commit
		else
			Rollback
end

exec t1b 10,1

--C.Si algún precio después del aumento es mayor al mayor valor ingresado, deshacer las modificaciones sobre los precios.
create or alter procedure t1c
	@porcentaje float,
	@valorMax int
as
begin
	begin tran
		declare @control int=0

		declare @valorPlato float,@idPlato int
		declare @calculo float
		declare valor cursor for
			select precio,IdPlato from platos

		open valor
			
			fetch next from valor into @valorPlato, @idPlato

			while(@@FETCH_STATUS = 0)
			begin
				select @calculo=@valorPlato*(1+(@porcentaje/100))
				update platos
				set platos.precio=@calculo
				where IdPlato=@idPlato

				select  @control=@control+@@ERROR
				fetch next from valor into @valorPlato, @idPlato
				select  @control=@control+@@ERROR

			end
		
			declare @aux int=(select max(precio) from platos)

			print cast(@aux as varchar(50))

			if(@aux>@valorMax)
			begin
				select @control=@control+1
			end

		close valor
		deallocate valor
		if(@control = 0 )    --Si control es igual a 0 significa que ninguna de mis operaciones dio error 
			commit
		else
			Rollback
	;
end


select * from Platos
exec t1c 1,1
--D.Disminuir en 2 unidades el porcentaje ingresado y volver a realizar los pasos b y c.
create or alter procedure t1d
	@porcentaje float,
	@valorMax int
as
begin
	begin tran
		declare @control int=0

		declare @valorPlato float,@idPlato int
		declare @calculo float
		declare valor cursor for
			select precio,IdPlato from platos

		select @porcentaje=@porcentaje-2
		open valor
			
			fetch next from valor into @valorPlato, @idPlato

			while(@@FETCH_STATUS = 0)
			begin
				select @calculo=@valorPlato*(1+(@porcentaje/100))
				update platos
				set platos.precio=@calculo
				where IdPlato=@idPlato

				select  @control=@control+@@ERROR
				fetch next from valor into @valorPlato, @idPlato
				select  @control=@control+@@ERROR

			end
		
			declare @aux int=(select max(precio) from platos)

			print cast(@aux as varchar(50))

			if(@aux>@valorMax)
			begin
				select @control=@control+1
			end

		close valor
		deallocate valor
		if(@control = 0 )    --Si control es igual a 0 significa que ninguna de mis operaciones dio error 
			commit
		else
			Rollback
	;
end
exec t1d 1,1
--E.Continuar con el decremento hasta que después de la actualización no exista ningún precio que sea mayor o igual al máximo ingresado.
create or alter  procedure trans2
	@porcentaje decimal,@maximo int
as
begin
	declare @masCaro int=(select max(precio) from Platos2)
	if (@masCaro<@maximo)
	begin
	begin transaction t1
		declare @idPLatoActual int, @precioPlato decimal, @nuevoTotal decimal
		declare misPlatos cursor for select idPlato,precio from Platos2
		--primer aplicacion del porcentaje

			open misPlatos
			fetch  next from misPlatos into  @idPlatoActual,@precioPlato
			while(@@FETCH_STATUS=0)
				begin	
					select @nuevoTotal=@precioPlato+(@precioPlato*@porcentaje)
					--update platos set precio=@nuevoTotal where platos.IdPlato=@idPLatoActual
					print( cast (@idPlatoActual as varchar) +'  viejo: '+cast(@precioPlato as varchar)+ '   nuevo '+cast( @nuevoTotal as varchar))
					fetch  next from misPlatos into  @idPlatoActual,@precioPlato
				end
			close misPlatos
			deallocate misPlatos

		--tomo el mayor precio y le aplico porcentaje
		declare @precioMaxConPorc decimal=( select precio*@porcentaje+precio from Platos2 where platos2.precio= (select max(precio) from platos2)    )
		print cast (@precioMaxConPorc as varchar) 

		if(@precioMaxConPorc>=@maximo)
			rollback transaction t1
		else
			commit transaction  t1
	

		--Caso en que no logra ser menor al maximo: incia while 
		while  (  @precioMaxConPorc >=@maximo )
			begin
			print 'maximo '+cast(@precioMaxConPorc as varchar)
			begin transaction t2
					declare misPlatos cursor for select idPlato,precio from platos2
					select @porcentaje=@porcentaje-2
					open misPlatos
						fetch  next from misPlatos into  @idPlatoActual,@precioPlato
						while(@@FETCH_STATUS=0)
							begin	
								select @nuevoTotal=@precioPlato+(@precioPlato*@porcentaje)
								
								--update platos set precio=@nuevoTotal where platos.IdPlato=@idPLatoActual
								print( cast (@idPlatoActual as varchar) +'  viejo: '+cast(@precioPlato as varchar)+ '   nuevo '+cast( @nuevoTotal as varchar))
								fetch  next from misPlatos into  @idPlatoActual,@precioPlato
							end
					 close misPlatos
					 deallocate misPlatos

			select @precioMaxConPorc =( select precio*@porcentaje+precio from platos2 where platos2.precio= (select max(precio) from Platos2)    )
		  --si con ese porcentaje no logra el objetivo, borro lo hecho y vuelve al ciclo while
		    if(@precioMaxConPorc>=@maximo)
					commit transaction t2
			else	rollback transaction t2
		
		end
	end
	else
		print 'No es menor'

--2.Hacer un procedimiento que utilizando transacciones recorra la lista de ingredientes y realice el ajuste necesario en el precio de uso para que el mismo sea un 40% 
--más alto que el precio de compra para cada uno de los artículos. 
create or alter procedure t2
as
begin
	begin tran
		declare @control int=0

		declare @idIngrediente int,@precComp decimal(6,2),@precUso decimal(8,6)

		declare c cursor for
			select IdIngrediente,precComp,precUso from Ingredientes

		declare @aux decimal(20,6)
		open c
			fetch next from c into @idIngrediente ,@precComp ,@precUso 
			select  @control=@control+@@ERROR
			while(@@FETCH_STATUS=0)
			begin
				select @aux=0
				select @aux=@precComp*1.4

				update Ingredientes
				set precUso=@aux
				where IdIngrediente=@idIngrediente
				select  @control=@control+@@ERROR
				fetch next from c into @idIngrediente ,@precComp ,@precUso 
				select  @control=@control+@@ERROR
				
				
			end
		close c
		deallocate c
		select  @control=@control+@@ERROR
		print cast(@control as varchar(50))
		if(@control = 0 )    --Si control es igual a 0 significa que ninguna de mis operaciones dio error 
			commit
		else
			Rollback
	;
end
select * from Ingredientes
select * from Unidades
rollback
exec t2
--3.Analice los procedimientos almacenados que realizó y determine si los mismos serían más eficientes o confiables si se implementasen transacciones. Justifique la elección por cada uno de los procedimientos seleccionados. 
	
