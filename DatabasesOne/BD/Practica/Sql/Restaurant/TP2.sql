--1. Listado de nombre y n�mero de tel�fono de los mozos.
select nombre,apellido,codArea,celular,central,telefonoMozo.numero 
	from mozos inner join telefonoMozo on Mozos.IdMozo=TelefonoMozo.IdMozo


--2. Listado de Proveedores sin el c�digo postal.
select idproveedor,proveedores.idCodPos 
	from proveedores 
		where Proveedores.IdCodPos = (null)


--3. Determine la cantidad de Platos
select COUNT(Platos.IdPlato) as cantidadDePlatos from Platos

--4. Determine la cantidad total de facturas
select COUNT(Facturas.NroFactura) as CantidadDeFacturas from Facturas


--5. Determine la cantidad de ingredientes con una letra f en la descripci�n. 
select *
	from ingredientes
	where descripcion like '%f%'

--6. Determine cuantas mesas hay en el sector 3
select * 
	from Mesas 
	where IdSector=3
 

--7. Determine la menor cantidad en stock de los ingredientes.
Select * 
	from Ingredientes 
	where stock <= all(select stock 
							from ingredientes)


--8. El promedio de platos en las facturas 
select AVG(cantida*1.0) as nroPromedioDePlatos from (
	select NroFactura, SUM(cantida) as cantida
		from itemsFactura
		group by NroFactura
		) c1

Select (sum(cantida)*1.0/count(distinct NroFactura)*1.0) as promedioPlatos
	from itemsFactura


--9. Determine la cantidad total de unidades en stock de todos los ingredientes.
select stock*cUniCompra as cantidadStock , descripcion
	from Ingredientes

--10.Determine La cantidad de �tems de la factura 6.
select NroFactura, SUM(cantida) as cantida
	from itemsFactura
	where NroFactura = 6
	group by NroFactura

--11.Mostrar el importe de cada factura.
select NroFactura,total as importe 
	from Facturas


--12.El mayor monto de una factura.
select NroFactura,total 
	from Facturas
	where total >= all(select total 
							from facturas)

--13.Liste los ingredientes y su cantidad en existencia, si la misma es menor o igual al punto de reposici�n.
select * 
	from ingredientes where stock<puntoRep

--14.Liste los ingredientes y su precio en los casos en que la cantidad a comprar sea 1. 
select IdIngrediente ,descripcion ,precUso 
	from Ingredientes
	where cantComp=1

--15.Liste los tel�fonos de proveedores con c�digo de �rea igual a 1
select * 
	from TelefonoProveedor
	where codArea = 1

--16.Muestre el c�digo postal de la localidad de Tigre.
select * from Localidades
	where localida = 'Tigre'


--17.Muestre los n�meros de factura, su total y la fecha de emisi�n, de las facturas del mes de enero.
select NroFactura,total, fechaApertura from Facturas
	where MONTH(fechaApertura)=1

	
--18.Liste la cantidad de ventas por mozo.
select COUNT(Facturas.NroFactura) cantidadDeVentas , Facturas.IdMozo
	from Facturas inner join Mozos on Mozos.IdMozo=Facturas.IdMozo
	group by Facturas.IdMozo


--19.Liste monto vendido por mozo.
select COUNT(Facturas.NroFactura) cantidadDeVentas , Facturas.IdMozo, SUM(total) as MontoVendido
	from Facturas inner join Mozos on Mozos.IdMozo=Facturas.IdMozo
	group by Facturas.IdMozo

select sum(total) monto, idmozo 
	from facturas 
	group by idmozo

--20.Liste el total de venta por mesa.
select COUNT(Facturas.NroFactura) cantidadDeVentas , Facturas.IdMesa, SUM(total) as MontoVendido
	from Facturas inner join Mesas on Mesas.IdMesa=Facturas.IdMesa
	group by Facturas.IdMesa

select sum(total) monto, IdMesa 
	from facturas 
	group by IdMesa

--21.Total de ventas por mozo y mesa.
select idmozo, idmesa,sum(total) as totalVenta
	from facturas
	group by idmozo,idmesa
	Order by idmozo, IdMesa

--22.Determine la cantidad vendida de cada plato.
select COUNT(cantida) as cantidadVendida,IdPlato 
	from ItemsFactura
	group by idPlato

--23.Determine la cantidad total utilizada de cada ingrediente para hacer un plato  de cada uno de los platos.
select i.descripcion, sum(cantida) as Total
	from PlaIng p inner join Ingredientes i on p.IdIngrediente = i.IdIngrediente
	group by p.IdIngrediente, i.descripcion

--24.Determine la cantidad de platos por cada rubro.
select platos.IdRubro , COUNT(idPlato) as cantidadDePlatos
	from platos inner join Rubros on Rubros.IdRubro=Platos.IdRubro
	group by Platos.IdRubro


--25.Determine la cantidad de �tems por factura
select NroFactura,COUNT(idPlato) as cantidadDeItems
	from ItemsFactura
	group by NroFactura


--26.Determine cuantas veces se pidio cada plato.
select  idPlato , count (cantida) as pedidos
	from ItemsFactura
	group by IdPlato	


--27.Liste las facturas y la cantidad de �tems, donde el total calculado sea mayor que 60
select Facturas.NroFactura, cantidadDeItems,total
	from Facturas 
	inner join (select NroFactura,COUNT(idPlato) as cantidadDeItems
								from ItemsFactura
								group by NroFactura) c1 on Facturas.NroFactura=c1.NroFactura
	where Facturas.total>60


--28.Liste los proveedores que proveen m�s de 10 art�culos.
select COUNT(idIngrediente) as Articulos, IdProveedor
	from  ProIng
	group by idProveedor
	having  COUNT(idIngrediente)>10
	

--29.Liste los platos que hayan sido pedidos m�s de una vez.
select COUNT(cantida) ,idplato
	from ItemsFactura
	group by IdPlato
	having COUNT(cantida) > 1

--30.Listado de nombre y apellido de los mozos, ordenado alfab�ticamente en orden descendente. 
select apellido ,nombre
	from Mozos
	order by  apellido DESC,nombre 

--31.Liste los proveedores y su direcci�n en orden descendente por direcci�n 
select * 
	from Proveedores
	order by calle DESC

--32.Liste en n�mero de factura y la cantidad de �tems, ordenado por cantidad de �tems descendente. 
select NroFactura,COUNT(Cantida) as cantida
	from ItemsFactura
	group by NroFactura
	order by Cantida DESC

--33.Liste las facturas con sus totales calculados ordenado en forma descendente 
--por su importe en los casos en que la factura supere los 75 pesos.
select * 
	from Facturas
	where total>75
	order by total DESC
	

--34.Liste los ingredientes y la cantidad utilizada en la preparaci�n de una unidad 
--de todos los platos, cuando esa cantidad sea mayor a cien unidades.
select descripcion,cantida 
	from PlaIng inner join Ingredientes on PlaIng.IdIngrediente=Ingredientes.IdIngrediente
	where cantida>100

--35.Liste los proveedores que posean m�s de 2 tel�fonos en orden ascendente.
select * 
	from Proveedores inner join (select COUNT(idTelefono) as cantTel, idProveedor
									from TelefonoProveedor
									group by IdProveedor
									having COUNT(IdTelefono) > 2 ) c1 on c1.IdProveedor=Proveedores.IdProveedor

	
--36.Liste los sectores con m�s de 2 mesas ordenado ascendentemente por el id 
--del sector.
select c1.IdSector, c1.cantMesa,descripcion
	from Sectores inner join (select COUNT(IdMesa) cantMesa ,IdSector 
								from mesas
								group by IdSector 
								having COUNT(idMesa) > 2) c1 on Sectores.IdSector=c1.IdSector
	order by c1.IdSector ASC
	


--37.Liste los rubros y la cantidad de platos de cada rubro en los que los que la 
--cantidad de platos sea mayor que 4, en orden descendente en cantidad de 
--platos.
select * 
	from Rubros inner join (select COUNT(idplato) cantPlatos,IdRubro 
								from Platos
								group by IdRubro
								having COUNT(idPlato) > 4) c1 on c1.IdRubro=Rubros.IdRubro
	order by c1.cantPlatos DESC
	

--38.Liste de los mozos y su facturaci�n acumulada para cada mozo, excluyendo 
--al mozo de id 1, cuya en las cuales la mencionada sumatoria haya superado 
--el monto de 50 pesos.
select c1.idMozo  ,nombre,apellido, FactAcum
	from mozos inner join (select SUM(total) as FactAcum,IdMozo 
							from Facturas
							group by IdMozo
							having SUM(total) > 50 and IdMozo !=1) c1 on c1.IdMozo=Mozos.IdMozo


--39.Liste de facturaci�n acumulada por mesa exceptuando la mesa 'Rinconera' y 
--desechando las facturas menores o iguales a 70 pesos.
select NroFactura,c1.IdMesa,Descripcion,total
	from Facturas inner join (select IdMesa, Descripcion 
								from Mesas
								where Descripcion != 'Rinconera') c1 on c1.IdMesa=Facturas.IdMesa
	where total>70
	


--40.Liste las mesas y los sectores a los que pertenecen
select IdMesa,Mesas.descripcion descripcionMesa,Mesas.IdSector , Sectores.descripcion descripcionSector
	from Mesas inner join Sectores on Mesas.IdSector=Sectores.IdSector


--41.Liste el plato, el rubro y el precio. 
select Platos.descripcion NombreDePlato, Rubros.descripcion Rubro, precio 
	from Platos inner join Rubros on Rubros.IdRubro=Platos.IdRubro


--42.Liste las facturas con los nombres de los mozos y el nombre de las mesas y 
--el nombre del sector.
select NroFactura,f.IdMesa, me.Descripcion mesa , cantidadPersonas,f.IdMozo ,mo.nombre nombreMozo ,mo.apellido ApellidoMozo , total, fechaApertura,horaApertuta,fechaCierre,horaCierre
	from Facturas f
	inner join Mozos mo on f.IdMozo=mo.IdMozo
	inner join Mesas me on me.IdMesa=f.IdMesa
	inner join Sectores s on me.IdSector=s.IdSector


-- 43.Liste de n�mero de facturas con su total mayor que 50 indique el nombre 
--del sector, la mesa y el mozo.
select NroFactura, me.Descripcion mesa ,mo.nombre nombreMozo ,mo.apellido ApellidoMozo , total
	from Facturas f
	inner join Mozos mo on f.IdMozo=mo.IdMozo
	inner join Mesas me on me.IdMesa=f.IdMesa
	inner join Sectores s on me.IdSector=s.IdSector
	where total > 50

--44.Liste las facturas con el nombre del mozo cuyo nombre contiene una letra o 
--en el nombre
select NroFactura, me.Descripcion mesa ,mo.nombre nombreMozo ,mo.apellido ApellidoMozo , total
	from Facturas f
	inner join Mozos mo on f.IdMozo=mo.IdMozo
	inner join Mesas me on me.IdMesa=f.IdMesa
	inner join Sectores s on me.IdSector=s.IdSector
	where nombre like '%o%'

--45.Liste los mozos que vivan en la localidad de Castelar
select IdMozo,nombre,apellido,calle,numero,piso,departamento,c1.IdCodpos,localida
	from Mozos inner join (select * 
							from Localidades
							where localida='Castelar') c1 on Mozos.IdCodPos=c1.IdCodpos


--46.Liste los proveedores y su direcci�n de las localidades de Mart�nez y Moreno.
select IdProveedor, razsoc,calle,numero,piso,departamento,c1.IdCodpos,c1.localida
	from proveedores inner join (select * 
							from Localidades
							where localida='Martinez' or localida='Moreno') c1 on Proveedores.IdCodPos=c1.IdCodpos

--47.Liste los productos provistos por La Rural S.A.
select i.IdIngrediente, descripcion
	from Ingredientes i inner join (select IdIngrediente
										from ProIng inner join (select *
																	from Proveedores
																	where razsoc='La Rural S.A.') c1 on c1.IdProveedor=ProIng.IdProveedor
																	group by IdIngrediente) c2 on c2.IdIngrediente=i.IdIngrediente



--48.Listado de cantidad de mesas de cada tipo.
select Descripcion  , COUNT(idMesa) CantidadDeMesas
	from mesas 
	group by Descripcion



--49.Liste la cantidad de mesas por sector, indique el nombre del sector.
select s.descripcion ,c1.IdSector,cantidadDeMesas
	from Sectores s inner join (select idSector,COUNT(Mesas.IdMesa) cantidadDeMesas
									from Mesas 
									group by IdSector) c1 on c1.IdSector=s.IdSector


--50.Liste los mozos y la cantidad de n�meros telef�nicos registrados de cada uno
select Mozos.IdMozo,nombre,apellido,calle,numero,piso,departamento,IdCodPos,c1.cantTelefonos
	from Mozos inner Join (select IdMozo,COUNT(idTelefono) cantTelefonos
							from TelefonoMozo
							group by IdMozo)c1 on c1.IdMozo=Mozos.IdMozo


--51.Liste los mozos con sus totales vendidos por cada uno de ellos, indicando su 
--nombre y apellido.
select NroFactura,IdMesa,Mozos.IdMozo,nombre,apellido,total
	from Facturas inner join Mozos on Mozos.IdMozo=Facturas.IdMozo
	   

--52.Liste los platos y la cantidad de veces que cada uno aparece en una factura. 
select idplato ,count(cantida) cantidad ,NroFactura 
	from itemsfactura i 
	group by i.NroFactura , i.IdPlato
								


--53.Liste los proveedores (su raz�n social) que proveen m�s de 10 art�culos.
select razsoc ,articulos
	from Proveedores inner join (select COUNT(idIngrediente) articulos, IdProveedor
									from ProIng
									group by IdProveedor
									having  COUNT(idIngrediente) > 10) c1 on c1.IdProveedor=Proveedores.IdProveedor
	

--54.Liste los platos, con su respectiva descripci�n en los casos que hayan sido 
--pedidos m�s de una vez,
select c1.IdPlato , descripcion, cantidad
	from Platos inner join (select count(NroFactura) cantidad , idPlato	
								from ItemsFactura 
								group by IdPlato
								having COUNT(NroFactura) > 1) c1 on c1.IdPlato=Platos.IdPlato

--55.Liste la cantidad de uso de los ingredientes en cada uno de los platos con su 
--correspondiente unidad de uso y ordenado descendentemente por cantidad 
--de utilizaci�n.
select ingredientes.IdIngrediente,Ingredientes.descripcion,cantida ,cUniUso,Platos.IdPlato,Platos.descripcion
	from PlaIng 
	inner join platos on PlaIng.IdPlato=Platos.IdPlato
	inner join Ingredientes on PlaIng.IdIngrediente=Ingredientes.IdIngrediente
	order by cantida DESC

--56.Liste de Raz�n Social de los proveedores con la cantidad de productos que 
--proveen cada uno, ordenados alfab�ticamente 
select razsoc,cantidad,proveedores.IdProveedor
	from Proveedores inner join (select IdProveedor, COUNT(idIngrediente) cantidad
									from ProIng 
									group by IdProveedor)c1 on c1.IdProveedor=Proveedores.IdProveedor
	order by razsoc


--57.Liste platos con chocolate o crema o ambos. 
select platos.IdPlato,platos.descripcion,precio 
	from PlaIng 
	inner join Ingredientes on Ingredientes.IdIngrediente=PlaIng.IdIngrediente
	inner join platos on PlaIng.IdPlato=Platos.IdPlato
	where Ingredientes.descripcion = 'Chocolate' or Ingredientes.descripcion = 'Crema'


--58.Liste los platos y su precio que no tengan ni pescado, ni frutos de mar, ni 
--cebolla, ordenado por precio de mayor a menor.
select * from rubros
select * from PlaIng
select * from Ingredientes
select * from Platos

select platos.IdPlato,platos.descripcion,precio ,Platos.IdRubro
	from PlaIng 
	inner join Ingredientes on Ingredientes.IdIngrediente=PlaIng.IdIngrediente
	inner join platos on PlaIng.IdPlato=Platos.IdPlato
	inner join (select idRubro ,descripcion 
				from Rubros 
				where descripcion!= 'Pescados') c1 on c1.IdRubro=Platos.IdRubro
	where Ingredientes.descripcion != 'Frutos de mar' or Ingredientes.descripcion != 'Cebolla'
	order by precio DESC


--59.Liste las facturas con su total y la mesa en que se realizaron las ventas 
--pertenecientes al Sal�n Azul.
select NroFactura,total,Mesas.IdMesa, Mesas.descripcion
	from Facturas 
	inner join Mesas on Mesas.IdMesa=Facturas.IdMesa
	inner join Sectores on Sectores.IdSector=Mesas.IdSector
	where Sectores.descripcion = 'Salon Azul'

--60.Listado de platos que poseen alg�n ingrediente de cantidad menor o igual a 
--la cantidad de reposici�n ordenado alfab�ticamente por ingrediente
select platos.IdPlato,platos.descripcion,platos.precio,platos.IdRubro,Ingredientes.IdIngrediente,Ingredientes.descripcion Ingredientes,puntoRep
	from Platos 
	inner join PlaIng on PlaIng.IdPlato=Platos.IdPlato
	inner join ingredientes on Ingredientes.IdIngrediente=PlaIng.IdIngrediente
	where cantida <= puntoRep
	order by Ingredientes.descripcion 
