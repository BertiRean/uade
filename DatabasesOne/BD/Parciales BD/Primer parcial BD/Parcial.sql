IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('Provee') AND type in ('U'))
BEGIN
	CREATE TABLE Provee
	(
		IdProveedor int NOT NULL,
		Nombre varchar(40) NULL,
		IdPieza int NOT NULL,
		IdProyecto int NOT NULL,
		CantPiezas INT NOT NULL,
		CONSTRAINT PK_Proveedores PRIMARY KEY CLUSTERED (IdProveedor),
	) 
END
GO
INSERT INTO Provee VALUES ( 1,'Roberto',1,1,10)

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('Usa') AND type in ('U'))
BEGIN
	CREATE TABLE Usa
	(
		IdPieza int NOT NULL,
		descripcion varchar(40) NULL,
		Cant int NOT NULL,
		CONSTRAINT PK_Usa PRIMARY KEY CLUSTERED (IdPieza)
	) 
END
GO
INSERT INTO Usa VALUES ( 1,'Plastico',1)

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('Utilizadas') AND type in ('U'))
BEGIN
	CREATE TABLE Utilizadas
	(
		IdPieza int NOT NULL,
		Cantidad int NOT NULL,
		IdEmpleado int NOT NULL,
		CONSTRAINT PK_Utilizadas PRIMARY KEY CLUSTERED (IdPieza),
	) 
END 
GO
INSERT INTO Utilizadas VALUES (1,2,1)

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('TrabajaEnProyecto') AND type in ('U'))
BEGIN
	CREATE TABLE TrabajaEnProyecto
	(
		IdProyecto int NOT NULL,
		IdEmpleado int NOT NULL,
		Nombre varchar(40) NULL,
		inicio datetime null,
		fin datetime null,
		CantHorasAsig int NOT NULL,
		CantHorasRealizadas int NOT NULL,
		CONSTRAINT PK_Unidades PRIMARY KEY CLUSTERED (IdProyecto)
	) 
End
GO
INSERT INTO TrabajaEnProyecto VALUES ( 1,2,'Carlos',01/01/2000,12/05/2000)

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('Composicion') AND type in ('U'))
BEGIN
	CREATE TABLE Composicion
	(
		IdPieza int NOT NULL,
		NombrePieza varchar(40) NULL,
		IdMateriales int NOT NULL,
		Materiales varchar(40) NULL,
		CONSTRAINT PK_Unidades PRIMARY KEY CLUSTERED (IdPieza)
	) 
End
GO
INSERT INTO Composicion VALUES ( 1,'Teclado',1,'Plastico')

--1)�Cu�l es el proveedor que m�s cantidad de piezas provey� a un proyecto determinado pero que no es el proyecto que tiene asignada la mayor cantidad de empleados?
select TrabajaEnProyecto.IdProyecto, COUNT(IdEmpleado) as CantEmpleados
	from TrabajaEnProyecto 
	inner join Provee on Provee.IdProyecto=TrabajaEnProyecto.IdProyecto
	Group by TrabajaEnProyecto.IdProyecto 
	Having COUNT(IdEmpleado) < (select IdProyecto, COUNT(IdEmpleado) as CantEmpleados
								from TrabajaEnProyecto
								Group by IdProyecto 
								having COUNT(IdEmpleado) >= all (select IdProyecto, COUNT(IdEmpleado) as CantEmpleados
																	from TrabajaEnProyecto
																	Group by IdProyecto )
							)
select COUNT(IdProyecto) ,IdProveedor
	from provee 
	Group by IdProveedor


--2
select Provee.Nombre
	from TrabajaEnProyecto 
	inner join Utilizadas on Utilizadas.IdEmpleado=TrabajaEnProyecto.IdEmpleado
	inner join Provee on Provee.IdPieza=Utilizadas.IdPieza
	where TrabajaEnProyecto.Nombre='Julio Hernandez' and CantPiezas=0

