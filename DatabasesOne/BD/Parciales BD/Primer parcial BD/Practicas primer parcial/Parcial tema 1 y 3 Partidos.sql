--Practica Primer Parcial Tema 1:
--1.Determinar el equipo con la mayor cantidad de partidos ganados que no posea la mayor cantidad de jugadores en la categoria 85
select * 
	from General
	where id_club != (select id_club
						from (select id_club ,count(nroDoc) cantJugadores
								from Jugadores
								where Categoria=85
								group by Id_Club
								having count(nroDoc) = (select MAX(cantJugadores) 
															from (select id_club ,count(nroDoc) cantJugadores
																	from Jugadores
																	where Categoria = 85 
																	group by Id_Club) c1  ) ) c2) and ganados >= all (select ganados from general) 

--2.Determinar el equipo con el mejor y peor promedio
select Nombre 
	from General
	where (Promedio>= all (Select Promedio
							From General))
	or (Promedio<=All(select Promedio
							from General))

--Practica Primer Parcial Tema 3:
--1.Determinar el jugador con el numero de documento mas bajo del equipo que se encuentra en el ultimo lugar de la categoria 84
select Tipodoc,Nrodoc,Jugadores.Nombre,Fecha_Nac,Categoria
	from Jugadores inner join General on General.Id_Club=Jugadores.Id_Club
	where (Categoria=84) and (Puntos <= all(select puntos
											from General)
							 )
	and (Nrodoc <= all(select Nrodoc 
						from Jugadores inner join General on General.Id_Club=Jugadores.Id_Club
						where Categoria=84 and (Puntos <= all(select puntos
																from General)
												) 
						)
		)
	
--2.Listar los nombres de los clubes que tengan mas jugadores en la categoria 84 que el promedio de los jugadores de la mencionada categoria
select Clubes.Id_Club,Nombre
	from Clubes inner join (select COUNT(Categoria) as cantJugadores,Id_Club 
								from Jugadores
								where Categoria=84
								group by Id_Club) c1 on c1.Id_Club=Clubes.Id_Club
	where cantJugadores>= all (select COUNT(Categoria) as cantJugadores 
								from Jugadores
								where Categoria=84
								group by Id_Club)

