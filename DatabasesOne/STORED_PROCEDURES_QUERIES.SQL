DROP PROCEDURE [dbo].[VER_LOCALIDADES];
GO
CREATE PROCEDURE VER_LOCALIDADES(
	@locName VARCHAR(56)
)
AS
SELECT * FROM Localidades where localida = @locName
GO

DROP PROCEDURE [dbo].[INSERTAR_LOCALIDAD];
GO
CREATE PROCEDURE INSERTAR_LOCALIDAD(
	@locName VARCHAR(56)
)
AS
	DECLARE @myMax INT;
	SET @myMax = (SELECT MAX(IdCodPos) FROM Localidades);
	SELECT * FROM Localidades WHERE IdCodpos = @myMax;
	INSERT INTO Localidades (IdCodpos, localida) VALUES
	(@myMax + 1, @locName)
	SELECT * FROM Localidades WHERE IdCodpos = @myMax + 1;
GO


EXECUTE INSERTAR_LOCALIDAD
	@locName = 'Caba'
GO