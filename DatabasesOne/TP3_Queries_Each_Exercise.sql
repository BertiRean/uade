use ZRESTAURANTE;
-- Ejercicio 1
-- Forma 1
/*select *
from Mozos
FULL OUTER JOIN TelefonoMozo
ON Mozos.IdMozo = TelefonoMozo.IdMozo
WHERE IdTelefono IS NULL*/

-- Forma 2
-- select *  from Mozos where IdMozo not in (select IdMozo from TelefonoMozo)

-- Ejercicio 2
-- 2.	Mostrar la lista de todos los platos existentes con sus respectivos ingredientes, ordenar por nombre de plato.

-- Ejercicio 3
-- 3.	Mostrar el men� del restaurant considerando Rubro, Plato, Precio.
/*DELETE FROM Platos where descripcion = 'Arepas con Carne Mechada';
INSERT INTO Platos (IdPlato, descripcion, precio, IdRubro) values
(5500,'Arepas con Carne Mechada', 560, NULL);

Select Platos.IdPlato, Platos.descripcion, Rubros.descripcion, precio FROM Platos
INNER JOIN Rubros
ON Rubros.IdRubro = platos.IdRubro*/

-- Ejercicio 4
-- 4. Mostrar la raz�n social, direcci�n (Formato: �Calle � Altura, Piso: x�) y localidad de los proveedores que tengan un departamento como oficina.

/* SELECT razsoc, CONCAT(Calle, '- Altura: ', numero, ', Piso: ', piso) AS Direccion, Localidades.localida from Proveedores
INNER JOIN Localidades
ON Localidades.IdCodpos = Proveedores.IdCodPos
WHERE Proveedores.departamento != ''; */

-- Ejercicio 5
-- 5.	Crear una vista donde se muestre el nrofactura, la cantidad total y el importe total, donde se haya vendido m�s de 30 pesos y m�s de 5 �tems, 
-- renombrar los campos a cantidad_items e importe_total, adem�s mostrar la cantidad de personas atendidas en cada factura.


/* select Facturas.NroFactura, Facturas.cantidadPersonas AS PersonasAtendidas, SUM(ItemsFactura.cantida) as CantidadItems, 
total as TotalVendido from Facturas
INNER JOIN ItemsFactura
ON ItemsFactura.NroFactura = Facturas.NroFactura
Group by Facturas.NroFactura, Facturas.cantidadPersonas, Facturas.total
HAVING Facturas.total > 30 and SUM(ItemsFactura.cantida) > 5 */

-- Ejercicio 6
-- 6.	Crear una vista donde se muestre ordenado por proveedor la lista de ingredientes que ofrece cada uno y su precio.
select Proveedores.IdProveedor, razsoc, Ingredientes.descripcion, Ingredientes.precComp * (ProIng.factor) as Precio from Proveedores
INNER JOIN ProIng
ON ProIng.IdProveedor = Proveedores.IdProveedor
INNER JOIN Ingredientes
ON Ingredientes.IdIngrediente = ProIng.IdIngrediente
order by Proveedores.razsoc