create or alter procedure nuevoClub 
	@nombre varchar(50)

as
begin

	if exists(select * from Clubes where Nombre=@nombre)
	begin
		Print 'El nombre del club ya se encuentra registrado'
	end
	else
	begin
		declare @cantPartidos int =(select COUNT(id_club) from Clubes)

		declare @idZona int=(
		select MIN(x.nroZona) as NroZona from Clubes 
		inner join (select COUNT(Nombre) as cantidad, Nrozona from Clubes group by Nrozona) x on Clubes.Nrozona=x.Nrozona
		inner join(
			select min(cantidad) as cantidad
				from clubes 
				inner join (select COUNT(Nombre) as cantidad, Nrozona from Clubes group by Nrozona) x on x.NroZona=clubes.nroZona
				) y on y.cantidad=x.cantidad
						)
		declare @idClub int=1

		while exists(select * from Clubes where Id_Club=@idClub)
		begin
			select @idClub=@idClub+1
		end
	
		declare @nombreTabla varchar(50)='JugadoresDel'+REPLACE(@nombre,' ','_')

		if exists (select * from sysobjects where xtype = 'U' and name = @nombreTabla)
		begin
			declare @eliminar nvarchar(100)= 'drop table '+@nombreTabla
			exec sp_executeSQL @eliminar
		end

		declare @crear nvarchar(1000)= 'create table '+@nombreTabla +' (nombreClub varchar(50), tipoDoc Varchar(50), nroDoc int, idClub int )'
		exec sp_executeSQL @crear


		declare JugadoresMasJovenes cursor for
			select Tipodoc,Nrodoc,Categoria from Jugadores order by Categoria,Fecha_Nac DESC
		open jugadoresMasJovenes
			declare @categoria int,@nroDoc int, @tipoDoc varchar(50)
			fetch next from JugadoresMasJovenes into @tipoDoc,@nroDoc,@categoria
			
			declare @insertados int =0
			declare @catAnterior int= @categoria

			while (@@FETCH_STATUS =0)
			begin
				if(@categoria=@catAnterior and @insertados<3) 
				begin
					declare @insertarDatos nvarchar(1000) = 'insert into ' +@nombreTabla +' values ( '+''''+@nombre+'''' +','+''''+@tipoDoc+''''+','+cast(@nroDoc as varchar(50))+','+ cast(@idClub as varchar(50)) +')'
					exec sp_executeSQL @insertarDatos 
					select @insertados=@insertados+1
					fetch next from JugadoresMasJovenes into @tipoDoc,@nroDoc,@categoria
				end
				else
				begin
					if(@categoria=@catAnterior)
					begin
						fetch next from JugadoresMasJovenes into @tipoDoc,@nroDoc,@categoria
					end
					else
					begin
						select @catAnterior=@categoria
						select @insertados=0
					end
				end
			end

			close JugadoresMasJovenes
			deallocate JugadoresMasJovenes
			
			if(@cantPartidos!=null)
			begin
				
			end
	end

end

select * from Partidos
exec nuevoClub 'Bokita'
select * from JugadoresDelBokita